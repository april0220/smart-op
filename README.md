 **二维映射：** <br>
![输入图片说明](Doc/2Dmapping.png)

 **道路细化：** <br>
![输入图片说明](thin_road_error.png)

 **道路中线提取流程：** <br>
![输入图片说明](Doc/Images/%E9%81%93%E8%B7%AF%E4%B8%AD%E7%BA%BF%E6%8F%90%E5%8F%96%E4%B8%AD%E9%97%B4%E7%BB%93%E6%9E%9C.png)

 **连通性增强：** <br>
![输入图片说明](Doc/Images/connenction_2.png)

 **路口形状计算：** <br>
![输入图片说明](intersection.png)

 **最终模型：** <br>
![输入图片说明](Doc/Images/mesh.png)