#include "pch.h"
#include "PointCloudFileLoader.h"

using namespace hiveObliquePhotography;

hiveObliquePhotography::IPointCloudFileLoader::IPointCloudFileLoader()
{
}

hiveObliquePhotography::IPointCloudFileLoader::~IPointCloudFileLoader()
{
}

//*****************************************************************
//FUNCTION: 
SUnorganizedPointCloud* IPointCloudFileLoader::load(const std::string& vFileName)
{
	try
	{
		if (!hiveUtility::hiveFileSystem::hiveIsFileExisted(vFileName))
			_THROW_RUNTIME_ERROR(_FORMAT_STR1("Fail to load the specified point cloud file [%1%] because it does not exist.", vFileName));
		SUnorganizedPointCloud *pPointCloud = __loadV(vFileName);
		if (!pPointCloud) _THROW_RUNTIME_ERROR("IPointCloudFileLoader::__loadV() return nullptr");
		return pPointCloud;
	}
	catch (std::runtime_error& e)
	{
		_HIVE_OUTPUT_WARNING(_FORMAT_STR2("Fail to load point cloud file [%1%] due to the following error: [%2%].", vFileName, e.what()));
		return nullptr;
	}
	catch (...)
	{
		_HIVE_OUTPUT_WARNING(_FORMAT_STR1("Fail to load point cloud file [%1%] due to unexpected error.", vFileName));
		return nullptr;
	}
}