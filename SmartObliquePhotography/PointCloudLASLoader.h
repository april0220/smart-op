#pragma once
#include "PointCloudFileLoader.h"

namespace hiveObliquePhotography
{
	class CPointCloudLASLoader : public IPointCloudFileLoader
	{
	public:
		CPointCloudLASLoader() = default;
		~CPointCloudLASLoader() = default;

	private:
	};
}

