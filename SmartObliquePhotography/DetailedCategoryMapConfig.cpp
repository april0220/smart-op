#include "pch.h"
#include "DetailedCategoryMapConfig.h"

using namespace hiveObliquePhotography;

bool hiveObliquePhotography::CDetailedCategoryMapConfig::init()
{
	_ASSERTE(!m_IsReady);

	const hiveConfig::CHiveConfig* pConfig = getSubconfigAt(0);
	if (!Common::isSubconfigValid(pConfig, std::string("DetailedRegion")))return false;

	const hiveConfig::CHiveConfig* pMapLegendConfig = pConfig->getSubconfigAt(0);
	if (!Common::isSubconfigValid(pMapLegendConfig, std::string("MapLengend")))return false;

	std::optional<float> MapWorldMinX = pMapLegendConfig->getAttribute<float>("WorldMinX");
	_ASSERTE(MapWorldMinX.has_value());
	std::optional<float> MapWorldMaxX = pMapLegendConfig->getAttribute<float>("WorldMaxX");
	_ASSERTE(MapWorldMaxX.has_value());
	std::optional<float> MapWorldMinY = pMapLegendConfig->getAttribute<float>("WorldMinY");
	_ASSERTE(MapWorldMinY.has_value());
	std::optional<float> MapWorldMaxY = pMapLegendConfig->getAttribute<float>("WorldMaxY");
	_ASSERTE(MapWorldMaxY.has_value());
	m_WorldBoundingBox << MapWorldMinX.value(), MapWorldMaxX.value(), MapWorldMinY.value(), MapWorldMaxY.value();

	std::stringstream String2Int;
	std::optional<std::string> MapWidthInPixelStr = pMapLegendConfig->getAttribute<std::string>("WidthInPixel");
	_ASSERTE(MapWidthInPixelStr.has_value());
	String2Int.clear();
	String2Int << MapWidthInPixelStr.value();
	String2Int >> m_SizeInPixel.x();
	std::optional<std::string> MapHeightInPixelStr = pMapLegendConfig->getAttribute<std::string>("HeightInPixel");
	_ASSERTE(MapHeightInPixelStr.has_value());
	String2Int.clear();
	String2Int << MapHeightInPixelStr.value();
	String2Int >> m_SizeInPixel.y();

	const hiveConfig::CHiveConfig* pBitmapConfig = pConfig->getSubconfigAt(1);
	if (!Common::isSubconfigValid(pBitmapConfig, std::string("CategoryFiles")))return false;

	std::optional<std::string> BitmapDirectory = pBitmapConfig->getAttribute<std::string>("Directory");
	_ASSERTE(BitmapDirectory.has_value());
	std::optional<int> BitmapNum = pBitmapConfig->getAttribute<int>("FileNum");
	_ASSERTE(BitmapNum.has_value());

	std::pair<std::vector<int>, std::vector<int>> ImageCoordinates;
	for (int i = 0; i < BitmapNum.value(); i++)
	{
		const hiveConfig::CHiveConfig* pTempConfig = pBitmapConfig->getSubconfigAt(i);
		if (!Common::isSubconfigValid(pTempConfig, std::string("File")))return false;

		std::optional<std::string> BitmapName = pTempConfig->getAttribute<std::string>("Name");
		_ASSERTE(BitmapName.has_value());

		std::tuple<std::string, std::uint32_t, std::uint32_t> tempMap;
		std::get<0>(tempMap) = BitmapDirectory.value() + BitmapName.value();
		std::optional<std::string> ImageCoordinatesString = pTempConfig->getAttribute<std::string>(std::string("ImageCoordinate"));
		_ASSERTE(ImageCoordinatesString.has_value());
		String2Int.clear();
		String2Int << ImageCoordinatesString.value();
		String2Int >> std::get<1>(tempMap) >> std::get<2>(tempMap);

		m_MapPathSet.emplace_back(tempMap);
	}

	const hiveConfig::CHiveConfig* pDescriptorConfig = pConfig->getSubconfigAt(2);
	if (!Common::isSubconfigValid(pDescriptorConfig, std::string("DescriptorFiles")))return false;

	std::optional<std::string> DescriptorDir = pMapLegendConfig->getAttribute<std::string>("Directory");
	_ASSERTE(DescriptorDir.has_value());
	std::optional<std::string> RoadDescriptorFileName = pMapLegendConfig->getAttribute<std::string>("RoadDescriptorFileName");
	_ASSERTE(RoadDescriptorFileName.has_value());
	std::optional<std::string> RiverDescriptorFileName = pMapLegendConfig->getAttribute<std::string>("RiverDescriptorFileName");
	_ASSERTE(RiverDescriptorFileName.has_value());
	std::optional<std::string> LandscapeDescriptorFileName = pMapLegendConfig->getAttribute<std::string>("LandscapeDescriptorFileName");
	_ASSERTE(LandscapeDescriptorFileName.has_value());
	std::optional<std::string> BuildingDescriptorFileName = pMapLegendConfig->getAttribute<std::string>("BuildingDescriptorFileName");
	_ASSERTE(BuildingDescriptorFileName.has_value());
	
}

void hiveObliquePhotography::CDetailedCategoryMapConfig::__defineAttributesV()
{
	_defineAttribute(MetafileKeywords::DetailedRegion, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute(MetafileKeywords::MapLengend, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute((const std::string)"WorldMinX", hiveConfig::EConfigDataType::ATTRIBUTE_FLOAT);
	_defineAttribute((const std::string)"WorldMaxX", hiveConfig::EConfigDataType::ATTRIBUTE_FLOAT);
	_defineAttribute((const std::string)"WorldMinY", hiveConfig::EConfigDataType::ATTRIBUTE_FLOAT);
	_defineAttribute((const std::string)"WorldMaxY", hiveConfig::EConfigDataType::ATTRIBUTE_FLOAT);
	_defineAttribute((const std::string)"WidthInPixel", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute((const std::string)"HeightInPixel", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::CategoryFiles, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute(MetafileKeywords::Directory, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::FileNum, hiveConfig::EConfigDataType::ATTRIBUTE_INT);
	_defineAttribute(MetafileKeywords::File, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute(MetafileKeywords::Name, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute((const std::string)"ImageCoordinate", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute((const std::string)"DescriptorFiles", hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute((const std::string)"RoadDescriptorFileName", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute((const std::string)"RiverDescriptorFileName", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute((const std::string)"LandscapeDescriptorFileName", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute((const std::string)"BuildingDescriptorFileName", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
}