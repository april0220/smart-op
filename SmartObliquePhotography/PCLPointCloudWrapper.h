#pragma once
#include "pcl/point_types.h"
#include "pcl/point_cloud.h"
#include "DefineMemberChecker.h"

_DEFINE_MEMBER_CHECKER(r)
_DEFINE_MEMBER_CHECKER(normal_x)

namespace hiveObliquePhotography
{
	class CPCLPointCloudWrapper  //FIXME：这个类目前的功能很混乱，已经实现的dumpData2UnorgnizedPointCloud()根本就没用到一个成员变量，那这个函数放在这个类干嘛？
	{
	public:
		CPCLPointCloudWrapper() = default;
		~CPCLPointCloudWrapper() = default;

		// TODO: 参数以及返回值暂定
		// vPointTypeFlag表示我想要的PCL数据类型，
		[[nodiscard]] bool extractDataFromUnorgnizedPointCloud(std::uint8_t vPointTypeFlag, SUnorganizedPointCloud* vUnorganizedPointCloud);

		// TODO: 外部通过模板告知想要的类型，同时这个类型必须和成员变量中非空指针的类型匹配，否则异常处理
		template<class T>
		pcl::PointCloud<T>* getPointCloud() const
		{
			if constexpr (!_HAS_MEMBER(T, r) && !_HAS_MEMBER(T, normal_x))
					return m_pPointCloudXYZ;
			if constexpr (_HAS_MEMBER(T, r) && !_HAS_MEMBER(T, normal_x))
					return m_pPointCloudXYZRGB;
			if constexpr (_HAS_MEMBER(T, r) && _HAS_MEMBER(T, normal_x))
					return m_pPointCloudXYZRGBNormal;
		}

		template<class T>
		[[nodiscard]] bool dumpData2UnorgnizedPointCloud(const pcl::PointCloud<T>* vPCLPointCloud, SUnorganizedPointCloud* voUnorganizedPointCloud)
		{
			_ASSERTE(vPCLPointCloud && voUnorganizedPointCloud);

			_HIVE_EARLY_RETURN(vPCLPointCloud->points.empty(), "PCL Point cloud is empty", false);
			if (voUnorganizedPointCloud == NULL) return false;
			voUnorganizedPointCloud->clear();

			bool isColorSetEmpty = true;
			bool isNormalSetEmpty = true;
			for (const auto& Point : vPCLPointCloud->points)
			{//FIXME：这里代码有很强的假设，基本就认为T最多就是XYZ+Color+Normal
				voUnorganizedPointCloud->_PositionSet.push_back(Eigen::Vector3f(Point.x, Point.y, Point.z));
				if (_HAS_MEMBER(T, r))
					voUnorganizedPointCloud->_ColorSet.push_back(SRGBColor(Point.r, Point.g, Point.b));
				if (_HAS_MEMBER(T, normal_x))
					voUnorganizedPointCloud->_NormalSet.push_back(Eigen::Vector3f(Point.normal_x, Point.normal_y, Point.normal_z));
				if (isColorSetEmpty && (Point.r != 0) && (Point.g != 0) && (Point.b != 0))
					isColorSetEmpty = false;
				if (isNormalSetEmpty && (Point.normal_x != 0) && (Point.normal_y != 0) && (Point.normal_z != 0))
					isNormalSetEmpty = false;
			}

			if (isColorSetEmpty) std::vector<SRGBColor>().swap(voUnorganizedPointCloud->_ColorSet);
			if (isNormalSetEmpty) std::vector<Eigen::Vector3f>().swap(voUnorganizedPointCloud->_NormalSet);

			return voUnorganizedPointCloud->isValid();
		}

	private:
		std::uint8_t m_PointTypeFlag = 0;
		pcl::PointCloud<pcl::PointXYZ>* m_pPointCloudXYZ = nullptr;
		pcl::PointCloud<pcl::PointXYZRGB>* m_pPointCloudXYZRGB = nullptr;
		pcl::PointCloud<pcl::PointXYZRGBNormal>* m_pPointCloudXYZRGBNormal = nullptr;
	};
}