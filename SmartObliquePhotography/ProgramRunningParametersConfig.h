#pragma once
#include "common/Singleton.h"
#include "common/HiveConfig.h"

namespace hiveObliquePhotography
{
	class CProgramRunningParametersConfig: public hiveDesignPattern::CSingleton<CProgramRunningParametersConfig>, public hiveConfig::CHiveConfig
	{
	public:
		~CProgramRunningParametersConfig() = default;
		[[nodiscard]] bool init();
		bool isReady() const { return m_IsReady; }
		const hiveConfig::CHiveConfig* getParametersPtr() const { return getSubconfigAt(0); }

	private:
		CProgramRunningParametersConfig() = default;
		virtual void __defineAttributesV() override;

		bool m_IsReady = false;

		friend class hiveDesignPattern::CSingleton<CProgramRunningParametersConfig>;
	};
}


