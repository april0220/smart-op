#pragma once

namespace hiveObliquePhotography
{
	class CUserDefinedCategoryMapConfig : public hiveDesignPattern::CSingleton<CUserDefinedCategoryMapConfig>, public hiveConfig::CHiveConfig
	{
	public:
			~CUserDefinedCategoryMapConfig() = default;
			[[nodiscard]] bool init();
			bool isReady() const { return m_IsReady; }
		
			std::map<SRGBColor, EUserDefinedPointCatetory> getMapCategory() const { return m_MapCategory; }
			std::pair<uint32_t, uint32_t> getBitmapDim() const { return m_BitmapDim; }
			std::pair<float, float> getBitmapSpan() const { return m_BitmapSpan; }
			std::vector<std::tuple<std::string, std::uint32_t, std::uint32_t>> getMapPathSet() const { return m_MapPathSet; }
			
	private:
		CUserDefinedCategoryMapConfig() = default;
		virtual void __defineAttributesV() override;

		bool m_IsReady = false;
		std::map<SRGBColor, EUserDefinedPointCatetory> m_MapCategory;
		std::pair<uint32_t, uint32_t> m_BitmapDim;
		std::pair<float, float> m_BitmapSpan;
		std::vector<std::tuple<std::string, std::uint32_t, std::uint32_t>> m_MapPathSet;
		
		friend class hiveDesignPattern::CSingleton<CUserDefinedCategoryMapConfig>;
	};
}



