#include "pch.h"
#include "PointCloudPLYLoader.h"
#include "PCLPointCloudWrapper.h"

using namespace hiveObliquePhotography;

_REGISTER_EXCLUSIVE_PRODUCT(CPointCloudPLYLoader, MetafileKeywords::PLY_LOADER_SIG)

//*****************************************************************
//FUNCTION: 
SUnorganizedPointCloud* CPointCloudPLYLoader::__loadV(const std::string& vFileName)
{
	pcl::PointCloud<pcl::PointXYZRGBNormal>* pPointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
	pcl::io::loadPLYFile<pcl::PointCloud<pcl::PointXYZRGBNormal>::PointType>(vFileName, *pPointCloud);

	SUnorganizedPointCloud* pUnorganizedPointCloud = new SUnorganizedPointCloud;
	CPCLPointCloudWrapper PCLPointCloudWrapper;
	bool IsSuccess = PCLPointCloudWrapper.dumpData2UnorgnizedPointCloud<pcl::PointXYZRGBNormal>(pPointCloud, pUnorganizedPointCloud);

	delete pPointCloud;
	_HIVE_EARLY_RETURN(!IsSuccess, _FORMAT_STR1("Fail to dump pcl point cloud to unorgnized point cloud [%1%].", vFileName), nullptr);
	return pUnorganizedPointCloud;
}