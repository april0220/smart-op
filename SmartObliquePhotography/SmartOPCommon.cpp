#include "pch.h"
#include "SmartOPCommon.h"

using namespace hiveObliquePhotography;

//*****************************************************************
//FUNCTION: 
double hiveObliquePhotography::Common::getSystemFreeMemoryInMB()
{
	MEMORYSTATUSEX Statex;
	Statex.dwLength = sizeof(Statex);
	GlobalMemoryStatusEx(&Statex);
	return Statex.ullAvailPhys / 1024 / 1024;
}

//*****************************************************************
//FUNCTION: 
double hiveObliquePhotography::Common::getSystemTotalMemoryInMB()
{
	MEMORYSTATUSEX Statex;
	Statex.dwLength = sizeof(Statex);
	GlobalMemoryStatusEx(&Statex);
	return Statex.ullTotalPhys / 1024 / 1024;
}

double hiveObliquePhotography::Common::degree2Radian(double vDegree)
{
	return vDegree * std::numbers::pi / 180.0;
}

double hiveObliquePhotography::Common::calcDistance(double vLat1, double vLng1, double vLat2, double vLng2)
{
	double radLat1 = degree2Radian(vLat1);
	double radLat2 = degree2Radian(vLat2);
	double a = radLat1 - radLat2;
	double b = degree2Radian(vLng1) - degree2Radian(vLng2);


	double s = 2 * asin(sqrt(pow(sin(a / 2), 2) + cos(radLat1) * cos(radLat2) * pow(sin(b / 2), 2)));
	s = s * EARTH_RADIUS;
	s = s * 1000;
	return s;
}

__int64 hiveObliquePhotography::Common::getFolderSize(const std::wstring& vStrDir)
{
	__int64 nSize = 0;
	std::wstring strRootPath = vStrDir + L"\\";
	std::wstring strRoot = strRootPath + L"*.*";
	WIN32_FIND_DATA fd;
	HANDLE hFind = FindFirstFile(strRoot.c_str(), &fd);
	if (INVALID_HANDLE_VALUE == hFind)
		return nSize;
	while (FindNextFile(hFind, &fd))
	{
		if (wcscmp(fd.cFileName, L".") == 0 || wcscmp(fd.cFileName, L"..") == 0)
			continue;
		std::wstring strPath = strRootPath + fd.cFileName;
		if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			nSize += getFolderSize(strPath);
		else
		{
			HANDLE hFile = CreateFile(strPath.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			if (INVALID_HANDLE_VALUE == hFile)
				continue;
			LARGE_INTEGER size;
			if (::GetFileSizeEx(hFile, &size))
			{
				nSize += size.QuadPart;
			}
			CloseHandle(hFile);
		}
	}
	FindClose(hFind);
	return nSize;
}

std::wstring hiveObliquePhotography::Common::string2Wstring(std::string vStr)
{
	std::wstring result;
	int len = MultiByteToWideChar(CP_ACP, 0, vStr.c_str(), vStr.size(), NULL, 0);
	TCHAR* buffer = new TCHAR[len + 1];
	MultiByteToWideChar(CP_ACP, 0, vStr.c_str(), vStr.size(), buffer, len);
	buffer[len] = '\0';
	result.append(buffer);
	delete[] buffer;
	return result;
}

bool hiveObliquePhotography::Common::isSubconfigValid(const hiveConfig::CHiveConfig* vConfig, std::string vConfigType)
{
	if (vConfig && _IS_STR_IDENTICAL(vConfig->getSubconfigType(), vConfigType))return true;
	return false;
}

std::pair<float, float> hiveObliquePhotography::Common::calcEntiretySpan(const hiveConfig::CHiveConfig* vConfig, std::pair<uint32_t, uint32_t> vDim)
{
	std::array<int, 4> VertexTileIndex = { 0,vDim.second - 1,vDim.first * vDim.second - 1,(vDim.first - 1) * vDim.second };
	std::stringstream String2Double;
	std::array<double, 8> Coordinates;
	for (int i = 0; i < 4; i++)
	{
		const hiveConfig::CHiveConfig* pLinearRing = vConfig->getSubconfigAt(VertexTileIndex[i])->getSubconfigAt(0);
		std::optional<std::string> CoordinatesString = pLinearRing->getAttribute<std::string>(std::string("Coordinates").append(std::to_string(i)));
		String2Double.clear();
		String2Double << CoordinatesString.value();
		String2Double >> Coordinates[i * 2] >> Coordinates[i * 2 + 1];
	}

	double LongitudeSpan = (Common::calcDistance(Coordinates[1], Coordinates[0], Coordinates[7], Coordinates[6]) + Common::calcDistance(Coordinates[3], Coordinates[2], Coordinates[5], Coordinates[4])) / 2;
	double LatitudeSpan = (Common::calcDistance(Coordinates[1], Coordinates[0], Coordinates[3], Coordinates[2]) + Common::calcDistance(Coordinates[5], Coordinates[4], Coordinates[7], Coordinates[6])) / 2;

	return std::make_pair((float)LongitudeSpan, (float)LatitudeSpan);
}