#include "pch.h"
#include "OverallConfig.h"
#include <array>
#include <windows.h>
#include <string>
#include <locale>
#include <codecvt>
#include <math.h>

using namespace hiveObliquePhotography;

bool CRawPointCloudConfig::init()
{
	_ASSERTE(!m_IsReady);

	const hiveConfig::CHiveConfig* pConfig = getSubconfigAt(0);
	if (!Common::isSubconfigValid(pConfig, std::string("TileFiles")))return false;

	std::optional<int> TileNum = pConfig->getAttribute<int>("FileNum");
	_ASSERTE(TileNum.has_value());

	std::optional<std::string> TileSetDirectory = pConfig->getAttribute<std::string>("Directory");
	_ASSERTE(TileSetDirectory.has_value());
	long long SizeInByte = Common::getFolderSize(Common::string2Wstring(TileSetDirectory.value()));
	float SizeInMB = SizeInByte / pow(1024, 2);
	setAttribute(MetafileKeywords::OVERALL_SCENE_SIZE_IN_MB, SizeInMB);

	std::stringstream String2Double;
	std::array<double, 8> PreCoordinates;
	PreCoordinates.fill(1000);//set an unreachalbe value(value>180)
	std::uint32_t TileXIndex = -1, TileYIndex = 0;
	for (int i = 0; i < TileNum.value(); i++)
	{
		const hiveConfig::CHiveConfig* pTempConfig = pConfig->getSubconfigAt(i);
		if (!Common::isSubconfigValid(pTempConfig, std::string("File")))return false;

		STile TempTile;
		std::optional<std::string> TileName = pTempConfig->getAttribute<std::string>("Name");
		_ASSERTE(TileName.has_value());
		TempTile.FileName = TileSetDirectory.value() + TileName.value();

		pTempConfig = pTempConfig->getSubconfigAt(0);
		if (!Common::isSubconfigValid(pTempConfig, std::string("LongitudeLatitudeSpan")))return false;

		std::array<double, 8> Coordinates;
		for (int k = 0; k < 4; k++)
		{
			std::optional<std::string> CoordinatesString = pTempConfig->getAttribute<std::string>(std::string("Coordinates").append(std::to_string(k)));
			_ASSERTE(CoordinatesString.has_value());
			String2Double.clear();
			String2Double << CoordinatesString.value();
			String2Double >> Coordinates[k * 2] >> Coordinates[k * 2 + 1];
		}

		TempTile.LatitudeSpan = (Common::calcDistance(Coordinates[1], Coordinates[0], Coordinates[3], Coordinates[2]) + Common::calcDistance(Coordinates[5], Coordinates[4], Coordinates[7], Coordinates[6])) / 2;
		TempTile.LongitudeSpan = (Common::calcDistance(Coordinates[1], Coordinates[0], Coordinates[7], Coordinates[6]) + Common::calcDistance(Coordinates[3], Coordinates[2], Coordinates[5], Coordinates[4])) / 2;

		if (Coordinates[0] == PreCoordinates[2] && Coordinates[1] == PreCoordinates[3]
			&& Coordinates[6] == PreCoordinates[4] && Coordinates[7] == PreCoordinates[5])
		{
			TempTile.TileX = TileXIndex;
			TempTile.TileY = ++TileYIndex;
		}
		else
		{
			TempTile.TileX = ++TileXIndex;
			TempTile.TileY = TileYIndex = 0;
		}
		m_TileSet.emplace_back(TempTile);
		PreCoordinates = Coordinates;
	}
	m_TileDim = std::make_pair(++TileXIndex, ++TileYIndex);

	m_SceneSpan = Common::calcEntiretySpan(pConfig, m_TileDim);

	__CalcTileLongitudeLatitudeSpanSet();

	m_IsReady = true;
	return true;
}

//*****************************************************************
//FUNCTION: reorganization tile index from column to row
void CRawPointCloudConfig::__CalcTileLongitudeLatitudeSpanSet()
{
	m_TileLongitudeLatitudeSpanSet.reserve(m_TileSet.size());
	for (std::uint32_t i = 0; i < m_TileDim.second; i++)
	{
		for (std::uint32_t k = 0; k < m_TileDim.first; k++)
		{
			int Index = i + k * m_TileDim.second;
			m_TileLongitudeLatitudeSpanSet.emplace_back(std::make_pair(m_TileSet[Index].LongitudeSpan, m_TileSet[Index].LatitudeSpan));
		}
	}
}

//*****************************************************************
//FUNCTION: 
std::tuple<std::string, std::uint32_t, std::uint32_t> CRawPointCloudConfig::getFirstPointCloudTileFile()
{
	std::string FileName;
	std::uint32_t TileX, TileY;
	if (m_TileSet.size() == 0)FileName = "";
	else
	{
		FileName = m_TileSet[0].FileName;
		TileX = m_TileSet[0].TileX;
		TileY = m_TileSet[0].TileY;
		m_TileSetIndex = 1;
	}
	return std::make_tuple(FileName, TileX, TileY);
}

//*****************************************************************
//FUNCTION: 
std::tuple<std::string, std::uint32_t, std::uint32_t> CRawPointCloudConfig::getNextPointCloudTileFile()
{
	std::string FileName;
	std::uint32_t TileX, TileY;
	if (m_TileSetIndex >= m_TileSet.size())FileName = "";
	else
	{
		FileName = m_TileSet[m_TileSetIndex].FileName;
		TileX = m_TileSet[m_TileSetIndex].TileX;
		TileY = m_TileSet[m_TileSetIndex].TileY;
		m_TileSetIndex++;
	}

	return std::make_tuple(FileName, TileX, TileY);
}

//*****************************************************************
//FUNCTION: 
void CRawPointCloudConfig::__defineAttributesV()
{
	_defineAttribute(MetafileKeywords::OVERALL_SCENE_SIZE_IN_MB, hiveConfig::EConfigDataType::ATTRIBUTE_FLOAT);
	_defineAttribute(MetafileKeywords::CELL_SCALE, hiveConfig::EConfigDataType::ATTRIBUTE_FLOAT);
	_defineAttribute(MetafileKeywords::WORLD_SCALE_X, hiveConfig::EConfigDataType::ATTRIBUTE_FLOAT);
	_defineAttribute(MetafileKeywords::WORLD_SCALE_Y, hiveConfig::EConfigDataType::ATTRIBUTE_FLOAT);
	_defineAttribute(MetafileKeywords::TileFiles, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute(MetafileKeywords::Directory, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::FileNum, hiveConfig::EConfigDataType::ATTRIBUTE_INT);
	_defineAttribute(MetafileKeywords::File, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute(MetafileKeywords::Name, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::LongitudeLatitudeSpan, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute(MetafileKeywords::Coordinates0, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::Coordinates1, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::Coordinates2, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::Coordinates3, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
}

void hiveObliquePhotography::CRawPointCloudConfig::__onLoadedV()
{
	setAttribute(MetafileKeywords::CELL_SCALE, 0.25f);
	setAttribute(MetafileKeywords::WORLD_SCALE_X, 1.0f);
	setAttribute(MetafileKeywords::WORLD_SCALE_Y, 1.0f);
}