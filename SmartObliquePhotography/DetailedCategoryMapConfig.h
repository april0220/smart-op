#pragma once

namespace hiveObliquePhotography
{
	class CDetailedCategoryMapConfig : public hiveDesignPattern::CSingleton<CDetailedCategoryMapConfig>, public hiveConfig::CHiveConfig
	{
	public:
		~CDetailedCategoryMapConfig() = default;
		[[nodiscard]] bool init();
		bool isReady() const { return m_IsReady; }

		Eigen::Vector2i getSizeInPixel() { return m_SizeInPixel; }
		Eigen::Vector4f getWorldBoundingBox() { return m_WorldBoundingBox; }
		std::vector<std::tuple<std::string, std::uint32_t, std::uint32_t>> getMapPathSet() { return m_MapPathSet; }

	private:
		CDetailedCategoryMapConfig() = default;
		virtual void __defineAttributesV() override;

		bool m_IsReady = false;
		Eigen::Vector2i m_SizeInPixel;
		Eigen::Vector4f m_WorldBoundingBox;
		std::vector<std::tuple<std::string, std::uint32_t, std::uint32_t>> m_MapPathSet;
		
		friend class hiveDesignPattern::CSingleton<CDetailedCategoryMapConfig>;
	};
}



