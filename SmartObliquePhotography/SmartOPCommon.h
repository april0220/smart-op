#pragma once
#include <stdint.h>
#include <string>
#include <Eigen/Core>
#include "common/HiveConfig.h"

namespace hiveObliquePhotography
{
	enum class EUserDefinedPointCatetory : std::uint8_t
	{
		USER_UNDEFINED = 1,
		USER_DEFINED_ROAD = 2,
		USER_DEFINED_LANDSCAPE = 4,
		USER_DEFINED_RIVER = 8,
		USER_DEFINED_BUILDING = 16,
	};

	enum class EPointCategory : std::uint16_t
	{
		UNDEFINED = 1,  //用户的类型图没有覆盖到的区域
		ROAD = 2,       
		BUILDING = 4,
		LANDSCAPE = 8,
		RIVER = 16,

		GROUND = 128,
		BUSH = 256,
		BOUDNARY_3 = 4096,  //在3x3邻域内有属于不同区域的点
		BOUDNARY_5 = 8192, //在5x5邻域内有属于不同区域的点
		BOUDNARY_7 = 16384, //在7x7邻域内有属于不同区域的点

		DISCARDED = 32768,
	};

	enum class EPointInfo : std::uint8_t
	{
		POSITION = 1,
		COLOR = 2,
		NORMAL = 4,
		RAIDUS = 8,
	};

	struct SRoad
	{
		std::vector<std::uint32_t> PixelArray;
		std::vector<std::uint32_t> KeyPointArray;
		
		std::int16_t IntersectionIndexA = -1;
		std::int16_t IntersectionIndexB = -1;
		float Width;
	};

	struct SRGBColor
	{
		std::uint8_t _r;
		std::uint8_t _g;
		std::uint8_t _b;

		bool operator==(const SRGBColor& s) const
		{
			return ((this->_r == s._r) && (this->_g == s._g) && (this->_b == s._b));
		}

		bool operator<(const SRGBColor& s) const
		{
			return (this->_r * 255 * 255 + this->_g * 255 + this->_b) < (s._r * 255 * 255 + s._g * 255 + s._b);
		}
	};

	struct SUnorganizedPointCloud  //用于存放从一个文件（PLY，OBJ...）中读入的点云数据
	{
		std::vector<Eigen::Vector3f> _PositionSet;
		std::vector<Eigen::Vector3f> _NormalSet;
		std::vector<SRGBColor>     _ColorSet;
		float _MaxLongitude = 0;
		float _MinLongitude = 0;
		float _MaxLatitude = 0;
		float _MinLatitude = 0;
		std::pair<Eigen::Vector3f, Eigen::Vector3f> _BoundingBox{ 
			Eigen::Vector3f(FLT_MAX, FLT_MAX, FLT_MAX), 
			Eigen::Vector3f(FLT_MAX, FLT_MAX, FLT_MAX)
		};
		
		void clear()
		{
			std::vector<Eigen::Vector3f>().swap(_PositionSet);
			std::vector<Eigen::Vector3f>().swap(_NormalSet);
			std::vector<SRGBColor>().swap(_ColorSet);
		}

		bool isValid() const
		{
			if (_PositionSet.empty()) return false;
			if (!_ColorSet.empty() && _ColorSet.size() != _PositionSet.size())
				return false;
			if (!_NormalSet.empty() && _NormalSet.size() != _PositionSet.size())
				return false;

			return true;
		}

		std::pair<Eigen::Vector3f, Eigen::Vector3f> getOrCalculateBoundingBox()
		{
			_ASSERTE(!_PositionSet.empty());
			if (_BoundingBox.first.x() != FLT_MAX) return _BoundingBox;
			std::pair<Eigen::Vector3f, Eigen::Vector3f> MinAndMax{ _PositionSet[0],_PositionSet[0] };
			for (const auto& e : _PositionSet)
			{
				if (e.x() < MinAndMax.first.x()) MinAndMax.first.x() = e.x();
				if (e.y() < MinAndMax.first.y()) MinAndMax.first.y() = e.y();
				if (e.z() < MinAndMax.first.z()) MinAndMax.first.z() = e.z();
				if (e.x() > MinAndMax.second.x()) MinAndMax.second.x() = e.x();
				if (e.y() > MinAndMax.second.y()) MinAndMax.second.y() = e.y();
				if (e.z() > MinAndMax.second.z()) MinAndMax.second.z() = e.z();
			}
			_BoundingBox = MinAndMax;
			return MinAndMax;
		}
	};

	struct SOrientedBB_2D
	{

	};

	struct SRegionDescriptor
	{
		EUserDefinedPointCatetory _Category;
		SRGBColor				  _RegionColor;
		std::uint32_t			  _SeedPoint;
	};

	struct SSimpleRoadRegionDescriptor : public SRegionDescriptor
	{
		std::vector<std::uint32_t> _Skeleton;
	};

	struct SSimpleRiverRegionDescriptor : public SRegionDescriptor
	{
		std::uint32_t _StartPoint;
		std::uint32_t _EndPoint;
	};

	struct SHeightMap
	{
		float* _pHeightMap = nullptr;
		std::uint32_t _DimX = 0;
		std::uint32_t _DimY = 0;
		std::pair<std::uint32_t, std::uint32_t> _CellCoordianteOfLeftDown;

		~SHeightMap() { delete _pHeightMap; }
	};

	const std::uint32_t INVALID_BLOCK_COORD = UINT_MAX;

	namespace MetafileKeywords
	{
		const std::string POINT_CLOUD_FILE_LOADER_SIG = "";
		const std::string CELL_SCALE = "Cell_Scale";
		const std::string WORLD_SCALE_X = "World_Scale_X";
		const std::string WORLD_SCALE_Y = "World_Scale_Y";
		const std::string OVERALL_SCENE_SIZE_IN_MB = "Overall_Scene_Size_In_MB";
		const std::string EXPECTED_BLOCK_SIZE_IN_MB = "Expected_Block_Size_In_MB";
		
		const std::string Metadata = "Metadata";
		const std::string RunningParameters = "RunningParameters";
		const std::string MAX_NUM_POINT_CLOUD_CONVERT_TASK = "Max_Num_Point_Cloud_Convert_Task";
		const std::string MAX_NUM_LOOP_WAIT_POINT_CLOUD_CONVERT_DONE = "Max_Num_Loop_Wait_Point_Cloud_Convert_Done";
		const std::string MAX_NUM_LOOP_VISIT_MEMORY = "MAX_NUM_LOOP_VISIT_MEMORY";
		const std::string UserProvidedCategory = "UserProvidedCategory";
		const std::string CategoryLegend = "CategoryLegend";
		const std::string CategoryNum = "CategoryNum";
		const std::string CategoryDescriptor = "CategoryDescriptor";
		const std::string Color = "Color";
		const std::string Description = "Description";
		const std::string CategoryFiles = "CategoryFiles";
		const std::string Directory = "Directory";
		const std::string FileNum = "FileNum";
		const std::string File = "File";
		const std::string Name = "Name";
		const std::string LongitudeLatitudeSpan = "LongitudeLatitudeSpan";
		const std::string Coordinates0 = "Coordinates0";
		const std::string Coordinates1 = "Coordinates1";
		const std::string Coordinates2 = "Coordinates2";
		const std::string Coordinates3 = "Coordinates3";
		const std::string TileFiles = "TileFiles";

		const std::string DetailedRegion = "DetailedRegion";
		const std::string MapLengend = "MapLengend";
		const std::string WorldMinMaxXY = "WorldMinMaxXY";
		const std::string WidthInPixel = "WidthInPixel";
		const std::string HeightInPixel = "HeightInPixel";
		const std::string ImageNum = "ImageNum";
		const std::string RoadRegion = "RoadRegion";
		const std::string SimpleRoadRegionNum = "SimpleRoadRegionNum";
		const std::string SimpleRoadRegionDescriptor = "SimpleRoadRegionDescriptor";
		const std::string VertexPosition = "VertexPosition";
		const std::string VertexNum = "VertexNum";
		const std::string Vertex = "Vertex";
		const std::string Coordinates = "Coordinates";
		const std::string SeedPoint = "SeedPoint";
		const std::string RiverdRegion = "RiverdRegion";
		const std::string SimpleRiverRegionNum = "SimpleRiverRegionNum";
		const std::string SimpleRiverRegionDescriptor = "SimpleRoadRegionDescriptor";
		const std::string LandscapeRegion = "LandscapeRegion";
		const std::string LandscapeRegionNum = "LandscapeRegionNum";
		const std::string LandscapeRegionDescriptor = "LandscapeRegionDescriptor";
		const std::string BuildingRegion ="BuildingRegion";
		const std::string BuildingRegionNum = "BuildingRegionNum";
		const std::string BuildingRegionDescriptor = "BuildingRegionDescriptor";

		const std::string PLY_LOADER_SIG = "ply";
		const std::string PCD_LOADER_SIG = "pcd";
		const std::string USER_DEFINED_CATEGORY_MAP_SIG = "UserDefinedCategoryMap";
		const std::string DETAILED_CATEGORY_MAP_SIG = "DetailedCategoryMap";
	}

	namespace MultilayeredSceneKeywords
	{
		const std::string CELL_DIM_X = "Cell_Dim_X";
		const std::string CELL_DIM_Y = "Cell_Dim_Y";
		const std::string BLOCK_DIM_X = "Block_Dim_X";
		const std::string BLOCK_DIM_Y = "Block_Dim_Y";
	}

	namespace Common  //在这个命名空间里封装一些和项目业务本身无关的一些函数，这些函数在未来将会被放入到hiveCommon中
	{
		const double EARTH_RADIUS = 6371.393;

		double getSystemTotalMemoryInMB();
		double getSystemFreeMemoryInMB(); 
		double degree2Radian(double vDegree);
		double calcDistance(double vLat1, double vLng1, double vLat2, double vLng2);
		__int64 getFolderSize(const std::wstring& vStrDir);
		std::wstring string2Wstring(std::string vStr);

		bool isSubconfigValid(const hiveConfig::CHiveConfig* vConfig, std::string vConfigType);
		std::pair<float, float> calcEntiretySpan(const hiveConfig::CHiveConfig* vConfig, std::pair<uint32_t, uint32_t> vDim);
	}
}