#include "pch.h"
#include "CategoryMap.h"

#define STB_IMAGE_STATIC
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

using namespace hiveObliquePhotography;

//*****************************************************************
//FUNCTION: 
std::vector<std::vector<std::vector<std::vector<SRGBColor>>>> ICategoryMap::_loadImageMatrix(const std::vector<std::tuple<std::string, std::uint32_t, std::uint32_t> >& vMapPathSet)
{
	std::uint32_t ImageRowNum = 0, ImageColNum = 0;
	for (int i = 0; i < vMapPathSet.size(); i++)
	{
		if (std::get<2>(vMapPathSet[i]) > ImageRowNum) ImageRowNum = std::get<2>(vMapPathSet[i]);//矩阵和图片坐标系不同，行列互换
		if (std::get<1>(vMapPathSet[i]) > ImageColNum) ImageColNum = std::get<1>(vMapPathSet[i]);
	}
	m_ImageRowNum = ++ImageRowNum;
	m_ImageColNum = ++ImageColNum;

	std::vector<std::vector<std::vector<std::vector<SRGBColor>>>> ImageMatrix;
	ImageMatrix.resize(ImageRowNum, std::vector<std::vector<std::vector<SRGBColor>>>(ImageColNum));

	try
	{
		for (int i = 0; i < vMapPathSet.size(); i++)
		{
			std::vector<std::vector<SRGBColor>> Image = __readImage(std::get<0>(vMapPathSet[i]));
			if (Image.size() <= 0)
			{
				_HIVE_OUTPUT_WARNING("Fail to execute [CUserDefinedCategoryMap::__readImage()].");
				ImageMatrix.resize(0);
				return ImageMatrix;
			}
			ImageMatrix[std::get<2>(vMapPathSet[i])][std::get<1>(vMapPathSet[i])] = Image;
		}
	}
	catch (...)
	{
		_HIVE_OUTPUT_WARNING("Fail to execute [CUserDefinedCategoryMap::__readImage()].");
		ImageMatrix.resize(0);
		return ImageMatrix;
	}

	return ImageMatrix;
}

//*****************************************************************
//FUNCTION: 
std::vector<std::vector<SRGBColor>> ICategoryMap::__readImage(const std::string& vImagePath)
{
	const char* Filepath = vImagePath.c_str();
	int ColNum, RowNum, BytesPerPixel;
	unsigned char* ImageData = stbi_load(Filepath, &ColNum, &RowNum, &BytesPerPixel, 0);

	std::vector<std::vector<SRGBColor> > Image;
	Image.resize(RowNum, std::vector<SRGBColor>(ColNum));
	for (int i = 0; i < RowNum; i++)
		for (int k = 0; k < ColNum; k++)
		{
			Image[i][k]._r = ImageData[(i * ColNum + k) * BytesPerPixel];
			Image[i][k]._g = ImageData[(i * ColNum + k) * BytesPerPixel + 1];
			Image[i][k]._b = ImageData[(i * ColNum + k) * BytesPerPixel + 2];
		}
	return Image;
}