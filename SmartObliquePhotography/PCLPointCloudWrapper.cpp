#include "pch.h"
#include "PCLPointCloudWrapper.h"

using namespace hiveObliquePhotography;

//*****************************************************************
//FUNCTION: 
bool CPCLPointCloudWrapper::extractDataFromUnorgnizedPointCloud(std::uint8_t vPointTypeFlag, SUnorganizedPointCloud* vUnorganizedPointCloud)
{
	_ASSERTE(vUnorganizedPointCloud->isValid());
	_HIVE_EARLY_RETURN(vUnorganizedPointCloud->_PositionSet.empty(), "Unorganized Point Cloud is empty", false);
	if (m_pPointCloudXYZ != nullptr) { pcl::PointCloud<pcl::PointXYZ>().swap(*m_pPointCloudXYZ);  m_pPointCloudXYZ = nullptr; }
	else if (m_pPointCloudXYZRGB != nullptr) { pcl::PointCloud<pcl::PointXYZRGB>().swap(*m_pPointCloudXYZRGB);  m_pPointCloudXYZRGB = nullptr; }
	else if (m_pPointCloudXYZRGBNormal != nullptr) { pcl::PointCloud<pcl::PointXYZRGBNormal>().swap(*m_pPointCloudXYZRGBNormal);  m_pPointCloudXYZRGBNormal = nullptr; }

	if (vPointTypeFlag == static_cast<std::uint8_t>(EPointInfo::POSITION))
	{
		m_pPointCloudXYZ = new pcl::PointCloud<pcl::PointXYZ>;

		for (const auto & Position : vUnorganizedPointCloud->_PositionSet)
			m_pPointCloudXYZ->points.push_back(pcl::PointXYZ(Position.x(), Position.y(), Position.z()));
		return true;
	}

	if (vPointTypeFlag == (static_cast<std::uint8_t>(EPointInfo::POSITION) | static_cast<std::uint8_t>(EPointInfo::COLOR)))
	{
		m_pPointCloudXYZRGB = new pcl::PointCloud<pcl::PointXYZRGB>;

		if (vUnorganizedPointCloud->_ColorSet.empty())
			for (int i = 0; i < vUnorganizedPointCloud->_PositionSet.size(); i++)
				m_pPointCloudXYZRGB->points.push_back(pcl::PointXYZRGB(
					vUnorganizedPointCloud->_PositionSet[i].x(), vUnorganizedPointCloud->_PositionSet[i].y(), vUnorganizedPointCloud->_PositionSet[i].z(),
					0, 0, 0));
		else
			for (int i = 0; i < vUnorganizedPointCloud->_PositionSet.size(); i++)
				m_pPointCloudXYZRGB->points.push_back(pcl::PointXYZRGB(
					vUnorganizedPointCloud->_PositionSet[i].x(), vUnorganizedPointCloud->_PositionSet[i].y(), vUnorganizedPointCloud->_PositionSet[i].z(),
					vUnorganizedPointCloud->_ColorSet[i]._r, vUnorganizedPointCloud->_ColorSet[i]._g, vUnorganizedPointCloud->_ColorSet[i]._b));		
		return true;
	}

	if (vPointTypeFlag == (static_cast<std::uint8_t>(EPointInfo::POSITION) | static_cast<std::uint8_t>(EPointInfo::NORMAL) | static_cast<std::uint8_t>(EPointInfo::COLOR)))
	{
		m_pPointCloudXYZRGBNormal = new pcl::PointCloud<pcl::PointXYZRGBNormal>;

		if (vUnorganizedPointCloud->_ColorSet.empty() && vUnorganizedPointCloud->_NormalSet.empty())
			for (int i = 0; i < vUnorganizedPointCloud->_PositionSet.size(); i++)
				m_pPointCloudXYZRGBNormal->points.push_back(pcl::PointXYZRGBNormal(
					vUnorganizedPointCloud->_PositionSet[i].x(), vUnorganizedPointCloud->_PositionSet[i].y(), vUnorganizedPointCloud->_PositionSet[i].z(),
					0, 0, 0, 0.f, 0.f, 0.f));
		else if (!vUnorganizedPointCloud->_ColorSet.empty() && vUnorganizedPointCloud->_NormalSet.empty())
			for (int i = 0; i < vUnorganizedPointCloud->_PositionSet.size(); i++)
				m_pPointCloudXYZRGBNormal->points.push_back(pcl::PointXYZRGBNormal(
					vUnorganizedPointCloud->_PositionSet[i].x(), vUnorganizedPointCloud->_PositionSet[i].y(), vUnorganizedPointCloud->_PositionSet[i].z(),
					vUnorganizedPointCloud->_ColorSet[i]._r, vUnorganizedPointCloud->_ColorSet[i]._g, vUnorganizedPointCloud->_ColorSet[i]._b,
					0.f, 0.f, 0.f));
		else if (vUnorganizedPointCloud->_ColorSet.empty() && !vUnorganizedPointCloud->_NormalSet.empty())
			for (int i = 0; i < vUnorganizedPointCloud->_PositionSet.size(); i++)
				m_pPointCloudXYZRGBNormal->points.push_back(pcl::PointXYZRGBNormal(
					vUnorganizedPointCloud->_PositionSet[i].x(), vUnorganizedPointCloud->_PositionSet[i].y(), vUnorganizedPointCloud->_PositionSet[i].z(),
					0, 0, 0,
					vUnorganizedPointCloud->_NormalSet[i].x(), vUnorganizedPointCloud->_NormalSet[i].y(), vUnorganizedPointCloud->_NormalSet[i].z()));
		else
			for (int i = 0; i < vUnorganizedPointCloud->_PositionSet.size(); i++)
				m_pPointCloudXYZRGBNormal->points.push_back(pcl::PointXYZRGBNormal(
					vUnorganizedPointCloud->_PositionSet[i].x(), vUnorganizedPointCloud->_PositionSet[i].y(), vUnorganizedPointCloud->_PositionSet[i].z(),
					vUnorganizedPointCloud->_ColorSet[i]._r, vUnorganizedPointCloud->_ColorSet[i]._g, vUnorganizedPointCloud->_ColorSet[i]._b,
					vUnorganizedPointCloud->_NormalSet[i].x(), vUnorganizedPointCloud->_NormalSet[i].y(), vUnorganizedPointCloud->_NormalSet[i].z()));			
		return true;
	}

	_HIVE_OUTPUT_WARNING("Fail to build PCL point cloud due to unknown point type flag.");
	return false;
}