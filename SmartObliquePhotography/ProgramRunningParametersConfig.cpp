#include "pch.h"
#include "ProgramRunningParametersConfig.h"

bool hiveObliquePhotography::CProgramRunningParametersConfig::init()
{
    _ASSERTE(!m_IsReady);

    const hiveConfig::CHiveConfig* pConfig = getSubconfigAt(0);
    if (!Common::isSubconfigValid(pConfig, std::string("RunningParameters")))return false;

    m_IsReady = true;
    return true;
}

void hiveObliquePhotography::CProgramRunningParametersConfig::__defineAttributesV()
{
    _defineAttribute(MetafileKeywords::RunningParameters, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
    _defineAttribute(MetafileKeywords::MAX_NUM_POINT_CLOUD_CONVERT_TASK, hiveConfig::EConfigDataType::ATTRIBUTE_INT);
    _defineAttribute(MetafileKeywords::MAX_NUM_LOOP_WAIT_POINT_CLOUD_CONVERT_DONE, hiveConfig::EConfigDataType::ATTRIBUTE_INT);
    _defineAttribute(MetafileKeywords::MAX_NUM_LOOP_VISIT_MEMORY, hiveConfig::EConfigDataType::ATTRIBUTE_INT);
    _defineAttribute(MetafileKeywords::EXPECTED_BLOCK_SIZE_IN_MB, hiveConfig::EConfigDataType::ATTRIBUTE_FLOAT);
}