#include "pch.h"
#include "UserDefinedCategoryMapConfig.h"

bool hiveObliquePhotography::CUserDefinedCategoryMapConfig::init()
{
	_ASSERTE(!m_IsReady);

	const hiveConfig::CHiveConfig* pConfig = getSubconfigAt(0);
	if (!Common::isSubconfigValid(pConfig, std::string("UserProvidedCategory")))return false;

	const hiveConfig::CHiveConfig* pCategoryLegendConfig = pConfig->getSubconfigAt(0);
	if (!Common::isSubconfigValid(pCategoryLegendConfig, std::string("CategoryLegend")))return false;

	std::optional<int> MapCategoryNum = pCategoryLegendConfig->getAttribute<int>("CategoryNum");
	_ASSERTE(MapCategoryNum.has_value());
	for (int i = 0; i < MapCategoryNum.value(); i++)
	{
		const hiveConfig::CHiveConfig* pMapCategoryConfig = pCategoryLegendConfig->getSubconfigAt(i);
		if (!Common::isSubconfigValid(pMapCategoryConfig, std::string("CategoryDescriptor")))return false;

		std::optional<std::string> DescriptionStr = pMapCategoryConfig->getAttribute<std::string>("Description");
		_ASSERTE(DescriptionStr.has_value());
		std::map<std::string, int> DescriptionMap;
		DescriptionMap.insert(std::map<std::string, int>::value_type("UNDEFINED", 1));
		DescriptionMap.insert(std::map<std::string, int>::value_type("GROUND", 2));
		DescriptionMap.insert(std::map<std::string, int>::value_type("ROAD", 4));
		DescriptionMap.insert(std::map<std::string, int>::value_type("BUILDING", 8));
		DescriptionMap.insert(std::map<std::string, int>::value_type("BUSH", 16));
		DescriptionMap.insert(std::map<std::string, int>::value_type("RIVER", 32));

		std::optional<std::string> Color = pMapCategoryConfig->getAttribute<std::string>("Color");
		_ASSERTE(Color.has_value());

		std::stringstream String2Double;
		SRGBColor TempColor;
		String2Double << Color.value();
		int r, g, b;
		String2Double >> r >> g >> b;
		TempColor._r = r;
		TempColor._g = g;
		TempColor._b = b;
		m_MapCategory.insert(std::pair<SRGBColor, EUserDefinedPointCatetory>(TempColor, (EUserDefinedPointCatetory)DescriptionMap.at(DescriptionStr.value())));
	}

	const hiveConfig::CHiveConfig* pBitmapConfig = pConfig->getSubconfigAt(1);
	if (!Common::isSubconfigValid(pBitmapConfig, std::string("CategoryFiles")))return false;

	std::optional<std::string> BitmapDirectory = pBitmapConfig->getAttribute<std::string>("Directory");
	_ASSERTE(BitmapDirectory.has_value());
	std::optional<int> BitmapNum = pBitmapConfig->getAttribute<int>("FileNum");
	_ASSERTE(BitmapNum.has_value());

	std::stringstream String2Double;
	std::array<double, 8> PreCoordinates;
	PreCoordinates.fill(1000);
	std::uint32_t TileXIndex = 0, TileYIndex = -1;
	for (int i = 0; i < BitmapNum.value(); i++)
	{
		const hiveConfig::CHiveConfig* pTempConfig = pBitmapConfig->getSubconfigAt(i);
		if (!Common::isSubconfigValid(pTempConfig, std::string("File")))return false;

		std::optional<std::string> BitmapName = pTempConfig->getAttribute<std::string>("Name");
		_ASSERTE(BitmapName.has_value());

		pTempConfig = pTempConfig->getSubconfigAt(0);
		if (!Common::isSubconfigValid(pTempConfig, std::string("LongitudeLatitudeSpan")))return false;

		std::array<double, 8> Coordinates;
		for (int k = 0; k < 4; k++)
		{
			std::optional<std::string> CoordinatesString = pTempConfig->getAttribute<std::string>(std::string("Coordinates").append(std::to_string(k)));
			_ASSERTE(CoordinatesString.has_value());
			String2Double.clear();
			String2Double << CoordinatesString.value();
			String2Double >> Coordinates[k * 2] >> Coordinates[k * 2 + 1];
		}
		for (int k = 0; k < Coordinates.size(); k++)
		{
			Coordinates[k] = (double)(long(Coordinates[k] * 1e6)) / 1e6;
		}

		std::tuple<std::string, std::uint32_t, std::uint32_t> tempMap;
		std::get<0>(tempMap) = BitmapDirectory.value() + BitmapName.value();

		if (Coordinates[0] == PreCoordinates[6] && Coordinates[1] == PreCoordinates[7]
			&& Coordinates[2] == PreCoordinates[4] && Coordinates[3] == PreCoordinates[5])
		{
			std::get<1>(tempMap) = ++TileXIndex;
			std::get<2>(tempMap) = TileYIndex;
		}
		else
		{
			std::get<1>(tempMap) = TileXIndex = 0;
			std::get<2>(tempMap) = ++TileYIndex;
		}
		m_MapPathSet.emplace_back(tempMap);
		PreCoordinates = Coordinates;
	}
	m_BitmapDim = std::make_pair(++TileXIndex, ++TileYIndex);
	m_BitmapSpan = Common::calcEntiretySpan(pBitmapConfig, m_BitmapDim);

	m_IsReady = true;
	return true;
}

void hiveObliquePhotography::CUserDefinedCategoryMapConfig::__defineAttributesV()
{
	_defineAttribute(MetafileKeywords::MAX_NUM_LOOP_VISIT_MEMORY, hiveConfig::EConfigDataType::ATTRIBUTE_INT);
	_defineAttribute(MetafileKeywords::UserProvidedCategory, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute(MetafileKeywords::CategoryLegend, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute(MetafileKeywords::CategoryNum, hiveConfig::EConfigDataType::ATTRIBUTE_INT);
	_defineAttribute(MetafileKeywords::CategoryDescriptor, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute(MetafileKeywords::Color, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::Description, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::CategoryFiles, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute(MetafileKeywords::Directory, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::FileNum, hiveConfig::EConfigDataType::ATTRIBUTE_INT);
	_defineAttribute(MetafileKeywords::File, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute(MetafileKeywords::Name, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::LongitudeLatitudeSpan, hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute(MetafileKeywords::Coordinates0, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::Coordinates1, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::Coordinates2, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute(MetafileKeywords::Coordinates3, hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
}
