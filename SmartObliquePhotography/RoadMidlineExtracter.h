#pragma once
#include <opencv2/opencv.hpp>
#include "CategoryMap.h"
#include "SmartOPCommon.h"

namespace hiveObliquePhotography
{
	class CRoadMidlineExtracter : public ICategoryMap
	{
	public:
		CRoadMidlineExtracter() = default;
		~CRoadMidlineExtracter() = default;

#ifdef _UNIT_TEST
		bool initParameters(float vLongitudeSpan, float vLatitudeSpan, std::uint32_t vWidthInPixel = 512, std::uint32_t vHeightInPixel = 512, int vImageRowNum = 1, int vImageColNum = 1)
		{
			m_WidthInPixel = vWidthInPixel;
			m_HeightInPixel = vHeightInPixel;
			m_ImageRowNum = vImageRowNum;
			m_ImageColNum = vImageColNum;
			m_XWorldSpanPerPixel = vLongitudeSpan / m_WidthInPixel;
			m_YWorldSpanPerPixel = vLatitudeSpan / m_HeightInPixel;
			return true;
		}
		std::vector<bool> generateSkeletonMap(const std::vector<bool>& vBinaryMap) { return __generateSkeletonMap(vBinaryMap); }
		std::vector<SRoad> divideRoadSkeletonByIntersection(std::vector<bool>& vRoadSkeletonMap, int vShiftSize = 1) { return __divideRoadSkeletonByIntersection(vRoadSkeletonMap, vShiftSize); }
		void findIntersections(const std::vector<bool>& vRoadSkeletonMap, int vShiftSize = 1) { __findIntersections(vRoadSkeletonMap, vShiftSize); }
		bool findIntersection2Roads(std::vector<bool>& vRoadSkeletonMap, std::vector<SRoad>& vioRoads, int vMaxDeadendLength = -1) { return __findIntersection2Roads(vRoadSkeletonMap, vioRoads, vMaxDeadendLength); }
		bool findIntersection2Roads_arcmap(std::vector<SRoad>& vioRoads) { return __findIntersection2Roads_arcmap(vioRoads); }
		bool addIntersection2Roads(std::vector<SRoad>& vioRoads) { return __addIntersection2Roads(vioRoads); }
		bool simplifyPixelArray(SRoad& vioRoad, float vFittingScale) { return __simplifyPixelArray(vioRoad, vFittingScale); }
		bool simplifyKeyPointArray(SRoad& vioRoad, float vFittingScale) { return __simplifyKeyPointArray(vioRoad, vFittingScale); }
		bool estimateRoadWidth(const std::vector<bool>& vBinaryMap, std::vector<SRoad>& vioRoads, double vMagicNumber) { return __estimateRoadWidth(vBinaryMap, vioRoads, vMagicNumber); }
		bool mergeSubRoads(std::vector<SRoad>& vioRoads) { return __mergeSubRoads(vioRoads); }
		bool extendBrokenRoads(std::vector<bool>& vRoadSkeletonMap, std::vector<SRoad>& vioRoads, int vMaxStep = 5) { return __extendBrokenRoads(vRoadSkeletonMap, vioRoads, vMaxStep); }
		bool deleteDeadendRoads(std::vector<SRoad>& vioRoads, double vMaxDeadendLength) { return __deleteDeadendRoads(vioRoads, vMaxDeadendLength); }
		std::vector<std::tuple<uint32_t, uint32_t>> getIntersectionSet() { return m_IntersectionSet; }
#endif
	private:
		std::vector<std::tuple<uint32_t, uint32_t>> m_IntersectionSet;
		int m_DeleteWindowShiftSize;
		
	private:
		[[nodiscard]] bool __initParameters(Eigen::Vector2i vSizeInPixel, Eigen::Vector4f vWorldBoundingBox);
		std::vector<bool> __generateSkeletonMap(const std::vector<bool>& vBinaryMap);
		std::vector<SRoad> __divideRoadSkeletonByIntersection(std::vector<bool>& vRoadSkeletonMap, int vShiftSize = 1);
		bool __mergeSubRoads(std::vector<SRoad>& vioRoads);
		bool __mergeTwoRoads(std::vector<SRoad>& vioRoads, int FirstIndex, int SecondIndex);
		bool __judgeCollinear(std::vector<SRoad>& vioRoads, int FirstIndex, int SecondIndex, bool isFirstStart, bool isSecondStart);
		bool __addIntersection2Roads(std::vector<SRoad>& vioRoads);
		bool __deleteDeadendRoads(std::vector<SRoad>& vioRoads, double vMaxDeadendLength);
		bool __extendBrokenRoads(const std::vector<bool>& vMidlineMap, std::vector<SRoad>& vioRoads, int vMaxStep = 5);
		bool __extendSubRoad(const std::vector<bool>& vMidlineMap, SRoad& vioRoad, bool isForward, int vMaxStep);
		bool __mergeIntersections(std::vector<std::tuple<uint32_t, uint32_t>>& intersectionSet, double vMergeRange);
		bool __findIntersection2Roads(const std::vector<bool>& vRoadSkeletonMap, std::vector<SRoad>& vioRoads, int vMaxDeadendLength = -1);
		bool __findIntersection2Roads_arcmap(std::vector<SRoad>& vioRoads);
		bool __estimateRoadWidth(const std::vector<bool>& vBinaryMap, std::vector<SRoad>& vioRoads, double vMagicNumber);
		std::vector<std::vector<uint32_t>> __dividePixelsByRegionGrow(std::vector<bool>& vBinaryMap);
		std::vector<std::vector<uint32_t>> __dividePixelsByOpenCV(const cv::Mat& vBinaryMap);
		bool __simplifyPixelArray(SRoad& vioRoad, float vFittingScale);
		bool __simplifyKeyPointArray(SRoad& vioRoad, float vFittingScale);
		float __calcPoint2LineDistance(const Eigen::Vector2f vPixelCoord, const Eigen::Vector2f vLineEndPointA, const Eigen::Vector2f vLineEndPointB, bool vIsSegmentLine);
		void __addPolylinePoint(int vStart, int vEnd, float vError, std::vector<uint32_t>& vPolyline, const std::vector<uint32_t>& vPixelArray);
		void __sort2DArray(std::vector<uint32_t>& vio2DArray);
		void __sortLineArray(std::vector<uint32_t>& vioLineArray);
		void __sortKeyPointsByPixelArray(std::vector<uint32_t>& vioKeyPointArray, const std::vector<uint32_t>& vPixelArray);
		void __calcEightNeighbourhood(const int vX, const int vY, std::vector<bool>& voEightNeighbourhood, const std::uint8_t* vPixel, const cv::Mat& vRoadSkeletonMap);
		void __findPixel4Delete(const int vX, const int vY, const int vflag, const std::vector<bool>& vEightNeighbourhood, const cv::Mat& vRoadSkeletonMap, std::uint8_t* vPixel, std::vector<std::uint8_t*>& voDeleteSet);
		void __doRegionGrow(const uint32_t vSeedCoord, std::vector<bool>& vioVisitedMap, std::vector<uint32_t>& voConnectedPixels);
		std::vector<uint32_t> __getNeighbourhood(const uint32_t vOffset, const std::vector<bool>& vVisitedMap, std::vector<uint32_t>& voSinglePixelSet);
		void __findIntersections(const std::vector<bool>& vRoadSkeletonMap, int vShiftSize = 1);
	};
}