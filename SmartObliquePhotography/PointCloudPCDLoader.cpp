#include "pch.h"
#include "PointCloudPCDLoader.h"
#include "PCLPointCloudWrapper.h"

using namespace hiveObliquePhotography;

_REGISTER_EXCLUSIVE_PRODUCT(CPointCloudPCDLoader, MetafileKeywords::PCD_LOADER_SIG)

//*****************************************************************
//FUNCTION: 
SUnorganizedPointCloud* CPointCloudPCDLoader::__loadV(const std::string& vFileName)
{
	pcl::PointCloud<pcl::PointXYZRGBNormal>* pPointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
	pcl::io::loadPCDFile<pcl::PointCloud<pcl::PointXYZRGBNormal>::PointType>(vFileName, *pPointCloud);

	SUnorganizedPointCloud* pUnorganizedPointCloud = new SUnorganizedPointCloud;
	CPCLPointCloudWrapper PCLPointCloudWrapper;
	bool IsSuccess = PCLPointCloudWrapper.dumpData2UnorgnizedPointCloud<pcl::PointXYZRGBNormal>(pPointCloud, pUnorganizedPointCloud);

	delete pPointCloud;
	_HIVE_EARLY_RETURN(!IsSuccess, _FORMAT_STR1("Fail to dump pcl point cloud to unorgnized point cloud [%1%].", vFileName), nullptr);
	return pUnorganizedPointCloud;
}