#pragma once
#include "SmartOPExport.h"

namespace hiveObliquePhotography
{
	class CMultiLayeredPointCloud;

	//全过程处理，所有输入输出信息都由vRawPointcloudMetafileName所指定的文件提供
	SMARTOP_DECLSPEC bool hiveExecuteCompleteFlow(const std::string& vRawPointcloudMetafileName, const std::string& vUserDefinedCategoryMetafileName, const std::string& vProgramRunningParameterConfigFileName);

	//读入vRawPointcloudMetafileName所指定文件提供的原始点云，合并成一个大的点云数据，并以新的格式进行分块存储，分块的索引文件由vBlockIndexFileName指定
	SMARTOP_DECLSPEC bool hiveConvertRawPointCloud2MultiLayered(const std::string& vRawPointcloudMetafileName, const std::string& vUserDefinedCategoryMetafileName, const std::string& vProgramRunningParameterConfigFileName, const std::string& vMultiLayeredPointCloudMetafileName);


	SMARTOP_DECLSPEC bool hiveGenerateDetailedCategoryMap(const std::string& vUserDefinedCategoryMapMetafileName, const std::string& vDetailedCategoryMapMetafileName);
	
	//点云分类
	SMARTOP_DECLSPEC bool hiveSegmentPointCloud(const std::string& vMultiLayeredPointCloudMetafileName, const std::string& vDetailedCategoryMetafileName, const std::string& vProgramRunningParameterConfigFileName);

	//
	SMARTOP_DECLSPEC bool hiveHandleDiscardedRegion(const std::string& vMultiLayeredPointCloudMetafileName);

	bool _hiveParseRawPointCloudMetafile(const std::string& vFileName);  // -> UTest_001
	bool _hiveParseUserDefinedCategoryMetafile(const std::string& vFileName);
	bool _hiveParseDetailedCategoryMapMetafile(const std::string& vFileName);
	bool _hiveParseProgramRunningParameterConfigFile(const std::string& vFileName);
}