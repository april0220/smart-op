#include "pch.h"
#include "SmartOPInterface.h"
#include "RoadMidlineExtracter.h"
#include "OverallConfig.h"
#include "ProgramRunningParametersConfig.h"
#include "PointCloudFileLoader.h"

using namespace hiveObliquePhotography;

//*****************************************************************
//FUNCTION: 
bool hiveObliquePhotography::_hiveParseRawPointCloudMetafile(const std::string& vFileName)
{
	_HIVE_EARLY_RETURN(!hiveUtility::hiveFileSystem::hiveIsFileExisted(vFileName), _FORMAT_STR1("The specified metafile [%1%] does not exist.", vFileName), false);

	try
	{
		_HIVE_EARLY_RETURN((hiveConfig::hiveParseConfig(vFileName, hiveConfig::EConfigType::XML, CRawPointCloudConfig::getInstance()) != hiveConfig::EParseResult::SUCCEED),
			_FORMAT_STR1("Fail to parse the specified metafile [%1%].", vFileName), false);
		_HIVE_EARLY_RETURN(!CRawPointCloudConfig::getInstance()->init(),
			_FORMAT_STR1("The information provided by the specified metafile [%1%] is not enough to generate scene information.", vFileName), false);
		return true;
	}
	catch (...)
	{
		_HIVE_OUTPUT_WARNING("Fail to execute _hiveParseRawPointCloudMetafile() due to unexpected error");
		return false;
	}
}

//*****************************************************************
//FUNCTION: 
bool hiveObliquePhotography::_hiveParseProgramRunningParameterConfigFile(const std::string& vFileName)
{
	_HIVE_EARLY_RETURN(!hiveUtility::hiveFileSystem::hiveIsFileExisted(vFileName), _FORMAT_STR1("The specified metafile [%1%] does not exist.", vFileName), false);

	try
	{
		_HIVE_EARLY_RETURN((hiveConfig::hiveParseConfig(vFileName, hiveConfig::EConfigType::XML, CProgramRunningParametersConfig::getInstance()) != hiveConfig::EParseResult::SUCCEED),
			_FORMAT_STR1("Fail to parse the specified metafile [%1%].", vFileName), false);
		_HIVE_EARLY_RETURN(!CProgramRunningParametersConfig::getInstance()->init(),
			_FORMAT_STR1("The information provided by the specified metafile [%1%] is not enough.", vFileName), false);
		return true;
	}
	catch (...)
	{
		_HIVE_OUTPUT_WARNING("Fail to execute _hiveParseProgramRunningParameterConfigFile() due to unexpected error");
		return false;
	}
}