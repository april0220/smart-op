#pragma once
#include "common/Singleton.h"
#include "common/HiveConfig.h"
#include <vector>

struct STile
{
	std::string FileName;
	std::uint32_t TileX, TileY;
	float LatitudeSpan, LongitudeSpan;
};

namespace hiveObliquePhotography
{
	class CRawPointCloudConfig : public hiveDesignPattern::CSingleton<CRawPointCloudConfig>, public hiveConfig::CHiveConfig
	{
	public:
		~CRawPointCloudConfig() = default;
		[[nodiscard]] bool init();
		bool isReady() const { return m_IsReady; }

		std::tuple<std::string, std::uint32_t, std::uint32_t> getFirstPointCloudTileFile();    //这里返回值使用std::string而不是const std::string&，是希望通过放回empty std::string表明文件遍历结束
		std::tuple<std::string, std::uint32_t, std::uint32_t> getNextPointCloudTileFile();

		std::uint32_t getNumPointCloudTile() const { return m_TileSet.size(); };
		std::pair<uint32_t, uint32_t> getTileDim() const{ return m_TileDim; }
		std::pair<float, float> getSceneSpan() const { return m_SceneSpan; }
		std::vector<std::pair<float, float>> getTileLongitudeLatitudeSpanSet() const { return m_TileLongitudeLatitudeSpanSet; }

	private:
		CRawPointCloudConfig() = default;
		virtual void __defineAttributesV() override;
		virtual void __onLoadedV() override;
		void __CalcTileLongitudeLatitudeSpanSet();

		bool m_IsReady = false;

		std::vector<STile> m_TileSet;
		std::pair<float, float> m_SceneSpan;
		std::vector<std::pair<float, float>> m_TileLongitudeLatitudeSpanSet;
		std::uint32_t m_TileSetIndex = 1;
		std::pair<uint32_t, uint32_t> m_TileDim;

		friend class hiveDesignPattern::CSingleton<CRawPointCloudConfig>;
	};
}

