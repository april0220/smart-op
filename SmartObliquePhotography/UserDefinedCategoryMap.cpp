#include "pch.h"
#include "UserDefinedCategoryMap.h"

using namespace hiveObliquePhotography;

_REGISTER_EXCLUSIVE_PRODUCT(CUserDefinedCategoryMap, MetafileKeywords::USER_DEFINED_CATEGORY_MAP_SIG)

//*****************************************************************
//FUNCTION: 
bool CUserDefinedCategoryMap::load(float vLongitudeSpan, float vLatitudeSpan, const std::pair<float, float>& vWorldOrigin,
	const std::vector<std::tuple<std::string, std::uint32_t, std::uint32_t> >& vMapPathSet, const std::map<SRGBColor, EUserDefinedPointCatetory>& vColor2CategoryMap)
{
	if (vLongitudeSpan <= 0 || vLatitudeSpan <= 0 || vMapPathSet.empty() || vColor2CategoryMap.empty())
		return false;
	m_Color2CategoryMap = vColor2CategoryMap;

	std::vector<std::vector<std::vector<std::vector<SRGBColor>>>> ImageMatrix = _loadImageMatrix(vMapPathSet);
	if (!_initCategoryMap(ImageMatrix)) return false;
	if (__initParameters(vLongitudeSpan, vLatitudeSpan, vWorldOrigin, ImageMatrix)) m_IsReady = true;

	return m_IsReady;
}

//*****************************************************************
//FUNCTION: 
bool CUserDefinedCategoryMap::__initParameters(float vLongitudeSpan, float vLatitudeSpan, const std::pair<float, float>& vWorldOriginCoordinate,
	const std::vector<std::vector<std::vector<std::vector<SRGBColor>>>> vImageMatrix)
{
	if (vImageMatrix.size() <= 0)
		return false;

	m_MinWorldX = vWorldOriginCoordinate.first;
	m_MinWorldY = vWorldOriginCoordinate.second;
	m_MaxWorldX = vWorldOriginCoordinate.first + vLongitudeSpan;
	m_MaxWorldY = vWorldOriginCoordinate.second + vLatitudeSpan;

	m_WidthInPixel = 0;
	m_HeightInPixel = 0;
	for (int i = 0; i < vImageMatrix[0].size(); i++)
	{
		m_WidthInPixel += vImageMatrix[0][i][0].size();
	}
	for (int i = 0; i < vImageMatrix.size(); i++)
	{
		m_HeightInPixel += vImageMatrix[i][0].size();
	}

	m_XWorldSpanPerPixel = vLongitudeSpan / m_WidthInPixel;
	m_YWorldSpanPerPixel = vLatitudeSpan / m_HeightInPixel;

	return true;
}

//*****************************************************************
//FUNCTION: 
void hiveObliquePhotography::CUserDefinedCategoryMap::_addColor2CategoryMap(SRGBColor vColor)
{
	EUserDefinedPointCatetory Category = EUserDefinedPointCatetory::USER_UNDEFINED;
	if (m_Color2CategoryMap.find(vColor) != m_Color2CategoryMap.end())
		Category = m_Color2CategoryMap.find(vColor)->second;
	m_UserDefinedCategoryMap.push_back(Category);
}

//*****************************************************************
//FUNCTION: 按最近邻采样方式来获取category，如果用户指定的经纬度超出范围，则返回EPointCategory::Undefined
EUserDefinedPointCatetory CUserDefinedCategoryMap::getCategory(float vWorldX, float vWorldY)
{
	_ASSERTE(m_XWorldSpanPerPixel > 0 && m_YWorldSpanPerPixel > 0 && m_WidthInPixel > 0 && m_HeightInPixel > 0);
	_HIVE_SIMPLE_IF(vWorldX > m_MaxWorldX || vWorldX < m_MinWorldX || vWorldY > m_MaxWorldY || vWorldY < m_MinWorldY,
		return EUserDefinedPointCatetory::USER_UNDEFINED);

	std::uint32_t Division2Int = static_cast<std::uint32_t>((vWorldX - m_MinWorldX) / m_XWorldSpanPerPixel);
	std::uint32_t PixcelIndex4X = ((vWorldX - Division2Int * m_XWorldSpanPerPixel) == 0) ? (std::max((int)Division2Int - 1, 0)) : Division2Int;
	Division2Int = static_cast<std::uint32_t>((vWorldY - m_MinWorldY) / m_YWorldSpanPerPixel);
	std::uint32_t PixcelIndex4Y = ((vWorldY - Division2Int * m_YWorldSpanPerPixel) == 0) ? (std::max((int)Division2Int - 1, 0)) : Division2Int;
	auto Offset = _OFFSET_2D(PixcelIndex4X, PixcelIndex4Y, m_WidthInPixel);

	return m_UserDefinedCategoryMap[Offset];
} 

//*****************************************************************
//FUNCTION: 
void CUserDefinedCategoryMap::dumpUserDefinedCategoryMap(std::vector<EUserDefinedPointCatetory>& voUserDefinedCategoryMap)
{
	if (voUserDefinedCategoryMap.size()) voUserDefinedCategoryMap.clear();
	voUserDefinedCategoryMap = m_UserDefinedCategoryMap;
}