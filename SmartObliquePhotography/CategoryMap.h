#pragma once

namespace hiveObliquePhotography
{
	class ICategoryMap : public hiveDesignPattern::IProduct
	{
	public:
		ICategoryMap() = default;
		~ICategoryMap() = default;

		[[nodiscard]] bool isReady() { return m_IsReady; }
		std::pair<float, float> getWorldSpanPerPixel() const { return std::make_pair(m_XWorldSpanPerPixel, m_YWorldSpanPerPixel); }
		std::pair<std::uint32_t, std::uint32_t> getMapDim() const { return std::make_pair(m_WidthInPixel, m_HeightInPixel); }
		void setMapDim(const std::pair<std::uint32_t, std::uint32_t> vMapDim) { 
			m_WidthInPixel = vMapDim.first; 
			m_HeightInPixel = vMapDim.second;
		}
	
	protected:
		bool m_IsReady = false;
		float m_MinWorldX = 0;
		float m_MinWorldY = 0;
		float m_MaxWorldX = 0;
		float m_MaxWorldY = 0;
		std::uint32_t m_WidthInPixel = 0;
		std::uint32_t m_HeightInPixel = 0;
		float m_XWorldSpanPerPixel = 0;
		float m_YWorldSpanPerPixel = 0;
		std::uint32_t m_ImageRowNum = 0;
		std::uint32_t m_ImageColNum = 0;

		std::vector<std::vector<std::vector<std::vector<SRGBColor>>>> _loadImageMatrix(const std::vector<std::tuple<std::string, std::uint32_t, std::uint32_t> >& vMapPathSet);

	private:
		std::vector<std::vector<SRGBColor>> __readImage(const std::string& vImagePath);		
	};
}

