#pragma once
#include "PointCloudFileLoader.h"

namespace hiveObliquePhotography
{
	class CPointCloudPCDLoader : public IPointCloudFileLoader
	{
	private:
		SUnorganizedPointCloud* __loadV(const std::string& vFileName) override;
	};
}
