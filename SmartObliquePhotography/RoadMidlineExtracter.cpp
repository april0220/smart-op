#include "pch.h"
#include <random>
#include <ctime>
#include <algorithm>
#include "RoadMidlineExtracter.h"
#include "DefineMemberChecker.h"

_DEFINE_MEMBER_CHECKER(_Skeleton)
_DEFINE_MEMBER_CHECKER(_EndPoint)
using namespace hiveObliquePhotography;
_REGISTER_EXCLUSIVE_PRODUCT(CRoadMidlineExtracter, MetafileKeywords::DETAILED_CATEGORY_MAP_SIG)

//*****************************************************************
//FUNCTION: 
bool CRoadMidlineExtracter::__initParameters(Eigen::Vector2i vSizeInPixel, Eigen::Vector4f vWorldBoundingBox)
{
	if (vWorldBoundingBox[0] < vWorldBoundingBox[2] && vWorldBoundingBox[1] < vWorldBoundingBox[3] && vSizeInPixel[0] > 0 && vSizeInPixel[1] > 0)
	{
		m_MinWorldX = vWorldBoundingBox[0];
		m_MinWorldY = vWorldBoundingBox[1];
		m_MaxWorldX = vWorldBoundingBox[2];
		m_MaxWorldY = vWorldBoundingBox[3];
		m_WidthInPixel = vSizeInPixel[0];
		m_HeightInPixel = vSizeInPixel[1];
		m_XWorldSpanPerPixel = (m_MaxWorldX - m_MinWorldX) / m_WidthInPixel;
		m_YWorldSpanPerPixel = (m_MaxWorldY - m_MinWorldY) / m_HeightInPixel;
		return true;
	}
	else
	{
		hiveEventLogger::hiveOutputEvent("Wrong parameters.");
		return false;
	}
}

//*****************************************************************
//FUNCTION:
void CRoadMidlineExtracter::__addPolylinePoint(int vStart, int vEnd, float vFittingScale, std::vector<uint32_t>& vioPolyline, const std::vector<uint32_t>& vPixelArray)
{
	_ASSERTE(!vPixelArray.empty() && vStart >= 0 && vEnd < vPixelArray.size() && vFittingScale > 0);
	Eigen::Vector2f EndPointA = { (float)(vPixelArray[vStart] / m_WidthInPixel), (float)(vPixelArray[vStart] % m_WidthInPixel) };
	Eigen::Vector2f EndPointB = { (float)(vPixelArray[vEnd] / m_WidthInPixel), (float)(vPixelArray[vEnd] % m_WidthInPixel) };
	Eigen::Vector2f PixelCoord;
	float Distance;
	float MaxDistance = 0;
	int NewPointIndex = 0;
	if (vEnd - vStart != 1)
		for (int i = vStart + 1; i < vEnd; i++)
		{
			PixelCoord = { (float)(vPixelArray[i] / m_WidthInPixel), (float)(vPixelArray[i] % m_WidthInPixel) };
			Distance = __calcPoint2LineDistance(PixelCoord, EndPointA, EndPointB, false);
			if (Distance > MaxDistance)
			{
				MaxDistance = Distance;
				NewPointIndex = i;
			}
		}
	//float Error = std::sqrt((Eigen::Vector2i(vPixelArray[vEnd] / m_WidthInPixel, vPixelArray[vEnd] % m_WidthInPixel) - 
	//	Eigen::Vector2i(vPixelArray[vStart] / m_WidthInPixel, vPixelArray[vStart] % m_WidthInPixel)).squaredNorm()) * vFittingScale;
	//Error = max(Error, vFittingScale);

	if (MaxDistance > vFittingScale)
	{
		//vioPolyline.insert(vioPolyline.begin() + vStart, vPixelArray[NewPointIndex]);
		vioPolyline.push_back(vPixelArray[NewPointIndex]);
		__addPolylinePoint(vStart, NewPointIndex, vFittingScale, vioPolyline, vPixelArray);
		__addPolylinePoint(NewPointIndex, vEnd, vFittingScale, vioPolyline, vPixelArray);
	}
};

//*****************************************************************
//FUNCTION:
void CRoadMidlineExtracter::__sort2DArray(std::vector<uint32_t>& vio2DArray)
{
	std::uint32_t MinX = UINT32_MAX, MaxX = 0, MinY = UINT32_MAX, MaxY = 0;
	for (int i = 0; i < vio2DArray.size(); i++)
	{
		if (vio2DArray[i] / m_WidthInPixel < MinX) MinX = vio2DArray[i] / m_WidthInPixel;
		if (vio2DArray[i] / m_WidthInPixel > MaxX) MaxX = vio2DArray[i] / m_WidthInPixel;
		if (vio2DArray[i] % m_WidthInPixel < MinY) MinY = vio2DArray[i] % m_WidthInPixel;
		if (vio2DArray[i] % m_WidthInPixel > MaxY) MaxY = vio2DArray[i] % m_WidthInPixel;
	}

	bool isSortByX = (MaxX - MinX) > (MaxY - MinY) ? true : false;

	if (isSortByX)
	{
		uint32_t Width = m_WidthInPixel;
		std::sort(vio2DArray.begin(), vio2DArray.end(), [=](uint32_t a, uint32_t b) {
			return (a / Width) < (b / Width);
			});
	}
	else
	{
		uint32_t Width = m_WidthInPixel;
		std::sort(vio2DArray.begin(), vio2DArray.end(), [=](uint32_t a, uint32_t b) {
			return (a % Width) < (b % Width);
			});
	}
}

//*****************************************************************
//FUNCTION:
void CRoadMidlineExtracter::__sortLineArray(std::vector<uint32_t>& vio2DArray)
{
	std::uint32_t MinX = UINT32_MAX, MaxX = 0, MinY = UINT32_MAX, MaxY = 0;
	for (int i = 0; i < vio2DArray.size(); i++)
	{
		if (vio2DArray[i] / m_WidthInPixel < MinX) MinX = vio2DArray[i] / m_WidthInPixel;
		if (vio2DArray[i] / m_WidthInPixel > MaxX) MaxX = vio2DArray[i] / m_WidthInPixel;
		if (vio2DArray[i] % m_WidthInPixel < MinY) MinY = vio2DArray[i] % m_WidthInPixel;
		if (vio2DArray[i] % m_WidthInPixel > MaxY) MaxY = vio2DArray[i] % m_WidthInPixel;
	}
	_ASSERT(MinX <= MaxX && MinY <= MaxY);
	if (MinX != MaxX && MinY != MaxY)
	{
		// 建立二维数组表示线段的图，存取该点的序号，以便于排序
		std::vector<std::vector<std::uint32_t>> SearchMap(MaxX - MinX + 1, vector<std::uint32_t>(MaxY - MinY + 1));
		for (int i = 0; i < vio2DArray.size(); i++)
		{
			std::uint32_t X = vio2DArray[i] / m_WidthInPixel;
			std::uint32_t Y = vio2DArray[i] % m_WidthInPixel;
			SearchMap[X - MinX][Y - MinY] = i + 1;
		}

		bool isFindStartPoint = false;
		std::pair<std::uint32_t, std::uint32_t> StartPoint;
		StartPoint.first = (vio2DArray[0] / m_WidthInPixel) - MinX;
		StartPoint.second = (vio2DArray[0] % m_WidthInPixel) - MinY;

		std::vector<std::pair<std::uint32_t, std::uint32_t>> StartPointBeforeSelect;
		std::vector<std::pair<std::uint32_t, std::uint32_t>> AlternativeStartPointBeforeSelect;
		std::vector<uint32_t> TempArray;

		for (int i = 0; i < vio2DArray.size(); i++)
		{
			std::uint32_t X = vio2DArray[i] / m_WidthInPixel;
			std::uint32_t Y = vio2DArray[i] % m_WidthInPixel;
			int StartPointBeforeSelectX = X - MinX;
			int StartPointBeforeSelectY = Y - MinY;
			int StrongNeighborCount = 0;
			int WeakNeighborCount = 0;

			if (StartPointBeforeSelectX > 0 && StartPointBeforeSelectY > 0 && SearchMap[StartPointBeforeSelectX - 1][StartPointBeforeSelectY - 1]) WeakNeighborCount++;
			if (StartPointBeforeSelectX > 0 && SearchMap[StartPointBeforeSelectX - 1][StartPointBeforeSelectY]) StrongNeighborCount++;
			if (StartPointBeforeSelectX > 0 && StartPointBeforeSelectY < MaxY - MinY && SearchMap[StartPointBeforeSelectX - 1][StartPointBeforeSelectY + 1]) WeakNeighborCount++;
			if (StartPointBeforeSelectY > 0 && SearchMap[StartPointBeforeSelectX][StartPointBeforeSelectY - 1]) StrongNeighborCount++;
			if (StartPointBeforeSelectY < MaxY - MinY && SearchMap[StartPointBeforeSelectX][StartPointBeforeSelectY + 1]) StrongNeighborCount++;
			if (StartPointBeforeSelectX < MaxX - MinX && StartPointBeforeSelectY > 0 && SearchMap[StartPointBeforeSelectX + 1][StartPointBeforeSelectY - 1]) WeakNeighborCount++;
			if (StartPointBeforeSelectX < MaxX - MinX && SearchMap[StartPointBeforeSelectX + 1][StartPointBeforeSelectY]) StrongNeighborCount++;
			if (StartPointBeforeSelectX < MaxX - MinX && StartPointBeforeSelectY < MaxY - MinY && SearchMap[StartPointBeforeSelectX + 1][StartPointBeforeSelectY + 1]) WeakNeighborCount++;

			if (StrongNeighborCount + WeakNeighborCount == 0)
			{
				std::string Error = "There is no qualifying starting point!";
				return __sort2DArray(vio2DArray);
			}
			else if (StrongNeighborCount + WeakNeighborCount == 1)
			{
				isFindStartPoint = true;
				StartPoint.first = StartPointBeforeSelectX;
				StartPoint.second = StartPointBeforeSelectY;
				break;
			}
			else if (StrongNeighborCount == 1 && WeakNeighborCount == 1)
			{
				StartPointBeforeSelect.push_back(std::make_pair(StartPointBeforeSelectX, StartPointBeforeSelectY));
			}
			else if (StrongNeighborCount + WeakNeighborCount == 2 && (StrongNeighborCount == 2 || WeakNeighborCount == 2))
			{
				AlternativeStartPointBeforeSelect.push_back(std::make_pair(StartPointBeforeSelectX, StartPointBeforeSelectY));
			}
		}
		
		if (!isFindStartPoint)
		{
			// 暂无很好的解决方案，一个方向为优化骨骼细化算法，若暂不修改，则默认使用其中首个边界点
			if (StartPointBeforeSelect.size() != 0)
			{
				StartPoint = StartPointBeforeSelect[0];
			}
			else if(AlternativeStartPointBeforeSelect.size() != 0)
			{
				StartPoint = AlternativeStartPointBeforeSelect[0];
			}
		}

		TempArray.push_back(vio2DArray[SearchMap[StartPoint.first][StartPoint.second] - 1]);
		SearchMap[StartPoint.first][StartPoint.second] = 0;
		while (TempArray.size() != vio2DArray.size())
		{
			if (StartPoint.first >= 1 && SearchMap[StartPoint.first - 1][StartPoint.second])
			{
				StartPoint.first -= 1;
			}
			else if (StartPoint.first <= MaxX - MinX - 1 && SearchMap[StartPoint.first + 1][StartPoint.second])
			{
				StartPoint.first += 1;
			}
			else if (StartPoint.second >= 1 && SearchMap[StartPoint.first][StartPoint.second - 1])
			{
				StartPoint.second -= 1;
			}
			else if (StartPoint.second <= MaxY - MinY - 1 && SearchMap[StartPoint.first][StartPoint.second + 1])
			{
				StartPoint.second += 1;
			}
			else if (StartPoint.first >= 1 && StartPoint.second >= 1 && SearchMap[StartPoint.first - 1][StartPoint.second - 1])
			{
				StartPoint.first -= 1;
				StartPoint.second -= 1;
			}
			else if (StartPoint.first >= 1 && StartPoint.second <= MaxY - MinY - 1 && SearchMap[StartPoint.first - 1][StartPoint.second + 1])
			{
				StartPoint.first -= 1;
				StartPoint.second += 1;
			}
			else if (StartPoint.first <= MaxX - MinX - 1 && StartPoint.second >= 1 && SearchMap[StartPoint.first + 1][StartPoint.second - 1])
			{
				StartPoint.first += 1;
				StartPoint.second -= 1;
			}
			else if (StartPoint.first <= MaxX - MinX - 1 && StartPoint.second <= MaxY - MinY - 1 && SearchMap[StartPoint.first + 1][StartPoint.second + 1])
			{
				StartPoint.first += 1;
				StartPoint.second += 1;
			}
			else
			{
				std::string Error = "There are some points that have not been found!";
				break;
			}
			TempArray.push_back(vio2DArray[SearchMap[StartPoint.first][StartPoint.second] - 1]);
			SearchMap[StartPoint.first][StartPoint.second] = 0;
		}
		vio2DArray = TempArray;
	}
	else if (MinX == MaxX && MinY != MaxY)
	{
		uint32_t Width = m_WidthInPixel;
		std::sort(vio2DArray.begin(), vio2DArray.end(), [=](uint32_t a, uint32_t b) {
			return (a % Width) < (b % Width);
			});
	}
	else if (MinX != MaxX && MinY == MaxY)
	{
		uint32_t Width = m_WidthInPixel;
		std::sort(vio2DArray.begin(), vio2DArray.end(), [=](uint32_t a, uint32_t b) {
			return (a / Width) < (b / Width);
			});
	}
	else
	{
		// Something Error!
	}
}

//*****************************************************************
//FUNCTION:
void CRoadMidlineExtracter::__sortKeyPointsByPixelArray(std::vector<uint32_t>& vioKeyPointArray, const std::vector<uint32_t>& vPixelArray)
{
	// 创建一个索引数组，存储 vioSubLineArray 中元素在 vPixelArray 中的原始位置
	std::vector<std::size_t> indices(vioKeyPointArray.size());
	for (std::size_t i = 0; i < vioKeyPointArray.size(); i++) {
		for (std::size_t k = 0; k < vPixelArray.size(); k++) {
			if (vioKeyPointArray[i] == vPixelArray[k])
			{
				indices[i] = k;
				break;
			}
		}
	}

	// 使用 std::sort 根据 vLineArray 中元素的值对索引数组进行排序
	std::sort(indices.begin(), indices.end());
	for (std::size_t i = 0; i < vioKeyPointArray.size(); i++) {
		vioKeyPointArray[i] = vPixelArray[indices[i]];
	}
}

//*****************************************************************
//FUNCTION:
bool CRoadMidlineExtracter::__simplifyPixelArray(SRoad& vioRoad, float vFittingScale)
{
	_ASSERTE(!vioRoad.PixelArray.empty());

	std::vector<uint32_t> KeyPointArray;
	KeyPointArray.push_back(vioRoad.PixelArray[0]);
	KeyPointArray.push_back(vioRoad.PixelArray[vioRoad.PixelArray.size() - 1]);

	int Start = 0, End = vioRoad.PixelArray.size() - 1;
	__addPolylinePoint(Start, End, vFittingScale, KeyPointArray, vioRoad.PixelArray);
	//__sort2DArray(SimplifiedArray);
	__sortKeyPointsByPixelArray(KeyPointArray, vioRoad.PixelArray);

	vioRoad.KeyPointArray = KeyPointArray;

	return true;
}

//*****************************************************************
//FUNCTION:
bool hiveObliquePhotography::CRoadMidlineExtracter::__simplifyKeyPointArray(SRoad& vioRoad, float vFittingScale)
{
	_ASSERTE(!vioRoad.KeyPointArray.empty());

	std::vector<uint32_t> SimplifiedArray;
	SimplifiedArray.push_back(vioRoad.KeyPointArray[0]);
	SimplifiedArray.push_back(vioRoad.KeyPointArray[vioRoad.KeyPointArray.size() - 1]);

	int Start = 0, End = vioRoad.KeyPointArray.size() - 1;
	__addPolylinePoint(Start, End, vFittingScale, SimplifiedArray, vioRoad.KeyPointArray);
	//__sort2DArray(SimplifiedArray);
	__sortKeyPointsByPixelArray(SimplifiedArray, vioRoad.PixelArray);

	vioRoad.KeyPointArray = SimplifiedArray;

	return false;
}

//*****************************************************************
//FUNCTION:
float CRoadMidlineExtracter::__calcPoint2LineDistance(const Eigen::Vector2f vPixelCoord, const Eigen::Vector2f vLineEndPointA, const Eigen::Vector2f vLineEndPointB, bool vIsSegmentLine)
{
	Eigen::Vector2f PA = vPixelCoord - vLineEndPointA, BA = vLineEndPointB - vLineEndPointA;
	float Temp;
	if (vIsSegmentLine)
		Temp = std::clamp(PA.dot(BA) / BA.dot(BA), (float)0.0, (float)1.0);
	else
		Temp = PA.dot(BA) / BA.dot(BA);
	return (PA - BA * Temp).dot(PA - BA * Temp);
}

//*****************************************************************
//FUNCTION: 
void CRoadMidlineExtracter::__calcEightNeighbourhood(const int vX, const int vY, std::vector<bool>& voEightNeighbourhood, const std::uint8_t* vPixel, const cv::Mat& vRoadSkeletonMap)
{
	//7 0 1
	//6   2
	//5 4 3
	//计算8邻域kernel，边界处凑不够8邻域的话用对边填补
	int CenterPixelPos = vX * vRoadSkeletonMap.cols + vY;
	voEightNeighbourhood[0] = vPixel[CenterPixelPos + ((vX == 0) ? 0 : -vRoadSkeletonMap.cols)];
	voEightNeighbourhood[1] = vPixel[CenterPixelPos + ((vX == 0 || vY == vRoadSkeletonMap.cols - 1) ? 0 : -vRoadSkeletonMap.cols + 1)];
	voEightNeighbourhood[2] = vPixel[CenterPixelPos + ((vY == vRoadSkeletonMap.cols - 1) ? 0 : 1)];
	voEightNeighbourhood[3] = vPixel[CenterPixelPos + ((vX == vRoadSkeletonMap.rows - 1 || vY == vRoadSkeletonMap.cols - 1) ? 0 : vRoadSkeletonMap.cols + 1)];
	voEightNeighbourhood[4] = vPixel[CenterPixelPos + ((vX == vRoadSkeletonMap.rows - 1) ? 0 : vRoadSkeletonMap.cols)];
	voEightNeighbourhood[5] = vPixel[CenterPixelPos + ((vX == vRoadSkeletonMap.rows - 1 || vY == 0) ? 0 : vRoadSkeletonMap.cols - 1)];
	voEightNeighbourhood[6] = vPixel[CenterPixelPos + ((vY == 0) ? 0 : -1)];
	voEightNeighbourhood[7] = vPixel[CenterPixelPos + ((vX == 0 || vY == 0) ? 0 : -vRoadSkeletonMap.cols - 1)];
}

//*****************************************************************
//FUNCTION: 
void CRoadMidlineExtracter::__findPixel4Delete(const int vX, const int vY, const int vflag, const std::vector<bool>& vEightNeighbourhood,
	const cv::Mat& vRoadSkeletonMap, std::uint8_t* vPixel, std::vector<std::uint8_t*>& voDeleteSet)
{
	if (std::accumulate(vEightNeighbourhood.begin(), vEightNeighbourhood.end(), 0) >= 2 &&
		std::accumulate(vEightNeighbourhood.begin(), vEightNeighbourhood.end(), 0) <= 6) //8邻域中有2到6个非道路像素
	{
		int count = 0;
		for (int j = 0; j < vEightNeighbourhood.size(); j++)
		{
			if (vEightNeighbourhood[j] == 0 && vEightNeighbourhood[(j + 1) % vEightNeighbourhood.size()] == 1)
				++count;//count==1说明有且仅有一个临界点，进一步说明vX vY在道路边界上
		}

		if (vflag == 1)
		{
			if (count == 1 && vEightNeighbourhood[0] * vEightNeighbourhood[2] * vEightNeighbourhood[4] == 0 &&
				vEightNeighbourhood[2] * vEightNeighbourhood[4] * vEightNeighbourhood[6] == 0)//右，下
				voDeleteSet.emplace_back(&vPixel[vX * vRoadSkeletonMap.step + vY]);
			return;
		}
		if (count == 1 && vEightNeighbourhood[0] * vEightNeighbourhood[2] * vEightNeighbourhood[6] == 0 &&
			vEightNeighbourhood[0] * vEightNeighbourhood[4] * vEightNeighbourhood[6] == 0)//左，上
			voDeleteSet.emplace_back(&vPixel[vX * vRoadSkeletonMap.step + vY]);
	}
};

//*****************************************************************
//FUNCTION: 
std::vector<bool> CRoadMidlineExtracter::__generateSkeletonMap(const std::vector<bool>& vBinaryMap)
{
	if (vBinaryMap.empty())
		return std::vector<bool>();

	std::vector<std::uint8_t> OriginMap;
	for (auto e : vBinaryMap)
	{
		if (e == 0) OriginMap.emplace_back(0);
		else OriginMap.emplace_back(255);
	}
	cv::Mat RoadSkeletonMap(OriginMap);
	RoadSkeletonMap = RoadSkeletonMap.reshape(1, m_HeightInPixel).clone();

	bool IfEnd = false;
	std::vector<bool> EightNeighbourhood(8);
	std::vector<std::uint8_t*> DeleteSet;
	std::uint8_t* pPixelBuffer = RoadSkeletonMap.data;

	auto deletePixel = [&]()
	{
		for (auto e : DeleteSet) {
			*e = 0;
			IfEnd = false;
		}
		DeleteSet.clear();
	};

	do {
		IfEnd = true;
		for (int i = 0; i < RoadSkeletonMap.rows; ++i)
		{
			for (int k = 0; k < RoadSkeletonMap.cols; ++k)
			{
				if (pPixelBuffer[i * RoadSkeletonMap.step + k] == 0) continue;
				__calcEightNeighbourhood(i, k, EightNeighbourhood, pPixelBuffer, RoadSkeletonMap);
				__findPixel4Delete(i, k, 1, EightNeighbourhood, RoadSkeletonMap, pPixelBuffer, DeleteSet);
			}
		}
		deletePixel();

		for (int i = 0; i < RoadSkeletonMap.rows; ++i)
		{
			for (int k = 0; k < RoadSkeletonMap.cols; ++k)
			{
				if (pPixelBuffer[i * RoadSkeletonMap.step + k] == 0) continue;
				__calcEightNeighbourhood(i, k, EightNeighbourhood, pPixelBuffer, RoadSkeletonMap);
				__findPixel4Delete(i, k, 2, EightNeighbourhood, RoadSkeletonMap, pPixelBuffer, DeleteSet);
			}
		}
		deletePixel();

	} while (!IfEnd);

	std::vector<bool> RoadSkeletonBinaryMap;
	for (auto e : (std::vector<std::uint8_t>)(RoadSkeletonMap.reshape(1, 1)))
	{
		if (e == 0) RoadSkeletonBinaryMap.emplace_back(0);
		else RoadSkeletonBinaryMap.emplace_back(1);
	}
	hiveEventLogger::hiveOutputEvent("Generate SkeletonMap is done.");
	return RoadSkeletonBinaryMap;
}

//*****************************************************************
//FUNCTION:
bool getValidValue(int vRow, int vCol, std::uint8_t* vPixelBuffer, const cv::Mat &vRoadSkeletonMap) {
	if (vRow >= 0 && vRow < vRoadSkeletonMap.rows && vCol >= 0 && vCol < vRoadSkeletonMap.cols)
		return vPixelBuffer[vRow * vRoadSkeletonMap.cols + vCol] > 0;
	else
		return 0;
}

//*****************************************************************
//FUNCTION:
void hiveObliquePhotography::CRoadMidlineExtracter::__findIntersections(const std::vector<bool>& vRoadSkeletonMap, int vShiftSize) {
	std::vector<std::uint8_t> OriginMap;
	for (auto e : vRoadSkeletonMap)
	{
		if (e == 0) OriginMap.emplace_back(0);
		else OriginMap.emplace_back(255);
	}
	cv::Mat RoadSkeletonMap(OriginMap);
	RoadSkeletonMap = RoadSkeletonMap.reshape(1, m_WidthInPixel).clone();//调整通道数

	if (vShiftSize <= 0 || vShiftSize >= RoadSkeletonMap.rows || vShiftSize >= RoadSkeletonMap.cols)
		return;

	//找到邻域边界上1的个数大于2的点，合并这些点作为路口
	m_IntersectionSet.clear();
	std::uint8_t* pPixelBuffer = RoadSkeletonMap.data;
	std::vector<bool> Neightbourds((vShiftSize * 2 + 1) * (vShiftSize * 2 + 1));
	for (int i = 0; i < RoadSkeletonMap.rows; ++i)
	{
		for (int k = 0; k < RoadSkeletonMap.cols; ++k)
		{
			if (pPixelBuffer[i * RoadSkeletonMap.cols + k] == 0) continue;
			Neightbourds.clear();
			for (int t = -vShiftSize; t < vShiftSize; t++) {
				Neightbourds.push_back(getValidValue(i - vShiftSize, k + t, pPixelBuffer, RoadSkeletonMap));//上边界
			}
			for (int s = -vShiftSize; s < vShiftSize; s++) {
				Neightbourds.push_back(getValidValue(i + s, k + vShiftSize, pPixelBuffer, RoadSkeletonMap));//右边界
			}
			for (int t = vShiftSize; t > -vShiftSize; t--) {
				Neightbourds.push_back(getValidValue(i + vShiftSize, k + t, pPixelBuffer, RoadSkeletonMap));//下边界
			}
			for (int s = vShiftSize; s > -vShiftSize; s--) {
				Neightbourds.push_back(getValidValue(i + s, k - vShiftSize, pPixelBuffer, RoadSkeletonMap));//左边界
			}
			
			bool isContinuousOne = false;
			int ConnectedPartCnt = 0;
			for (bool n : Neightbourds) {
				if (n) {
					if(!isContinuousOne) ConnectedPartCnt++;
					isContinuousOne = true;
				}
				else {
					isContinuousOne = false;
				}
			}
			if (Neightbourds[Neightbourds.size() - 1] && Neightbourds[0]) ConnectedPartCnt--;

			if (ConnectedPartCnt > 2) {
				m_IntersectionSet.push_back({ i,k });
			}
			//补丁：多个ConnectedPart连在一起了
			if(accumulate(Neightbourds.begin(), Neightbourds.end(),0) >= 6)
				m_IntersectionSet.push_back({ i,k });
		}
	}
}

double calcDistance(int x1, int y1, int x2, int y2) {
	return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

//*****************************************************************
//FUNCTION:
std::vector<SRoad> hiveObliquePhotography::CRoadMidlineExtracter::__divideRoadSkeletonByIntersection(std::vector<bool>& vRoadSkeletonMap, int vShiftSize)
{
	//删除以intersection为中心的删除窗口内所有像素
	m_DeleteWindowShiftSize = vShiftSize;
	for (auto pos : m_IntersectionSet) {
		for (int i = -m_DeleteWindowShiftSize; i < m_DeleteWindowShiftSize; i++) {
			for (int k = -m_DeleteWindowShiftSize; k < m_DeleteWindowShiftSize; k++) {
				int x = get<0>(pos) + i, y = get<1>(pos) + k;
				if (x >= 0 && x < m_WidthInPixel && y >= 0 && y < m_HeightInPixel)
					vRoadSkeletonMap[x * m_WidthInPixel + y] = 0;
			}
		}
	}

	std::vector<std::vector<uint32_t>> SubRoadPixelSet = __dividePixelsByRegionGrow(vRoadSkeletonMap);

	std::vector<SRoad> Roads;
	for (auto Pixles : SubRoadPixelSet) {
		SRoad Road;
		std::set<uint32_t> PointSet(Pixles.begin(), Pixles.end());//去重
		Road.PixelArray.assign(Pixles.begin(), Pixles.end());
		__sortLineArray(Road.PixelArray);
		Road.Width = 0.0;
		Roads.push_back(Road);
	}

	return Roads;
}

//*****************************************************************
//FUNCTION:
bool hiveObliquePhotography::CRoadMidlineExtracter::__mergeIntersections(std::vector<std::tuple<uint32_t, uint32_t>>& vioIntersectionSet, double vMergeRange) {
	int MergedSum = 0;
	while (MergedSum != vioIntersectionSet.size())
	{
		bool ReStart = false;
		for (std::size_t i = MergedSum; i < vioIntersectionSet.size() && !ReStart; ++i)
		{
			for (std::size_t k = i + 1; k < vioIntersectionSet.size() && !ReStart; ++k)
			{
				// Extract X and Y coordinates from tuples
				int x1 = get<0>(m_IntersectionSet[i]);
				int y1 = get<1>(m_IntersectionSet[i]);
				int x2 = get<0>(m_IntersectionSet[k]);
				int y2 = get<1>(m_IntersectionSet[k]);
				// Calculate Euclidean distance
				double distance = std::sqrt(static_cast<float>(std::pow(x2 - x1, 2) + std::pow(y2 - y1, 2)));
				// If distance is less than 1, merge points
				if (distance < vMergeRange) {
					// Calculate average of X and Y coordinates
					int averageX = static_cast<float>(x1 + x2) / 2.0;
					int averageY = static_cast<float>(y1 + y2) / 2.0;

					vioIntersectionSet[i] = std::make_tuple(averageX, averageY);
					vioIntersectionSet.erase(vioIntersectionSet.begin() + k);
					ReStart = true;
				}
			}
			if (!ReStart) MergedSum++;
		}
	}
	return true;
}

//*****************************************************************
//FUNCTION:
bool hiveObliquePhotography::CRoadMidlineExtracter::__findIntersection2Roads(const std::vector<bool>& vRoadSkeletonMap, std::vector<SRoad>& vioRoads, int vMaxDeadendLength)
{
	__mergeIntersections(m_IntersectionSet, vMaxDeadendLength);

	double MaxDistance2Intersection = (m_DeleteWindowShiftSize * 2 + 1) * 2;
	for (auto iter = vioRoads.begin(); iter != vioRoads.end(); iter++) {
		std::vector<uint32_t>& sub_road = iter->KeyPointArray;
		//__sort2DArray(sub_road);
		int sx = sub_road[0] / m_WidthInPixel, sy = sub_road[0] % m_WidthInPixel;
		int tx = sub_road[sub_road.size() - 1] / m_WidthInPixel, ty = sub_road[sub_road.size() - 1] % m_WidthInPixel;
		int s_intersection_index = -1, t_intersection_index = -1;
		double s_min_dis = MaxDistance2Intersection, t_min_dis = MaxDistance2Intersection;
		for (int i = 0; i < m_IntersectionSet.size(); i++) {
			//if (s_intersection_index != -1 && t_intersection_index != -1) break;
			double s_dis = calcDistance(sx, sy, get<0>(m_IntersectionSet[i]), get<1>(m_IntersectionSet[i]));
			double t_dis = calcDistance(tx, ty, get<0>(m_IntersectionSet[i]), get<1>(m_IntersectionSet[i]));
			if (s_dis < s_min_dis) {
				s_intersection_index = i;
				s_min_dis = s_dis;
			}
			if (t_dis < t_min_dis) {
				t_intersection_index = i;
				t_min_dis = t_dis;
			}
		}
		if (s_intersection_index != -1){
			iter->IntersectionIndexA = s_intersection_index;
		}
		if (t_intersection_index != -1){
			iter->IntersectionIndexB = t_intersection_index;
		}
	}
	return true;
}

//*****************************************************************
//FUNCTION:
bool hiveObliquePhotography::CRoadMidlineExtracter::__findIntersection2Roads_arcmap(std::vector<SRoad>& vioRoads) {
	std::set<std::tuple<uint32_t, uint32_t>> UniqueIntersections;
	//收集路口
	for (auto road : vioRoads) {
		int Size = road.KeyPointArray.size();
		UniqueIntersections.insert({ road.KeyPointArray[0] / m_WidthInPixel, road.KeyPointArray[0] % m_WidthInPixel });
		UniqueIntersections.insert({ road.KeyPointArray[Size - 1] / m_WidthInPixel, road.KeyPointArray[Size - 1] % m_WidthInPixel });
	}
	m_IntersectionSet.assign(UniqueIntersections.begin(), UniqueIntersections.end());

	//分配IntersectionIndexA和IntersectionIndexB
	for (auto& road : vioRoads) {
		std::tuple<uint32_t, uint32_t> EndPointA{ road.KeyPointArray[0] / m_WidthInPixel, road.KeyPointArray[0] % m_WidthInPixel };
		auto iter = find(m_IntersectionSet.begin(), m_IntersectionSet.end(), EndPointA);
		if (iter != m_IntersectionSet.end())
			road.IntersectionIndexA = iter - m_IntersectionSet.begin();

		int Size = road.KeyPointArray.size();
		std::tuple<uint32_t, uint32_t> EndPointB{ road.KeyPointArray[Size - 1] / m_WidthInPixel, road.KeyPointArray[Size - 1] % m_WidthInPixel };
		iter = find(m_IntersectionSet.begin(), m_IntersectionSet.end(), EndPointB);
		if (iter != m_IntersectionSet.end())
			road.IntersectionIndexB = iter - m_IntersectionSet.begin();
	}

	return true;
}

//*****************************************************************
//FUNCTION:
bool hiveObliquePhotography::CRoadMidlineExtracter::__addIntersection2Roads(std::vector<SRoad>& vioRoads) 
{
	for (auto iter = vioRoads.begin(); iter != vioRoads.end(); iter++) {
		std::vector<uint32_t>& sub_road = iter->KeyPointArray;
		if (iter->IntersectionIndexA != -1)
		{
			/*uint32_t sx = sub_road[0] / m_WidthInPixel, sy = sub_road[0] % m_WidthInPixel;
			uint32_t ix = get<0>(m_IntersectionSet[iter->EndPointS]), iy = get<1>(m_IntersectionSet[iter->EndPointS]);
			double distance = std::sqrt(std::pow(sx - ix, 2) + std::pow(sy - iy, 2));
			if (distance > 20.0) {
				continue;
			}*/
			sub_road.insert(sub_road.begin(), get<0>(m_IntersectionSet[iter->IntersectionIndexA]) * m_WidthInPixel + get<1>(m_IntersectionSet[iter->IntersectionIndexA]));
		}
		if (iter->IntersectionIndexB != -1)
		{
			/*uint32_t tx = sub_road[sub_road.size() - 1] / m_WidthInPixel, ty = sub_road[sub_road.size() - 1] % m_WidthInPixel;
			uint32_t ix = get<0>(m_IntersectionSet[iter->EndPointT]), iy = get<1>(m_IntersectionSet[iter->EndPointT]);
			double distance = std::sqrt(std::pow(tx - ix, 2) + std::pow(ty - iy, 2));
			if (distance > 20.0) {
				continue;
			}*/
			sub_road.push_back(get<0>(m_IntersectionSet[iter->IntersectionIndexB]) * m_WidthInPixel + get<1>(m_IntersectionSet[iter->IntersectionIndexB]));
		}
	}
	return true;
}

//*****************************************************************
//FUNCTION:
bool hiveObliquePhotography::CRoadMidlineExtracter::__deleteDeadendRoads(std::vector<SRoad>& vioRoads, double vMaxDeadendLength)
{
	if (vMaxDeadendLength <= 0) {
		return true;
	}

	for (auto iter = vioRoads.begin(); iter != vioRoads.end(); iter++) {
		std::vector<uint32_t>& sub_road = iter->KeyPointArray;
		uint32_t sx = sub_road[0] / m_WidthInPixel, sy = sub_road[0] % m_WidthInPixel;
		uint32_t tx = sub_road[sub_road.size() - 1] / m_WidthInPixel, ty = sub_road[sub_road.size() - 1] % m_WidthInPixel;
		int s_intersection_index = iter->IntersectionIndexA, t_intersection_index = iter->IntersectionIndexB;
		if (s_intersection_index == -1 || t_intersection_index == -1 || s_intersection_index == t_intersection_index) {
			//断在边界上
			//if (!(sx <= m_DeleteWindowShiftSize || sx == m_WidthInPixel - 1 || sy <= m_DeleteWindowShiftSize || sy == m_HeightInPixel - 1 || tx <= m_DeleteWindowShiftSize || tx == m_WidthInPixel - 1 || ty <= m_DeleteWindowShiftSize || ty == m_HeightInPixel - 1))

			// Calculate Euclidean distance
			double distance = std::sqrt(std::pow(tx - sx, 2) + std::pow(ty - sy, 2));
			// If distance is less than 1, merge points
			if (distance < vMaxDeadendLength) {
				// Calculate average of X and Y coordinates
				iter = vioRoads.erase(iter);
				iter--;
			}
		}
	}

	return true;
}

//*****************************************************************
//FUNCTION: 
bool hiveObliquePhotography::CRoadMidlineExtracter::__extendBrokenRoads(const std::vector<bool>& vMidlineMap, std::vector<SRoad>& vioRoads, int vMaxStep)
{
	for (SRoad& Road : vioRoads) {
		__extendSubRoad(vMidlineMap, Road, true, vMaxStep);
		__extendSubRoad(vMidlineMap, Road, false, vMaxStep);
	}
	return true;
}

//*****************************************************************
//FUNCTION: 
bool hiveObliquePhotography::CRoadMidlineExtracter::__extendSubRoad(const std::vector<bool>& vMidlineMap, SRoad& vioRoad, bool isForward, int vMaxStep)
{
	int Width = m_WidthInPixel;
	int Height = m_HeightInPixel;

	int StartIdx = isForward ? 1 : vioRoad.KeyPointArray.size() - 2;
	int	EndIdx = isForward ? 0 : vioRoad.KeyPointArray.size() - 1;

	int x1 = vioRoad.KeyPointArray[StartIdx] / Width;
	int y1 = vioRoad.KeyPointArray[StartIdx] % Width;
	int x2 = vioRoad.KeyPointArray[EndIdx] / Width;
	int y2 = vioRoad.KeyPointArray[EndIdx] % Width;
	double slope = (y1 == y2) ? 9.0f : double(x2 - x1) / (y2 - y1);

	if (fabs(slope) > 1)
	{
		double tempx = x2, tempy = y2;
		for (int j = 0; j < vMaxStep; j++)
		{
			tempx += (double(x2 - x1) > 0) ? 1 : -1;
			tempy += (double(x2 - x1) > 0) ? (1 / slope) : (-1 / slope);
			if (tempx >= 0 && tempy >= 0 && tempx < Height && tempy < Width && vMidlineMap[static_cast<int>(tempx) * Width + static_cast<int>(tempy)] != 0)
			{
				if (isForward) {
					vioRoad.KeyPointArray.insert(vioRoad.KeyPointArray.begin(), (static_cast<int>(tempx) * Width + static_cast<int>(tempy)));
				}
				else {
					vioRoad.KeyPointArray.push_back((static_cast<int>(tempx) * Width + static_cast<int>(tempy)));
				}
				break;
			}
		}
	}
	else
	{
		double tempx = x2, tempy = y2;
		for (int j = 0; j < vMaxStep; j++)
		{
			tempx += (double(x2 - x1) > 0) ? slope : -slope;
			tempy += (double(x2 - x1) > 0) ? 1 : -1;
			if (tempx >= 0 && tempy >= 0 && tempx < Height && tempy < Width && vMidlineMap[static_cast<int>(tempx) * Width + static_cast<int>(tempy)] != 0)
			{
				if (isForward) {
					vioRoad.KeyPointArray.insert(vioRoad.KeyPointArray.begin(), (static_cast<int>(tempx) * Width + static_cast<int>(tempy)));
				}
				else {
					vioRoad.KeyPointArray.push_back((static_cast<int>(tempx) * Width + static_cast<int>(tempy)));
				}
				break;
			}
		}
	}
	return true;
}

//*****************************************************************
//FUNCTION: 
bool hiveObliquePhotography::CRoadMidlineExtracter::__mergeSubRoads(std::vector<SRoad>& vioRoads)
{
	int MergedSum = 0;
	while (MergedSum != vioRoads.size())
	{
		bool ReStart = false;
		for (std::size_t i = MergedSum; i < vioRoads.size() && !ReStart; ++i)
		{
			for (std::size_t j = i + 1; j < vioRoads.size() && !ReStart; ++j)
			{
				// 检查两个子路是否共用一个路口
				if ((vioRoads[i].IntersectionIndexA == vioRoads[j].IntersectionIndexA && vioRoads[i].IntersectionIndexA != -1)
					|| (vioRoads[i].IntersectionIndexA == vioRoads[j].IntersectionIndexB && vioRoads[i].IntersectionIndexA != -1)
					|| (vioRoads[i].IntersectionIndexB == vioRoads[j].IntersectionIndexA && vioRoads[i].IntersectionIndexB != -1)
					|| (vioRoads[i].IntersectionIndexB == vioRoads[j].IntersectionIndexB && vioRoads[i].IntersectionIndexB != -1))
				{
					/*if (vioRoads[i].EndPointS != -1) {
						int x = get<0>(m_IntersectionSet[vioRoads[i].EndPointS]);
						int y = get<1>(m_IntersectionSet[vioRoads[i].EndPointS]);
						if (x > 490 && x < 509 && y > 238 && y < 251)
							cout << x << " " << y << endl;
					}
					if (vioRoads[i].EndPointT != -1) {
						int x = get<0>(m_IntersectionSet[vioRoads[i].EndPointT]);
						int y = get<1>(m_IntersectionSet[vioRoads[i].EndPointT]);
						if (x > 490 && x < 509 && y > 238 && y < 251)
							cout << x << " " << y << endl;
					}*/

					ReStart = __mergeTwoRoads(vioRoads, i, j);
				}
			}
			if (!ReStart) MergedSum++;
		}
	}
	return true;
}

//*****************************************************************
//FUNCTION: 
bool hiveObliquePhotography::CRoadMidlineExtracter::__mergeTwoRoads(std::vector<SRoad>& vioRoads, int i, int k)
{
	bool isNeedMerge = false;
	//if(fabs(vioRoads[i].Width - vioRoads[k].Width) > max(vioRoads[i].Width, vioRoads[k].Width) * 0.2) return false;
	vioRoads[i].Width = (vioRoads[i].Width + vioRoads[k].Width) / 2.0f;
	if (vioRoads[i].IntersectionIndexA == vioRoads[k].IntersectionIndexA && vioRoads[i].IntersectionIndexA != -1)
	{
		isNeedMerge = __judgeCollinear(vioRoads, i, k, true, true);
		std::reverse(vioRoads[i].KeyPointArray.begin(), vioRoads[i].KeyPointArray.end());
		if (isNeedMerge)
		{
			vioRoads[i].KeyPointArray.insert(vioRoads[i].KeyPointArray.end(), vioRoads[k].KeyPointArray.begin(), vioRoads[k].KeyPointArray.end());
			vioRoads[i].IntersectionIndexA = vioRoads[i].IntersectionIndexB;
			vioRoads[i].IntersectionIndexB = vioRoads[k].IntersectionIndexB;
			vioRoads.erase(vioRoads.begin() + k);
			return true;
		}
	}
	if (vioRoads[i].IntersectionIndexA == vioRoads[k].IntersectionIndexB && vioRoads[i].IntersectionIndexA != -1)
	{
		isNeedMerge = __judgeCollinear(vioRoads, i, k, true, false);
		if (isNeedMerge)
		{
			std::reverse(vioRoads[i].KeyPointArray.begin(), vioRoads[i].KeyPointArray.end());
			vioRoads[i].KeyPointArray.insert(vioRoads[i].KeyPointArray.end(), vioRoads[k].KeyPointArray.rbegin(), vioRoads[k].KeyPointArray.rend());
			vioRoads[i].IntersectionIndexA = vioRoads[i].IntersectionIndexB;
			vioRoads[i].IntersectionIndexB = vioRoads[k].IntersectionIndexA;
			vioRoads.erase(vioRoads.begin() + k);
			return true;
		}
	}
	if (vioRoads[i].IntersectionIndexB == vioRoads[k].IntersectionIndexA && vioRoads[i].IntersectionIndexB != -1)
	{
		isNeedMerge = __judgeCollinear(vioRoads, i, k, false, true);
		if (isNeedMerge)
		{
			vioRoads[i].KeyPointArray.insert(vioRoads[i].KeyPointArray.end(), vioRoads[k].KeyPointArray.begin(), vioRoads[k].KeyPointArray.end());
			vioRoads[i].IntersectionIndexB = vioRoads[k].IntersectionIndexB;
			vioRoads.erase(vioRoads.begin() + k);
			return true;
		}
	}
	if (vioRoads[i].IntersectionIndexB == vioRoads[k].IntersectionIndexB && vioRoads[i].IntersectionIndexB != -1)
	{
		isNeedMerge = __judgeCollinear(vioRoads, i, k, false, false);
		if (isNeedMerge)
		{
			vioRoads[i].KeyPointArray.insert(vioRoads[i].KeyPointArray.end(), vioRoads[k].KeyPointArray.rbegin(), vioRoads[k].KeyPointArray.rend());
			vioRoads[i].IntersectionIndexB = vioRoads[k].IntersectionIndexA;
			vioRoads.erase(vioRoads.begin() + k);
			return true;
		}
	}
	return false;
}

//*****************************************************************
//FUNCTION: 
bool hiveObliquePhotography::CRoadMidlineExtracter::__judgeCollinear(std::vector<SRoad>& vioRoads, int i, int k, bool isFirstStart, bool isSecondStart)
{
	std::pair<int, int> VectorA, VectorB;
	// 计算线段A的角度
	int x1A = (isFirstStart ? vioRoads[i].KeyPointArray[0] : vioRoads[i].KeyPointArray[vioRoads[i].KeyPointArray.size() - 1]) / m_WidthInPixel;
	int y1A = (isFirstStart ? vioRoads[i].KeyPointArray[0] : vioRoads[i].KeyPointArray[vioRoads[i].KeyPointArray.size() - 1]) % m_WidthInPixel;
	/*if (vioRoads[i].KeyPoints.size() >= 3)
	{
		int x3A = (isFirstStart ? vioRoads[i].KeyPoints[2] : vioRoads[i].KeyPoints[vioRoads[i].KeyPoints.size() - 3]) / m_WidthInPixel;
		int y3A = (isFirstStart ? vioRoads[i].KeyPoints[2] : vioRoads[i].KeyPoints[vioRoads[i].KeyPoints.size() - 3]) % m_WidthInPixel;
		VectorA = { x1A - x3A, y1A - y3A };
	}*/
	if(vioRoads[i].KeyPointArray.size() >= 2)
	{
		int x2A = (isFirstStart ? vioRoads[i].KeyPointArray[1] : vioRoads[i].KeyPointArray[vioRoads[i].KeyPointArray.size() - 2]) / m_WidthInPixel;
		int y2A = (isFirstStart ? vioRoads[i].KeyPointArray[1] : vioRoads[i].KeyPointArray[vioRoads[i].KeyPointArray.size() - 2]) % m_WidthInPixel;
		VectorA = { x1A - x2A, y1A - y2A };
	}
	else
	{
		return false;
	}
	// 计算线段B的角度
	int x1B = (isSecondStart ? vioRoads[k].KeyPointArray[0] : vioRoads[k].KeyPointArray[vioRoads[k].KeyPointArray.size() - 1]) / m_WidthInPixel;
	int y1B = (isSecondStart ? vioRoads[k].KeyPointArray[0] : vioRoads[k].KeyPointArray[vioRoads[k].KeyPointArray.size() - 1]) % m_WidthInPixel;
	/*if (vioRoads[k].KeyPoints.size() >= 3)
	{
		int x3B = (isSecondStart ? vioRoads[k].KeyPoints[2] : vioRoads[k].KeyPoints[vioRoads[k].KeyPoints.size() - 3]) / m_WidthInPixel;
		int y3B = (isSecondStart ? vioRoads[k].KeyPoints[2] : vioRoads[k].KeyPoints[vioRoads[k].KeyPoints.size() - 3]) % m_WidthInPixel;
		VectorB = { x1B - x3B, y1B - y3B };
		if (x3B - x1B == 0)
			int x = 0;
	}*/
	if (vioRoads[k].KeyPointArray.size() >= 2)
	{
		int x2B = (isSecondStart ? vioRoads[k].KeyPointArray[1] : vioRoads[k].KeyPointArray[vioRoads[k].KeyPointArray.size() - 2]) / m_WidthInPixel;
		int y2B = (isSecondStart ? vioRoads[k].KeyPointArray[1] : vioRoads[k].KeyPointArray[vioRoads[k].KeyPointArray.size() - 2]) % m_WidthInPixel;
		VectorB = { x1B - x2B, y1B - y2B };
	}
	else
	{
		return false;
	}

	double distance = std::sqrt(static_cast<float>(std::pow(x1B - x1A, 2) + std::pow(y1B - y1A, 2)));
	if (distance > 15.0) {
		return false;
	}

	// Calculate dot product
	double dotProduct = VectorA.first * VectorB.first + VectorA.second * VectorB.second;
	// Calculate magnitudes
	double magnitudeA = std::sqrt(static_cast<double>(VectorA.first * VectorA.first + VectorA.second * VectorA.second));
	double magnitudeB = std::sqrt(static_cast<double>(VectorB.first * VectorB.first + VectorB.second * VectorB.second));
	// Calculate angle in radians
	double angle = std::acos(dotProduct / (magnitudeA * magnitudeB));
	// Convert angle to degrees
	double angleDegree = angle * (180.0 / M_PI);
	// Check if angle is in the desired range 
	bool Result = angleDegree < 1e-6 || angleDegree >= 135.0;
	return Result;
}

//*****************************************************************
//FUNCTION: 
bool hiveObliquePhotography::CRoadMidlineExtracter::__estimateRoadWidth(const std::vector<bool>& vBinaryMap, std::vector<SRoad>& vioRoads, double vMagicNumber)
{
	//double Ratio = ActualRoadWidth / m_WidthInPixel;
	double Ratio = 1.0;

	int Width = m_WidthInPixel;
	int Height = m_HeightInPixel;
	for (int i = 0; i < vioRoads.size(); i++)
	{
		double Sum = 0;
		auto Points = vioRoads[i].KeyPointArray;
		for (int k = 0; k < Points.size(); k++)
		{
			if (k + 1 < Points.size())
			{
				int x1 = Points[k] / Width;
				int y1 = Points[k] % Width;
				int x2 = Points[k + 1] / Width;
				int y2 = Points[k + 1] % Width;

				double slope = (y1 == y2) ? std::numeric_limits<double>::infinity() : double(x2 - x1) / (y2 - y1);
				//double slope = (x1 == x2) ? std::numeric_limits<double>::infinity() : double(y2 - y1) / (x2 - x1);
				double perpendicularSlope = -1.0 / slope;
				std::vector<double> lineWidthSet;
				for (double t = 0.1; t <= 0.9; t += 0.1) {
					int x = static_cast<int>(x1 + t * (x2 - x1));
					int y = static_cast<int>(y1 + t * (y2 - y1));
					double lineWidth = 0;
					if (fabs(perpendicularSlope) > 1)
					{
						double tempx = x, tempy = y;
						while (tempx >= 0 && tempy >= 0 && tempx < Height && tempy < Width && vBinaryMap[static_cast<int>(tempx) * Width + static_cast<int>(tempy)] != 0)
						{
							lineWidth++;
							tempx += 1;
							tempy += 1 / perpendicularSlope;
						}
						tempx = x - 1;
						tempy = y - 1 / perpendicularSlope;
						while (tempx >= 0 && tempy >= 0 && tempx < Height && tempy < Width && vBinaryMap[static_cast<int>(tempx) * Width + static_cast<int>(tempy)] != 0)
						{
							lineWidth++;
							tempx -= 1;
							tempy -= 1 / perpendicularSlope;
						}
					}
					else
					{
						double tempx = x, tempy = y;
						while (tempx >= 0 && tempy >= 0 && tempx < Height && tempy < Width && vBinaryMap[static_cast<int>(tempx) * Width + static_cast<int>(tempy)] != 0)
						{
							lineWidth++;
							tempx += perpendicularSlope;
							tempy += 1;
						}
						tempx = x - perpendicularSlope;
						tempy = y - 1;
						while (tempx >= 0 && tempy >= 0 && tempx < Height && tempy < Width && vBinaryMap[static_cast<int>(tempx) * Width + static_cast<int>(tempy)] != 0)
						{
							lineWidth++;
							tempx -= perpendicularSlope;
							tempy -= 1;
						}
					}
					lineWidthSet.push_back(lineWidth);
				}

				//删除极值
				int lowerThreshold = 1;
				int upperThreshold = 50;
				lineWidthSet.erase(std::remove_if(lineWidthSet.begin(), lineWidthSet.end(),
					[lowerThreshold, upperThreshold](int val) {
						return val < lowerThreshold || val > upperThreshold;
					}),
					lineWidthSet.end());

				//_ASSERT(lineWidthSet.size() != 0);
				if (lineWidthSet.size() == 0)
				{
					Sum += vMagicNumber;
				}
				else
				{
					// 平均数上下波动
					int count = 0;
					double TempSum = 0;
					double stand = 0;
					for (int j = 0; j < lineWidthSet.size(); j++)
					{
						stand += (lineWidthSet[j] / lineWidthSet.size());
					}
					for (int j = 0; j < lineWidthSet.size(); j++)
					{
						if ((lineWidthSet[j] > stand * 0.8) && (lineWidthSet[j] < stand * 1.2))
						{
							TempSum = TempSum + lineWidthSet[j];
							count++;
						}
					}
					if (count != 0)
						Sum += TempSum / count;
					else
						Sum += stand;
				}
			}
		}
		_ASSERT(Sum != 0 && Points.size() > 1);
		vioRoads[i].Width = (Sum / (Points.size() - 1)) * Ratio;
	}
	return true;
}

//*****************************************************************
//FUNCTION: 
std::vector<std::vector<uint32_t>> hiveObliquePhotography::CRoadMidlineExtracter::__dividePixelsByOpenCV(const cv::Mat& vBinaryMap)
{
	cv::Mat DividedMap;
	int Number = connectedComponents(vBinaryMap, DividedMap, 8, CV_16U);
	std::vector<std::vector<uint32_t>> Components(Number - 1);
	
	std::uint8_t* pPixelBuffer = vBinaryMap.data;
	for (int i = 0; i < vBinaryMap.rows; ++i)
	{
		for (int k = 0; k < vBinaryMap.cols; ++k)
		{
			int label = DividedMap.at<uint16_t>(i, k);
			if (label != 0)//0是背景
				Components[label - 1].push_back(i * vBinaryMap.cols + k);
		}
	}
	return Components;
}

//*****************************************************************
//FUNCTION: 
std::vector<uint32_t> CRoadMidlineExtracter::__getNeighbourhood(const uint32_t vCenter, const std::vector<bool>& vVisitedMap, std::vector<uint32_t>& voSinglePixelSet)
{
	std::vector<uint32_t> Neighbour;
	int Width = m_WidthInPixel;
	std::vector<int> NeighbourOffset = { -Width,1 - Width, 1,Width + 1,Width,Width - 1,-1,-1 - Width };
	std::vector<Eigen::Vector2i> CorrectNeighbourOffset = { Eigen::Vector2i(-1,0),Eigen::Vector2i(-1,1),Eigen::Vector2i(0,1),Eigen::Vector2i(1,1),Eigen::Vector2i(1,0),Eigen::Vector2i(1,-1),Eigen::Vector2i(0,-1),Eigen::Vector2i(-1,-1) };
	Eigen::Vector2i Coordinate(vCenter / m_WidthInPixel, vCenter % m_WidthInPixel);

	for (int i = 0; i < NeighbourOffset.size(); i++) {
		Eigen::Vector2i NeighbourCoordinate((vCenter + NeighbourOffset[i]) / m_WidthInPixel, (vCenter + NeighbourOffset[i]) % m_WidthInPixel);
		Eigen::Vector2i Distance = { NeighbourCoordinate.x() - Coordinate.x() , NeighbourCoordinate.y() - Coordinate.y() };
		if (NeighbourCoordinate.x() >= 0 && NeighbourCoordinate.y() >= 0 && NeighbourCoordinate.x() < m_WidthInPixel && NeighbourCoordinate.y() < m_HeightInPixel && Distance == CorrectNeighbourOffset[i] && vVisitedMap[(vCenter + NeighbourOffset[i])])
			Neighbour.push_back(vCenter + NeighbourOffset[i]);
	}
	return Neighbour;
}

//*****************************************************************
//FUNCTION: 
void CRoadMidlineExtracter::__doRegionGrow(const uint32_t vSeed, std::vector<bool>& vioVisitedMap, std::vector<uint32_t>& voConnectedPixels)
{
	std::vector<uint32_t> SeedPointSet;
	uint32_t SeedPoint;
	SeedPointSet.emplace_back(vSeed);
	while (SeedPointSet.size() != 0) {
		SeedPoint = SeedPointSet.back();
		SeedPointSet.pop_back();
		voConnectedPixels.emplace_back(vSeed);
		vioVisitedMap[SeedPoint] = false;
		std::vector<uint32_t> Neighbour = __getNeighbourhood(SeedPoint, vioVisitedMap, voConnectedPixels);
		if (Neighbour.size() != 0) {
			for (auto e : Neighbour) {
				vioVisitedMap[e] = false;
				SeedPointSet.push_back(e);
				voConnectedPixels.push_back(e);
			}
		}
	}
}

//*****************************************************************
//FUNCTION: 
std::vector<std::vector<uint32_t>> CRoadMidlineExtracter::__dividePixelsByRegionGrow(std::vector<bool>& vBinaryMap)
{
	std::vector<std::vector<uint32_t>> Regions;
	for (int i = 0; i < vBinaryMap.size(); i++)
		if (vBinaryMap[i]) {
			std::vector<uint32_t> ConnectedPixels;
			__doRegionGrow(i, vBinaryMap, ConnectedPixels);
			Regions.emplace_back(ConnectedPixels);
		}
	return Regions;
}