#include <chrono>
#include <iostream>
#include <common/CommonMicro.h>
#include <common/ConfigInterface.h>

#include "SmartOPInterface.h"

using namespace hiveObliquePhotography;

const std::string UserDefinedMetafile = TEST_DIR + std::string("Config/UserDefinedMetafile.xml");

const std::string DetailedCategoryMapInfo = TEST_DIR + std::string("DetailedCategoryMapInfo.xml");

int main()
{
	std::chrono::steady_clock::time_point StartMoment;
	std::chrono::steady_clock::time_point EndMoment;
	StartMoment = std::chrono::steady_clock::now();
	if (hiveGenerateDetailedCategoryMap(UserDefinedMetafile, DetailedCategoryMapInfo))
		std::cout << "Successfully generate detailed category map." << std::endl;
	else
	{
		std::cout << "An error occurred when generating detailed category map,see the information above." << std::endl;
		return 0;
	}
	EndMoment = std::chrono::steady_clock::now();
	std::cout << "The conversion process continues " << std::chrono::duration<double, std::ratio<1, 1000>>(EndMoment - StartMoment).count() << "ms." << std::endl;

	//根据生成的索引文件读取DetailedCategoryMap相关文件验证转换的正确性


}
