﻿#include <chrono>
#include <iostream>
#include <common/CommonMicro.h>
#include <common/ConfigInterface.h>

#include "TestInfoConfig.h"
#include "SmartOPInterface.h"

using namespace hiveObliquePhotography;

const std::string RawPointCloudMetafile = TEST_DIR + std::string("Config/RawPointCloudMetafile.xml");
const std::string UserDefinedMetafile = TEST_DIR + std::string("Config/UserDefinedMetafile.xml");
const std::string ProgramRunningParametersfile = TEST_DIR + std::string("Config/ProgramRunningParameters.xml");

const std::string MultiLayeredPointCloudInfoFileName = TEST_DIR + std::string("MultiLayeredPointCloudInfo.xml");


bool testBlockDim()
{
	auto BlockDim = CTestInfoConfig::getInstance()->getBlockDim();
	return BlockDim .first == 3 && BlockDim.second == 3;
}

bool testOverallPointNumber()
{
	auto NumFromOutputConfig = CTestInfoConfig::getInstance()->computeTotalPoints();
	return NumFromOutputConfig == 17891004;
}

int main()
{
	std::chrono::steady_clock::time_point StartMoment;
	std::chrono::steady_clock::time_point EndMoment;
	StartMoment = std::chrono::steady_clock::now();
	if (hiveConvertRawPointCloud2MultiLayered(RawPointCloudMetafile, UserDefinedMetafile, ProgramRunningParametersfile, MultiLayeredPointCloudInfoFileName))
		std::cout << "Successfully convert unorganized pointCloud to multiLayered pointCloud." << std::endl;
	else
	{
		std::cout << "An error occurred when converting unorganized pointCloud to multiLayered pointCloud,see the information above." << std::endl;
		return 0;
	}
	EndMoment = std::chrono::steady_clock::now();
	std::cout << "The conversion process continues " << std::chrono::duration<double, std::ratio<1, 1000>>(EndMoment - StartMoment).count() << "ms." << std::endl;

	//根据生成的索引文件读取Block相关文件验证转换的正确性
 
	_HIVE_EARLY_RETURN(hiveConfig::hiveParseConfig(MultiLayeredPointCloudInfoFileName, hiveConfig::EConfigType::XML, CTestInfoConfig::getInstance()) != hiveConfig::EParseResult::SUCCEED,
		_FORMAT_STR1("Fail to parse the specified metafile[%1%].", MultiLayeredPointCloudInfoFileName), 0);

	_HIVE_EARLY_RETURN(!CTestInfoConfig::getInstance()->computeSceneInfo(),
		"Init failed, MultiLayeredPointCloudInfo may be incomplete.", 0);

	hiveEventLogger::hiveOutputEvent(_FORMAT_STR1("The No.1 test result is [%1%].", testBlockDim()));
	hiveEventLogger::hiveOutputEvent(_FORMAT_STR1("The No.2 test result is [%1%].", testOverallPointNumber()));
}

