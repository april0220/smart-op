#pragma once
#include "common/Singleton.h"
#include "common/HiveConfig.h"

struct SBlockInfo
{
	std::pair<std::uint32_t, std::uint32_t> m_BlockId;
	std::pair<float, float> m_BlockOriginCoordinate;
	std::vector<std::string> m_RelatedFileSet;
};

class CTestInfoConfig : public hiveDesignPattern::CSingleton<CTestInfoConfig>, public hiveConfig::CHiveConfig
{
public:
	~CTestInfoConfig() = default;
	[[nodiscard]] bool computeSceneInfo();
	std::uint32_t computeTotalPoints();
	std::pair<std::uint32_t, std::uint32_t> getBlockDim();

private:
	CTestInfoConfig() = default;
	virtual void __defineAttributesV() override;
	bool __isSubconfigValid(const hiveConfig::CHiveConfig* vConfig, std::string vConfigType);
	
	std::uint32_t __readBlock4PointNumber(const std::string& vFilePath);
	std::vector<SBlockInfo> m_SceneBlockInfo;
	friend class hiveDesignPattern::CSingleton<CTestInfoConfig>;
};
