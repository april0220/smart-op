#include "TestInfoConfig.h"
#include "SmartOPCommon.h"
#include <string>
#include <sstream>


bool CTestInfoConfig::__isSubconfigValid(const hiveConfig::CHiveConfig* vConfig, std::string vConfigType)
{
	if (vConfig && _IS_STR_IDENTICAL(vConfig->getSubconfigType(), vConfigType))return true;
	return false;
}

bool CTestInfoConfig::computeSceneInfo()
{
	const hiveConfig::CHiveConfig* pConfig = getSubconfigAt(0);
	if (!__isSubconfigValid(pConfig, std::string("MultiLayeredPointCloudInfo")))return false;

	const hiveConfig::CHiveConfig* pParameterConfig = pConfig->getSubconfigAt(0);
	if (!__isSubconfigValid(pParameterConfig, std::string("MultiLayeredPointCloudParameter")))return false;
	std::optional<std::string> BlockDimStr = pParameterConfig->getAttribute<std::string>("BlockDim");
	_ASSERTE(BlockDimStr.has_value());
	std::stringstream String2Int;
	String2Int << BlockDimStr.value();
	int DimX, DimY;
	String2Int >> DimX >> DimY;

	for (int i = 0; i < DimX * DimY; i++)
	{
		const hiveConfig::CHiveConfig* pBlockConfig = pConfig->getSubconfigAt(i + 1);
		if (!__isSubconfigValid(pBlockConfig, std::string("Block")))return false;

		std::optional<std::string> BlockIDStr = pBlockConfig->getAttribute<std::string>("BlockID");
		_ASSERTE(BlockIDStr.has_value());
		String2Int.clear();
		String2Int << BlockIDStr.value();
		std::uint32_t BlockIDX, BlockIDY;
		String2Int >> BlockIDX >> BlockIDY;

		std::optional<std::string> OriginCoordinateStr = pBlockConfig->getAttribute<std::string>("OriginCoordinate");
		_ASSERTE(OriginCoordinateStr.has_value());
		std::stringstream String2Float;
		String2Float << OriginCoordinateStr.value();
		float CoordinateX, CoordinateY;
		String2Float >> CoordinateX >> CoordinateY;

		const hiveConfig::CHiveConfig* pFileNameConfig = pBlockConfig->getSubconfigAt(0);
		if (!__isSubconfigValid(pFileNameConfig, std::string("FileName")))return false;

		std::vector<std::string> RelatedFileSetName;
		RelatedFileSetName.reserve(8);
		RelatedFileSetName.emplace_back("NeedProcessingPositionFileName");
		RelatedFileSetName.emplace_back("NeedProcessingNoramlFileName");
		RelatedFileSetName.emplace_back("NeedProcessingColorFileName");
		RelatedFileSetName.emplace_back("NeedProcessingCategoryFileName");
		RelatedFileSetName.emplace_back("NoNeedProcessingPositionFileName");
		RelatedFileSetName.emplace_back("NoNeedProcessingNoramlFileName");
		RelatedFileSetName.emplace_back("NoNeedProcessingColorFileName");
		RelatedFileSetName.emplace_back("NoNeedProcessingCategoryFileName");

		std::vector<std::string> RelatedFileSet;
		for (auto Index : RelatedFileSetName)
		{
			std::optional<std::string> FileName = pFileNameConfig->getAttribute<std::string>(Index);
			_ASSERTE(FileName.has_value());
			RelatedFileSet.emplace_back(FileName.value());
		}

		SBlockInfo Temp;
		Temp.m_BlockId = std::make_pair(BlockIDX, BlockIDY);
		Temp.m_BlockOriginCoordinate = std::make_pair(CoordinateX, CoordinateY);
		Temp.m_RelatedFileSet = std::move(RelatedFileSet);

		m_SceneBlockInfo.emplace_back(Temp);
	}
}

std::uint32_t CTestInfoConfig::computeTotalPoints()
{
	std::uint32_t Sum = 0;
	for (auto& BlockInfo : m_SceneBlockInfo)
	{
		for (auto& FileName : BlockInfo.m_RelatedFileSet)
			Sum += __readBlock4PointNumber(FileName);
	}
	return Sum / 4;
}

std::pair<std::uint32_t, std::uint32_t> CTestInfoConfig::getBlockDim()
{
	const hiveConfig::CHiveConfig* pParameterConfig = getSubconfigAt(0)->getSubconfigAt(0);
	std::optional<std::string> BlockDimStr = pParameterConfig->getAttribute<std::string>("BlockDim");
	_ASSERTE(BlockDimStr.has_value());
	std::stringstream String2Int;
	String2Int << BlockDimStr.value();
	int DimX, DimY;
	String2Int >> DimX >> DimY;
	return std::make_pair(DimX, DimY);
}

std::uint32_t CTestInfoConfig::__readBlock4PointNumber(const std::string& vFilePath)
{
	std::string FileName = OUTPUT_DIR + vFilePath;
	std::ifstream ReadFile(FileName, std::ios::in | std::ios::binary);
	_HIVE_EARLY_RETURN(ReadFile.fail(), _FORMAT_STR1("Fail to open the specified file named %1%", FileName), 0);

	auto extractNumFromString = [](const char* vString) {
		int Num = 0;
		_HIVE_EARLY_RETURN(vString == nullptr, "The specified string is Null!", Num);
		while (*vString != '\0')
		{
			_HIVE_SIMPLE_IF((*vString >= '0' && *vString <= '9'), (Num = (Num * 10 + *vString - '0')));
			vString++;
		}
		return Num;
	};

	int ElementNum = 0;
	char FileHeader[64];
	for (int HeaderLine = 0, HeaderEntryNum = 8; HeaderLine < HeaderEntryNum; HeaderLine++)
	{
		ReadFile.getline(FileHeader, sizeof(FileHeader), '\n');
		if (HeaderLine == 5)   ElementNum = extractNumFromString(FileHeader);
	}
	ReadFile.close();
	return ElementNum;
}


void CTestInfoConfig::__defineAttributesV()
{
	_defineAttribute("MultiLayeredPointCloudInfo", hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute("MultiLayeredPointCloudParameter", hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute("LatitudeAndLongitudeSpan", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("WorldScale", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("OriginCoordinate", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("BlockDim", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("BlockScale", hiveConfig::EConfigDataType::ATTRIBUTE_FLOAT);
	_defineAttribute("CellDim", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("CellScale", hiveConfig::EConfigDataType::ATTRIBUTE_FLOAT);

	_defineAttribute("Block", hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);
	_defineAttribute("BlockID", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("FileName", hiveConfig::EConfigDataType::ATTRIBUTE_SUBCONFIG);

	_defineAttribute("NeedProcessingPositionFileName", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("NeedProcessingNoramlFileName", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("NeedProcessingColorFileName", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("NeedProcessingCategoryFileName", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("NoNeedProcessingPositionFileName", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("NoNeedProcessingNoramlFileName", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("NoNeedProcessingColorFileName", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("NoNeedProcessingCategoryFileName", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
	_defineAttribute("OriginCoordinate", hiveConfig::EConfigDataType::ATTRIBUTE_STRING);
}