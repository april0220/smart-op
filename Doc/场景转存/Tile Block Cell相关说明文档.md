## Tile Block Cell相关说明文档  
### Tile Block Cell
**Tile**为点云文件读入的单位，场景输入文件matedata组织的单位。  
**Block**为程序存储管理的单位，这是一个程序内部的概念。输入的若干点云组成的场景会被划分为空间连续的Bolck进行重新组织存储。为了方便计算与管理，Block要求大小一致（空间范围一致，保存成文件大小可不一致）且为正方形，单个Block可以跨越多个Tile。  
**Cell**为Block在空间上的进一步细分，但它是一种逻辑上的划分，用于后续的邻域计算或高程图生成，并没有物理的实体。Block被划分为空间连续大小一致且完整的Cell，这就意味着Cell不会跨越Block。 
   
三者的对应关系与实际场景下的划分如下示意图，红色Tile，蓝色Block，绿色Cell。
![attributes](pic/init_1.png) 

### Block的划分与初始化  
Block作为内存管理的单元，需要在多层点云初始化的时候进行划分。当前的Block划分方式是：  
计算需要的数据： 经纬度跨度(实际跨度) LongitudeSpan、LatitudeSpan  
&ensp; 场景总文件大小 OverallSceneSizeMB  
&ensp; 经纬度与世界坐标的比例尺 WorldScale  
&ensp; 期待Block文件大小 ExpectedBlockSizeMB  
计算过程的伪代码为：   
```
EstimatedBlockNumber = OverallSceneSizeMB / ExpectedBlockSizeMB;
AspectRatio = LongitudeSpan / LatitudeSpan;
//使用round还是ceil值得推敲,下同
BlockDimY = ceil(sqrt(EstimatedBlockNumber / AspectRatio));
BlockScale = LatitudeSpan / BlockDimY;
BlockDimX = ceil(LongitudeSpan / BlockScale);
BlockScale = BlockScale / WorldScale;
``` 
需要注意的是，Block的计算是根据经纬度计算得出，而之后的计算点属于哪个Block时使用的点世界坐标（点的经纬度无法获得），而经纬度与世界坐标对应关系未知，这就需要在初始化多层点云时需要计算合理的OriginX与OriginY。为了保证所有的点能够正确计算到Block中，OriginX与OriginY的值由左下角点云包围盒最小坐标偏移得到，同时需要修改Block大小，计算过程伪代码为：  
```
//扩大Block大小，BlockDim保持不变
LocalCellDim = ceil(BlockScale / CellScale);
LocalCellDim ++;
BlockScale = LocalCellDim * CellScale;  

//OriginX OriginY在包围盒基础上偏移，保证所有的点都在划分的Block内
OriginX = BoundingBox.first.x - BlockDimX * CellScale;
OriginY = BoundingBox.first.y - BlockDimY * CellScale;
```
在Block初始化时，还需要计算该Block跨越了哪几个Tile（TileContributerSet）。由于从输入数据只能得到每个Tile的跨度，这时就需要使用到多层级点云的OriginLongitude与OriginLatitude，经过测试计算（已知Tile经纬跨度换算与包围盒）,在这时令
```
OriginLongitude = BoundingBox.first.x;
OriginLatitude = BoundingBox.first.y;
```
在计算TileContributerSet产生的误差可以忽略。  
至此，实际的Tile Block对应图如下，红色为Tile，蓝色为Block。  
![attributes](pic/init_2.png)   

由于划分Block从OriginX OriginY开始，所以计算TileContributerSet时要加上与OriginLongitude OriginLatitude的偏移。