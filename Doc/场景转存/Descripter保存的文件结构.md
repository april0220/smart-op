注1：后缀名drd为descriptor region data的缩写



descriptor_Road.drd

```
drd
format ascii 1.0
comment Created by Smartop v1
comment Created 2022-1-17T20:00:00
element DetailedRegion USER_DEFINED_ROAD 300
property int SkeletonVertexOffset
property uint8_t Red
property uint8_t Green
property uint8_t Blue
property int SeedPointOffset
end_header
4099
16387
16389
40964
64 0 1
4099
...
```

descriptor_River.drd

```
drd
format ascii 1.0
comment Created by Smartop v1
comment Created 2022-1-17T20:00:00
element DetailedRegion USER_DEFINED_RIVER 2
property uint8_t Red
property uint8_t Green
property uint8_t Blue
property int SeedPointOffset
end_header
96 0 1
8196
...
```

descriptor_Landscape.drd

```
drd
format ascii 1.0
comment Created by Smartop v1
comment Created 2022-1-17T20:00:00
element DetailedRegion USER_DEFINED_LANDSCAPE 20
property uint8_t Red
property uint8_t Green
property uint8_t Blue
property int SeedPointOffset
end_header
32 0 1
40964
...
```

descriptor_Building.drd

```
drd
format ascii 1.0
comment Created by Smartop v1
comment Created 2022-1-17T20:00:00
element DetailedRegion USER_DEFINED_BUILDING 30
property uint8_t Red
property uint8_t Green
property uint8_t Blue
property int SeedPointOffset
end_header
128 0 1
24582
...
```

