注1：后缀名bpd为block point data的缩写

注2：如果normal为空，则block_x_y_Normal.bpd只有文件头，且point normal数量为0



block_x_y_Position.bpd

```
bpd
format ascii 1.0
comment Created by SmartObliquePhotography v1
comment Created 2021-11-23T16:47:07
block x y
element point position 2651383
property float x
property float y
property float z
end_header
-3335.771240 19221.537109 430.207184
-3335.762207 19221.701172 430.282837
...
```

block_x_y_Normal.bpd

```
bpd
format ascii 1.0
comment Created by SmartObliquePhotography v1
comment Created 2021-11-23T16:47:07
block x y
element point normal 2651383
property float nx
property float ny
property float nz
end_header
0.428503 -0.658594 0.618578 
0.539182 -0.257869 0.801740 
...
```

block_x_y_Color.bpd

```
bpd
format ascii 1.0
comment Created by SmartObliquePhotography v1
comment Created 2021-11-23T16:47:07
block x y
element point color 2651383
property uint8_t red
property uint8_t green
property uint8_t blue
end_header
89 104 45
72 90 38
...
```

block_x_y_Category.bpd

```
bpd
format ascii 1.0
comment Created by SmartObliquePhotography v1
comment Created 2021-11-23T16:47:07
block x y
element point category 2651383
property uint16_t category
end_header
4
8
...
```

