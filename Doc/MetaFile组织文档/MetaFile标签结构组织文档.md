## MetaFile标签结构组织文档

***

Metadata   
├── Parameters  
│ &emsp; ├── MAX_NUM_POINT_CLOUD_CONVERT_TASK  
│ &emsp; └── MAX_NUM_LOOP_WAIT_POINT_CLOUD_CONVERT_DONE  
├── Placemark  
│ &emsp; └── BITMAP  
│&emsp; &emsp; &emsp; ├── CategoryLegend  
│&emsp; &emsp; &emsp; │ &emsp; └── XML  
│&emsp; &emsp; &emsp; │ &emsp; &emsp; &emsp;├── Directory  
│&emsp; &emsp; &emsp; │ &emsp; &emsp; &emsp;└── File  
│&emsp; &emsp; &emsp; │&emsp; &emsp; &emsp; &emsp; &emsp;└── Name  
│&emsp; &emsp; &emsp; └── JPG  
│&emsp; &emsp; &emsp; &emsp; &emsp;├── Directory  
│&emsp; &emsp; &emsp; &emsp; &emsp;├── NUM  
│&emsp; &emsp; &emsp; &emsp; &emsp;└── FileSet  
│&emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp;├── File  
│&emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp;⋮&emsp;&emsp;⋮  
│&emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp;└── File  
│&emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp;&emsp;&emsp; ├── Name  
│&emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp;&emsp;&emsp; └── LinearRing  
│&emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp;&emsp;&emsp; &emsp;&emsp; ├── Coordinates  
│&emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp;&emsp;&emsp; &emsp;&emsp; ├── Coordinates  
│&emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp;&emsp;&emsp; &emsp;&emsp; ├── Coordinates  
│&emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp;&emsp;&emsp; &emsp;&emsp; └── Coordinates  
└── Model  
&emsp;&emsp; └── Ply  
&emsp; &emsp; &emsp;&emsp;├── Directory  
&emsp; &emsp; &emsp;&emsp;├── TileNum  
&emsp; &emsp; &emsp;&emsp;└── TileSet  
&emsp; &emsp; &emsp;&emsp;&emsp;&emsp; ├── Tile  
&emsp; &emsp; &emsp;&emsp;&emsp;&emsp; ⋮&emsp;&emsp;⋮  
&emsp; &emsp; &emsp;&emsp;&emsp;&emsp; └── Tile  
&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;&emsp; &emsp; ├── Name  
&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;&emsp; &emsp; └── LinearRing  
&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;&emsp; &emsp; &emsp; &emsp;├── Coordinates  
&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;&emsp; &emsp; &emsp; &emsp;├── Coordinates  
&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;&emsp; &emsp; &emsp; &emsp;├── Coordinates  
&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;&emsp; &emsp; &emsp; &emsp;└── Coordinates  

