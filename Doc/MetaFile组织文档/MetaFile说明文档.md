## MetaFile说明文档 

***

**MetaFile**由**Parameters**、**Placemark**、**Model**三部分组成。  
整个文件最大标签为&lt;Metadata/&gt;，下属&lt;Parameters/&gt;、&lt;Placemark/&gt;、&lt;Model/&gt;三个子标签

***

#### $Parameters$
**Parameters**表示程序需要使用到的参数，包括：  
+ MAX_NUM_POINT_CLOUD_CONVERT_TASK，表示支持的最大的点云转换Task数目
+ MAX_NUM_LOOP_WAIT_POINT_CLOUD_CONVERT_DONE，表示点云转换中等待Task完成的最大循环次数

#### $Placemark$
**Placemark**表示点云Tile的标识文件，包括：  
+ BITMAP，位图  
  + CategoryLegend，图例  
    + XML，图例文件的格式  
      + Directory，文件目录  
      + File，图例文件  
        + Name，图例文件名  
  + JPG，位图本体  
    + Directory，文件目录  
    + NUM，位图文件数量  
    + FileSet，位图文件集合  
      + File，位图文件  
        + Name，位图文件名  
        + LinearRing，位图在经纬度上覆盖的区域  
          + Coordinates，位图4个角经纬度坐标

#### $Model$
**Model**表示点云Tile，包括：
+ PLY，点云模型格式  
  + Directory，文件目录  
  + TileNum，Tile数量  
  + TileSet，Tile文件集合  
    + Tile，点云文件  
      + Name，点云文件名
      + LinearRing，该Tile在经纬度上覆盖的区域  
        + Coordinates，位图4个角经纬度坐标  



说明：  
1. 结合文件目录+文件名，可以得到文件路径
2. 位图图例是另一个XML，通过解析XML的方式，得到类别和颜色的对应关系
3. MetaFile中的Tile在地理空间上最好是连续的




