**Q：`std::async`的执行机制是什么？系统会启动一个线程去完成这个任务吗？如果启动了一个线程，那和自己用`std::thread`有什么区别**？

A 1：`std::async`执行机制（`std::launch::async`的启动策略）：

- 创建一个线程和一个`std::promise`对象
- 将`std::promise`对象传递给线程函数，并返回相关的`std::future`对象
- 当我们传递参数的函数退出时，它的值将被设置在这个`std::promise`对象中，所以最终的返回值将在`std::future`对象中可用

A 2：如果指定用`std::launch::async`的启动策略，则`std::async`会启动一个新线程，否则不一定。具体如下：

- `std::launch::deferred`

  延迟到调用 get() 或者 wait() 时执行，如果不调用就不会执行

- `std::launch::async`

  强制这个异步任务在新线程上执行，系统必须要创建出新线程来运行入口函数。

- `std::launch::async | std::launch::deferred `

  std::async可能创建新线程立即执行， 也可能没有创建新线程并且延迟到调用get()执行，取决于系统负载。

A 3：和自己用std::thread的区别在于，std::async获取线程执行的任务的返回值更为方便。例如：

```C++
//用std::async
std::future Future = std::async([](bool flag)->bool
{
    if (flag)
        throw std::exception("Hi!");
    return flag;
}, true); // <- pass true to lambda
// do some work in parallel...
try
{
    bool flag = Future.get();
}
catch(std::exception & e)
{
    std::cout << e.what() << std::endl;
}
```

```c++
//用std::thread（还需要std::promise和std::future来获得返回值）
std::promise Promise;
std::thread Thread = std::thread([](std::promise<bool> & Promise, bool flag)
{
   if (flag)
     Promise.set_exception(std::make_exception_ptr(std::exception("Hi!")));
   else
     Promise.set_value(flag);
}, std::ref(Promise), true);
// do some work in parallel...
Thread.join();
std::future Future = Promise.get_future();
try
{
   bool flag = Future.get();
}
catch(std::exception & e)
{
   std::cout << e.what() << std::endl;
}
```



**Q：`std::async`中可以使用`std::mutex`、`std::condition_variable`等方式来和其他任务来访问共享资源，或者阻塞当前任务的执行，等待其他任务的执行结果吗？**

A：可以。`std::async`只是对`std::thread`的封装，不影响线程的特性。



**Q：`std::async`和`std::thread`的各自应用场景是什么？**

A：`std::async`和`std::thead`适合的任务粒度不同：

- `std::async`适用于执行负载比较大的任务，比如长时间的计算或大规模IO。因为频繁的进程创建会抵消掉并行带来的性能优势。

- 而对于小规模的任务，用`std::thread`实现的线程池更合适（或者微软的PPL、英特尔的TBB）



参考链接：

https://blog.csdn.net/lijinqi1987/article/details/78909479

https://www.cnblogs.com/chengyuanchun/p/5394843.html

https://stackoverflow.com/questions/17963172/why-should-i-use-stdasync

https://stackoverflow.com/questions/25814365/when-to-use-stdasync-vs-stdthreads?noredirect=1&lq=1

https://bartoszmilewski.com/2011/10/10/async-tasks-in-c11-not-quite-there-yet/