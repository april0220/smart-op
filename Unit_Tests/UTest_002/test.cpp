#include "pch.h"
#include "PointCloudFileLoader.h"

using namespace hiveObliquePhotography;

class TEST_LoadUnorganizedPointCloud : public testing::Test
{
protected:
	void SetUp() override
	{
	}

	void TearDown() override
	{
	}

	const std::string m_PLYFileLoaderSig = MetafileKeywords::PLY_LOADER_SIG;
	const std::string m_PCDFileLoaderSig = MetafileKeywords::PCD_LOADER_SIG;

	const int PointNumber = 2651383;
	const std::string m_PLYFileName = TESTDATA_DIR + std::string("UTest_002_Data/Tile_+377_-10903.ply");
	const std::string m_PLYFileName_WithoutNormal = TESTDATA_DIR + std::string("UTest_002_Data/Tile_+377_-10903_WithoutNormal.ply");
	const std::string m_PCDFileName = TESTDATA_DIR + std::string("UTest_002_Data/Tile_+377_-10903.pcd");
	const std::string m_BadFormatFileName = TESTDATA_DIR + std::string("UTest_002_Data/bad_tile.txt");
};

//测试点：读入PLY格式的点云
TEST_F(TEST_LoadUnorganizedPointCloud, NT_LoadPLYFile)
{
	IPointCloudFileLoader* pLoader = hiveDesignPattern::hiveGetOrCreateProduct<IPointCloudFileLoader>(m_PLYFileLoaderSig);
	ASSERT_NE(pLoader, nullptr);

	SUnorganizedPointCloud* pPointCloud = pLoader->load(m_PLYFileName);
	ASSERT_NE(pPointCloud, nullptr);
	EXPECT_TRUE(pPointCloud->isValid());
	EXPECT_EQ(pPointCloud->_PositionSet.size(), PointNumber);
	EXPECT_EQ(pPointCloud->_PositionSet[0], Eigen::Vector3f(-3335.77124, 19221.5371, 430.207184));
	EXPECT_EQ(pPointCloud->_NormalSet[0], Eigen::Vector3f(0.428502977, -0.658594072, 0.618578255));
	EXPECT_EQ(pPointCloud->_ColorSet[0], SRGBColor(89, 104, 45));
	EXPECT_EQ(pPointCloud->_PositionSet[11442], Eigen::Vector3f(-3314.57300, 19233.1602, 430.091736));
	EXPECT_EQ(pPointCloud->_NormalSet[11442], Eigen::Vector3f(-0.150930017, -0.126388550, 0.980431557));
	EXPECT_EQ(pPointCloud->_ColorSet[11442], SRGBColor(52, 59, 28));
	EXPECT_EQ(pPointCloud->_PositionSet[21779], Eigen::Vector3f(-3324.04761, 19252.8320, 430.012421));
	EXPECT_EQ(pPointCloud->_NormalSet[21779], Eigen::Vector3f(0.607167006, 0.620622218, 0.496161401));
	EXPECT_EQ(pPointCloud->_ColorSet[21779], SRGBColor(22, 28, 24));
	EXPECT_EQ(pPointCloud->_PositionSet[39814], Eigen::Vector3f(-3317.20996, 19269.6621, 430.178772));
	EXPECT_EQ(pPointCloud->_NormalSet[39814], Eigen::Vector3f(-0.609516442, -0.565617740, 0.555487275));
	EXPECT_EQ(pPointCloud->_ColorSet[39814], SRGBColor(87, 96, 105));
	EXPECT_EQ(pPointCloud->_PositionSet[49999], Eigen::Vector3f(-3293.17822, 19233.8281, 429.937469));
	EXPECT_EQ(pPointCloud->_NormalSet[49999], Eigen::Vector3f(-0.0282323770, -0.0449626744, 0.998589635));
	EXPECT_EQ(pPointCloud->_ColorSet[49999], SRGBColor(226, 222, 223));

	delete pPointCloud;
}

//测试点：读入不带法线的PLY格式的点云
TEST_F(TEST_LoadUnorganizedPointCloud, NT_LoadPLYFile_WithoutNormal)
{
	IPointCloudFileLoader* pLoader = hiveDesignPattern::hiveGetOrCreateProduct<IPointCloudFileLoader>(m_PLYFileLoaderSig);
	ASSERT_NE(pLoader, nullptr);

	SUnorganizedPointCloud* pPointCloud = pLoader->load(m_PLYFileName);
	ASSERT_NE(pPointCloud, nullptr);
	EXPECT_TRUE(pPointCloud->isValid());
	EXPECT_EQ(pPointCloud->_PositionSet.size(), PointNumber);
	EXPECT_EQ(pPointCloud->_PositionSet[0], Eigen::Vector3f(-3335.77124, 19221.5371, 430.207184));
	EXPECT_EQ(pPointCloud->_ColorSet[0], SRGBColor(89, 104, 45));
	EXPECT_EQ(pPointCloud->_PositionSet[11442], Eigen::Vector3f(-3314.57300, 19233.1602, 430.091736));
	EXPECT_EQ(pPointCloud->_ColorSet[11442], SRGBColor(52, 59, 28));
	EXPECT_EQ(pPointCloud->_PositionSet[21779], Eigen::Vector3f(-3324.04761, 19252.8320, 430.012421));
	EXPECT_EQ(pPointCloud->_ColorSet[21779], SRGBColor(22, 28, 24));
	EXPECT_EQ(pPointCloud->_PositionSet[39814], Eigen::Vector3f(-3317.20996, 19269.6621, 430.178772));
	EXPECT_EQ(pPointCloud->_ColorSet[39814], SRGBColor(87, 96, 105));
	EXPECT_EQ(pPointCloud->_PositionSet[49999], Eigen::Vector3f(-3293.17822, 19233.8281, 429.937469));
	EXPECT_EQ(pPointCloud->_ColorSet[49999], SRGBColor(226, 222, 223));

	delete pPointCloud;
}

//测试点：读入PCD格式的点云
TEST_F(TEST_LoadUnorganizedPointCloud, NT_LoadPCDFile)
{
	IPointCloudFileLoader* pLoader = hiveDesignPattern::hiveGetOrCreateProduct<IPointCloudFileLoader>(m_PCDFileLoaderSig);
	ASSERT_NE(pLoader, nullptr);

	SUnorganizedPointCloud* pPointCloud = pLoader->load(m_PCDFileName);
	ASSERT_NE(pPointCloud, nullptr);
	EXPECT_TRUE(pPointCloud->isValid());
	EXPECT_EQ(pPointCloud->_PositionSet.size(), PointNumber);
	EXPECT_EQ(pPointCloud->_PositionSet[0], Eigen::Vector3f(-3335.77124, 21.5371094, 430.207184));
	EXPECT_EQ(pPointCloud->_NormalSet[0], Eigen::Vector3f(0.428502977, -0.658594072, 0.618578255));
	EXPECT_EQ(pPointCloud->_ColorSet[0], SRGBColor(89, 104, 45));
	EXPECT_EQ(pPointCloud->_PositionSet[11442], Eigen::Vector3f(-3314.57300, 33.1601562, 430.091736));
	EXPECT_EQ(pPointCloud->_NormalSet[11442], Eigen::Vector3f(-0.150930017, -0.126388550, 0.980431557));
	EXPECT_EQ(pPointCloud->_ColorSet[11442], SRGBColor(52, 59, 28));
	EXPECT_EQ(pPointCloud->_PositionSet[21779], Eigen::Vector3f(-3324.04761, 52.8320312, 430.012421));
	EXPECT_EQ(pPointCloud->_NormalSet[21779], Eigen::Vector3f(0.607167006, 0.620622218, 0.496161401));
	EXPECT_EQ(pPointCloud->_ColorSet[21779], SRGBColor(22, 28, 24));
	EXPECT_EQ(pPointCloud->_PositionSet[39814], Eigen::Vector3f(-3317.20996, 69.6621094, 430.178772));
	EXPECT_EQ(pPointCloud->_NormalSet[39814], Eigen::Vector3f(-0.609516442, -0.565617740, 0.555487275));
	EXPECT_EQ(pPointCloud->_ColorSet[39814], SRGBColor(87, 96, 105));
	EXPECT_EQ(pPointCloud->_PositionSet[49999], Eigen::Vector3f(-3293.17822, 33.8281250, 429.937469));
	EXPECT_EQ(pPointCloud->_NormalSet[49999], Eigen::Vector3f(-0.0282323770, -0.0449626744, 0.998589635));
	EXPECT_EQ(pPointCloud->_ColorSet[49999], SRGBColor(226, 222, 223));

	delete pPointCloud;
}

//测试点：读入的点云文件其内部格式有错，期待返回空指针 
TEST_F(TEST_LoadUnorganizedPointCloud, DT_BadFileFormat)
{
	IPointCloudFileLoader* pLoader = hiveDesignPattern::hiveGetOrCreateProduct<IPointCloudFileLoader>(m_PLYFileLoaderSig);
	ASSERT_NE(pLoader, nullptr);
	SUnorganizedPointCloud* pPointCloud = pLoader->load(m_BadFormatFileName);
	EXPECT_EQ(pPointCloud, nullptr);
	delete pPointCloud;
}
