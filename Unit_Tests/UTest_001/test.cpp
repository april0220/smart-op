#include "pch.h"

using namespace hiveObliquePhotography;

class Test_ParseMetafile : public testing::Test
{
protected:
	void SetUp() override
	{
		ASSERT_EQ(hiveConfig::hiveParseConfig(m_RawPointCloudMetafileName, hiveConfig::EConfigType::XML, CRawPointCloudConfig::getInstance()), hiveConfig::EParseResult::SUCCEED);
		ASSERT_EQ(hiveConfig::hiveParseConfig(m_ProgramRunningParametersMetafileName, hiveConfig::EConfigType::XML, CProgramRunningParametersConfig::getInstance()), hiveConfig::EParseResult::SUCCEED);
		ASSERT_EQ(hiveConfig::hiveParseConfig(m_UserDefinedMetafileName, hiveConfig::EConfigType::XML, CUserDefinedCategoryMapConfig::getInstance()), hiveConfig::EParseResult::SUCCEED);
	}

	void TearDown() override
	{
		CRawPointCloudConfig::getInstance()->destroy();
	}

private:
	const std::string m_ProgramRunningParametersMetafileName = TESTDATA_DIR + std::string("FullDataSetConfig\\ProgramRunningParameters.xml");
	const std::string m_UserDefinedMetafileName = TESTDATA_DIR + std::string("FullDataSetConfig\\UserDefinedMetafile.xml");
	const std::string m_RawPointCloudMetafileName = TESTDATA_DIR + std::string("FullDataSetConfig\\RawPointCloudMetafile.xml");
};

//测试目的：下面这些属性用户可以通过metafile来定义，但如果用户未定义，它们应该有默认值 
TEST_F(Test_ParseMetafile, NT_CheckDefaultValue)
{
	EXPECT_TRUE(CRawPointCloudConfig::getInstance()->isAttributeExisted(MetafileKeywords::CELL_SCALE));
	EXPECT_TRUE(CRawPointCloudConfig::getInstance()->isAttributeExisted(MetafileKeywords::WORLD_SCALE_X));
	EXPECT_TRUE(CRawPointCloudConfig::getInstance()->isAttributeExisted(MetafileKeywords::WORLD_SCALE_Y));

	const hiveConfig::CHiveConfig* ParametersConfig = CProgramRunningParametersConfig::getInstance()->getParametersPtr();
	EXPECT_TRUE(ParametersConfig->isAttributeExisted(MetafileKeywords::MAX_NUM_POINT_CLOUD_CONVERT_TASK));
	EXPECT_TRUE(ParametersConfig->isAttributeExisted(MetafileKeywords::MAX_NUM_LOOP_WAIT_POINT_CLOUD_CONVERT_DONE));
	EXPECT_TRUE(ParametersConfig->isAttributeExisted(MetafileKeywords::MAX_NUM_LOOP_VISIT_MEMORY));
	EXPECT_TRUE(ParametersConfig->isAttributeExisted(MetafileKeywords::EXPECTED_BLOCK_SIZE_IN_MB));
}

//测试目的：下面这些属性不能通过metafile定义，需要通过CRawPointCloudConfig::generateSceneInfo()推导出来	
TEST_F(Test_ParseMetafile, NT_GenerateSceneInfoFromMetaFile)
{
	EXPECT_FALSE(CRawPointCloudConfig::getInstance()->isAttributeExisted(MetafileKeywords::OVERALL_SCENE_SIZE_IN_MB));
	ASSERT_TRUE(CRawPointCloudConfig::getInstance()->init());
	EXPECT_TRUE(CRawPointCloudConfig::getInstance()->isAttributeExisted(MetafileKeywords::OVERALL_SCENE_SIZE_IN_MB));
}

//测试目的：COverAllConfig能够通过getFirstPointCloudTileFile()/getNextPointCloudTileFile()来遍历所有的point cloud tile文件
TEST_F(Test_ParseMetafile, NT_TraverseAllPointTileFiles)
{
	std::string FileName;
	std::uint32_t TileX, TileY;
	std::tie(FileName, TileX, TileY) = CRawPointCloudConfig::getInstance()->getFirstPointCloudTileFile();
	EXPECT_TRUE(FileName.empty());

	ASSERT_TRUE(CRawPointCloudConfig::getInstance()->init());

	std::vector<std::pair<std::uint32_t, std::uint32_t>> TileSet;
	std::tie(FileName, TileX, TileY) = CRawPointCloudConfig::getInstance()->getFirstPointCloudTileFile();
	EXPECT_EQ(TileX, 0);
	EXPECT_EQ(TileY, 0);
	EXPECT_FALSE(FileName.empty());
	TileSet.push_back(std::make_pair(TileX, TileY));
	std::uint32_t Counter = 0;
	while (!FileName.empty())
	{
		std::tie(FileName, TileX, TileY) = CRawPointCloudConfig::getInstance()->getNextPointCloudTileFile();
		if (FileName.empty()) break;
		TileSet.push_back(std::make_pair(TileX, TileY));
		Counter++;
		if (Counter > 10000) ASSERT_TRUE(false);  //如果getNextPointCloudTileFile()有bug，导致FileName始终不为空，这个机制来打破死循环
	}

	EXPECT_EQ(CRawPointCloudConfig::getInstance()->getNumPointCloudTile(), TileSet.size());

	std::sort(TileSet.begin(), TileSet.end());
	TileSet.erase(std::unique(TileSet.begin(), TileSet.end()), TileSet.end());
	EXPECT_EQ(CRawPointCloudConfig::getInstance()->getNumPointCloudTile(), TileSet.size());  //确保TileSet中没有相同的(TileX, TileY)
}
