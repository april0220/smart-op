//
// pch.h
//

#pragma once

#include "gtest/gtest.h"

#include "common/ConfigInterface.h"
#include "SmartOPCommon.h"
#include "OverallConfig.h"
#include "ProgramRunningParametersConfig.h"
#include "UserDefinedCategoryMapConfig.h"

#include "common/Product.h"
#include "common/Singleton.h"
#include "common/CommonMicro.h"
#include "common/CommonInterface.h"
#include "common/EventLoggerInterface.h"
#include "common/DesignPatternInterface.h"
#include "common/HiveConfig.h"
#include "common/ConfigInterface.h"
#include "common/MathInterface.h"
#include "common/UtilityInterface.h"
