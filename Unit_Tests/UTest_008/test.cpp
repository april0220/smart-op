#include "pch.h"
#include "AsyncFileIO.h"
#include "common/DesignPatternInterface.h"
#include "common/HiveThread.h"
#include "common/MathInterface.h"

using namespace hiveObliquePhotography;

class TEST_AsyncFileIO : public testing::Test
{
protected:
	void SetUp() override
	{
		m_AsyncFileIO = hiveDesignPattern::hiveGetOrCreateProduct<Common::CAsyncFileIO>("AsyncFileIO");
	}

	void TearDown() override
	{

	}

	Common::CAsyncFileIO* m_AsyncFileIO;
};

TEST_F(TEST_AsyncFileIO, LoadFile)
{
	hiveSystem::CHiveThread t;
	t.startThread(&Common::CAsyncFileIO::run, m_AsyncFileIO);
	std::function<bool()> LoadFunc = [] 
	{
		int RandomNum = hiveMath::hiveGenerateRandomInteger<int>(1, 100);
		_SLEEP(RandomNum);
		return true; 
	};

	int FuncCount = 0;
	std::function<int* ()> LoadIntFunc = [&FuncCount]
	{
		int RandomNum = hiveMath::hiveGenerateRandomInteger<int>(1, 100);
		_SLEEP(RandomNum);
		int* IntList = new int[10]();
		IntList[0] = ++FuncCount;
		return IntList;
	};

	for (int i = 0; i < 20; i ++)
	{
		m_AsyncFileIO->issueFileLoadingTask(LoadFunc);
		m_AsyncFileIO->issueFileLoadingTask(LoadIntFunc);
	}
	m_AsyncFileIO->waitUntilTaskIsDone(20);
	int* Result = m_AsyncFileIO->getTaskResult<int>(20);
	EXPECT_EQ(Result[0], 10);
	m_AsyncFileIO->waitUntilTaskIsDone(40);
	m_AsyncFileIO->requestInterruptTask();
	t.join();
}