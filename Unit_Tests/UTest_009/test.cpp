#include "pch.h"
#include "RoadMidlineExtracter.h"
#include <json/json.h>

#define STB_IMAGE_STATIC
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

using namespace hiveObliquePhotography;
using namespace std;

const std::string SaveImagesDir = TESTDATA_DIR + std::string("UTest_009_Data/");

//SCU
const std::string SCURoadBinaryMapPath = TESTDATA_DIR + std::string("UTest_009_Data/BinaryMap/close_k8_1_connect.png");
const std::string SCUSubSkeletonPath = TESTDATA_DIR + std::string("UTest_009_Data/SkeletonMap/Skeleton_sub/RoadSkeletonMap_1.png");
const std::string SCUResultPath = TESTDATA_DIR + std::string("UTest_009_Data/SCU_Result.png");
float WidthInMeter_SCU = 475.0;
float HeightInMeter_SCU = 475.0;

//Sensat
const std::string ProcessedBinaryMapPath = TESTDATA_DIR + std::string("UTest_009_Data/BinaryMap/afterprocess.png");
const std::string SensatBinaryMapPath = TESTDATA_DIR + std::string("UTest_009_Data/BinaryMap/Mask21_k8_connect.png");//closed_512_connect
const std::string Cambridge_20_BinaryMapPath = TESTDATA_DIR + std::string("UTest_009_Data/BinaryMap/closed_512_connect.png");//closed_512_connect
const std::string SensatResultPath = TESTDATA_DIR + std::string("UTest_009_Data/Sensat_Result.png");
const std::string MidlineMapPath = TESTDATA_DIR + std::string("UTest_009_Data/broken_lines.png");
float WidthInMeter_sensat = 1000.0;
float HeightInMeter_sensat = 1000.0;

//ArcGIS
//const std::string WholeBinaryMapPath = TESTDATA_DIR + std::string("UTest_009_Data/BinaryMap/Binarymap_cambridge.png");
//const std::string ArcgisBinaryMapPath = TESTDATA_DIR + std::string("UTest_009_Data/BinaryMap/Arcgis_cambridge.png");

//const std::string WholeBinaryMapPath = TESTDATA_DIR + std::string("UTest_009_Data/BinaryMap/close_k8_1_connect.png");
const std::string WholeBinaryMapPath = TESTDATA_DIR + std::string("UTest_009_Data/BinaryMap/Binarymap_cambridge_whole_8x8.png");
const std::string ArcgisBinaryMapPath = TESTDATA_DIR + std::string("UTest_009_Data/SkeletonMap/Result_cambridge_whole_delete.png");

//ArcMap
const std::string Curves_arcmap = TESTDATA_DIR + std::string("UTest_009_Data/curves_Arcmap.json");
const std::string Curves_arcmap_connected = TESTDATA_DIR + std::string("UTest_009_Data/curves_Arcmap_connected.json");

class TEST_GenerateRoadMidline : public testing::Test
{
protected:
	void SetUp() override
	{
		m_pDetailedCategoryMap = hiveDesignPattern::hiveGetOrCreateProduct<CRoadMidlineExtracter>(MetafileKeywords::DETAILED_CATEGORY_MAP_SIG);
		__initTestMap();
	}

	void __initTestMap()
	{
		ASSERT_NE(m_pDetailedCategoryMap, nullptr);
		m_pDetailedCategoryMap->initParameters(WidthInMeter_sensat, HeightInMeter_sensat);
	}
	
	CRoadMidlineExtracter* m_pDetailedCategoryMap = nullptr;
};

std::vector<bool> loadBinaryMap(const std::string vFileName, int& voWidth, int& voHeight)
{
	const char* filepath = vFileName.c_str();
	int BytesPerPixel;
	unsigned char* ImageData = stbi_load(filepath, &voWidth, &voHeight, &BytesPerPixel, 0);

	std::vector<bool> BinaryMap;
	for (int i = 0; i < voWidth; i++)
		for (int k = 0; k < voHeight; k++)
		{
			if (ImageData[(i * voWidth + k) * BytesPerPixel] == 255)
				BinaryMap.push_back(true);
			else
				BinaryMap.push_back(false);
		}
	return BinaryMap;
}

bool saveBinaryMap(const std::string& vImageName, const std::vector<bool> vBinaryMap, int vWidth, int vHeight)
{
	if (vBinaryMap.empty())
	{
		_HIVE_OUTPUT_WARNING("Fail to execute [saveBinaryMap()].");
		return false;
	}
	cv::Mat Image(vWidth, vHeight, CV_8UC1);
	std::uint8_t* pPixel = Image.data;
	for (int i = 0; i < vBinaryMap.size(); i++) {
		pPixel[i] = vBinaryMap[i] * 255;
	}

	cv::imwrite(SaveImagesDir + "SkeletonMap/" + vImageName, Image);

	return true;
}

bool saveColorMap(const std::string& vImageName, const std::vector<bool> vSkeletonMap, int vWidth, int vHeight, std::vector<uint32_t> vMarks = {})
{
	if (vSkeletonMap.empty())
	{
		_HIVE_OUTPUT_WARNING("Fail to execute [saveBinaryMap()].");
		return false;
	}
	cv::Mat Image(cv::Mat::zeros(vWidth, vHeight, CV_8UC3));
	cv::Scalar red = cv::Scalar(0, 0, 255);
	cv::Scalar yellow = cv::Scalar(255, 255, 0);

	std::uint8_t* pPixel = Image.data;
	for (int i = 0; i < vSkeletonMap.size(); i++) {
		pPixel[i * 3 + 0] = vSkeletonMap[i] * 255;
		pPixel[i * 3 + 1] = vSkeletonMap[i] * 255;
		pPixel[i * 3 + 2] = vSkeletonMap[i] * 255;
	}

	int ShiftSize = 3;
	for (uint32_t point : vMarks) {
		cv::rectangle(Image, cv::Rect(cv::Point(point % vWidth - ShiftSize, point / vWidth - ShiftSize), cv::Point(point % vWidth + ShiftSize, point / vWidth + ShiftSize)), red, 3);
	}

	cv::imwrite(SaveImagesDir + "SkeletonMap/" + vImageName, Image);

	return true;
}

//0-line, 1-point, 2-intersection, 3-brokenline
bool saveRoads2Image(const std::vector<SRoad>& vRoads, int vWidth, int vHeight, int vType = 0, std::vector<uint32_t> vIntersections = {}) { 
	//注意opencv中，row == height == Point.y
	//cv::Mat outputImg(cv::Mat::zeros(vWidth, vHeight, CV_8UC3));
	cv::Mat outputImg(vWidth, vHeight, CV_8UC4, cv::Scalar(0, 0, 0, 0));
	cv::Scalar white = cv::Scalar(255, 255, 255, 255);
	cv::Scalar red = cv::Scalar(0, 0, 255, 255);
	cv::Scalar black = cv::Scalar(0, 0, 0, 255);
	int ShiftSize = 1;
	if (vType == 0) {
		bool isShowEndpoint = 0;
		for (int i = 0; i < vRoads.size(); i++) {
			auto Points = vRoads[i].KeyPointArray;
			for (int k = 0; k < Points.size() - 1; k++) {
				cv::line(outputImg, cv::Point(Points[k] % vWidth, Points[k] / vWidth), cv::Point(Points[k + 1] % vWidth, Points[k + 1] / vWidth), red, 2, cv::LINE_8);
				//cv::circle(outputImg, cv::Point(Points[k] % vWidth, Points[k] / vWidth), 2, red);
			}
			//cv::circle(outputImg, cv::Point(Points[Points.size() - 1] % vWidth, Points[Points.size() - 1] / vWidth), 2, red);
			if (isShowEndpoint) {
				cv::circle(outputImg, cv::Point(Points[0] % vWidth, Points[0] / vWidth), 2, red);
				int t = Points.size() - 1;
				cv::circle(outputImg, cv::Point(Points[t] % vWidth, Points[t] / vWidth), 2, red);
			}
		}
		cv::imwrite(SaveImagesDir + "lines.png", outputImg);
	}
	else if (vType == 1) {
		for (int i = 0; i < vRoads.size(); i++) {
			auto Points = vRoads[i].KeyPointArray;
			for (int k = 0; k < Points.size(); k++) {
				auto point = Points[k];
				cv::rectangle(outputImg, cv::Rect(cv::Point(point % vWidth - 1, point / vWidth - 1), cv::Point(point % vWidth + 1, point / vWidth + 1)), white);
			}
		}
		cv::imwrite(SaveImagesDir + "points.png", outputImg);
	}
	else if (vType == 2) {
		int ShiftSize = 1;
		for (uint32_t point : vIntersections) {
			cv::rectangle(outputImg, cv::Rect(cv::Point(point % vWidth - ShiftSize, point / vWidth - ShiftSize), cv::Point(point % vWidth + ShiftSize, point / vWidth + ShiftSize)), red);
		}
		cv::imwrite(SaveImagesDir + "intersections.png", outputImg);
	}
	else {
		for (int i = 0; i < vRoads.size(); i++) {
			auto Points = vRoads[i].KeyPointArray;
			for (int k = 0; k < Points.size() - 1; k++) {
				cv::line(outputImg, cv::Point(Points[k] % vWidth, Points[k] / vWidth), cv::Point(Points[k + 1] % vWidth, Points[k + 1] / vWidth), white, 1, cv::LINE_8);
				//cv::circle(outputImg, cv::Point(Points[k] % vWidth, Points[k] / vWidth), 2, red);
			}
			//cv::circle(outputImg, cv::Point(Points[Points.size() - 1] % vWidth, Points[Points.size() - 1] / vWidth), 2, red);
		}
		cv::imwrite(SaveImagesDir + "broken_lines.png", outputImg);
	}

	return true;
}

void saveRoads2Json(const std::vector<SRoad>& vRoads,int vWidth,int vHeight, bool isSameWidth=false)
{
	Json::Value root;

	for (int i = 0; i < vRoads.size(); ++i)
	{
		Json::Value points;
		auto Points = vRoads[i].KeyPointArray;
		for (int k = 0; k < Points.size(); ++k)
		{
			int pixel_ffset = Points[k];
			int offset_x = pixel_ffset % vWidth;
			int offset_y = pixel_ffset / vHeight;
			Json::Value point;
			//point.append(WidthInMeter_sensat * offset_x / vWidth);
			point.append(double(offset_x));
			point.append(0.0);//Houdini是y轴朝上
			//point.append(HeightInMeter_sensat * offset_y / vHeight);
			point.append(double(offset_y));
			points.append(point);
		}
		Json::Value curve;
		curve["points"] = points;
		curve["width"] = vRoads[i].Width;
		root["curves"].append(curve);
	}
  
	ofstream os;
	os.open(SaveImagesDir + "curves2.json", std::ios::out);
	if (!os.is_open())
		cout << "error：can not find or create the file which named \" demo.json\"." << endl;
	Json::StyledWriter writer;
	os << writer.write(root);
	os.close();
}

std::vector<SRoad> loadJson2Roads(int vWidth, string vPath)
{
	std::vector<SRoad> Roads;

	std::ifstream is(vPath);
	if (!is.is_open()) {
		std::cout << "error: unable to open the JSON file." << std::endl;
		return Roads;
	}

	Json::Value root;
	Json::CharReaderBuilder reader;
	std::string errors;
	if (!Json::parseFromStream(reader, is, &root, &errors)) {
		std::cout << "error: failed to parse the JSON file." << std::endl;
		return Roads;
	}

	for (const auto& curve : root["curves"]) {
		Json::Value points = curve["points"];
		SRoad Road;
		for (int i = 0; i < points.size(); i++) {
			double y = points[i][0].asDouble();
			double x = points[i][2].asDouble();
			int coord = static_cast<int>(x) * vWidth + static_cast<int>(y);
			Road.KeyPointArray.push_back(coord);
			if (i == 0) Road.IntersectionIndexA = coord;
			if (i == points.size() - 1) Road.IntersectionIndexB = coord;
		}
		double width = curve["width"].asDouble();
		Roads.push_back(Road);
	}

	is.close();
	return Roads;
}

//TEST_F(TEST_GenerateRoadMidline, NT_ArcMap)
//{
//	int MaxDeadendLength = 15;
//
//	int Width, Height;
//	std::vector<bool> WholeBinaryMap = loadBinaryMap(ProcessedBinaryMapPath, Width, Height);
//	m_pDetailedCategoryMap->setMapDim(make_pair(Width, Height));
//	std::vector<SRoad> Roads = loadJson2Roads(Width, Curves_arcmap_connected);//Curves_arcmap_connected  Curves_arcmap
//
//	m_pDetailedCategoryMap->findIntersection2Roads_arcmap(Roads);
//	m_pDetailedCategoryMap->mergeSubRoads(Roads);
//	//m_pDetailedCategoryMap->deleteDeadendRoads(Roads, MaxDeadendLength);
//
//	m_pDetailedCategoryMap->estimateRoadWidth(WholeBinaryMap, Roads, 0.1);
//
//	saveRoads2Json(Roads, Width, Height, true);
//	saveRoads2Image(Roads, Width, Height, 0);
//}

TEST_F(TEST_GenerateRoadMidline, NT_Whole)
{
	int DeleteWindowSize = 3;
	double SimplifyRatio = 15;
	int MaxDeadendLength = 15;
	int ExtendStep = 10;

	//1.生成骨架图
	int Width, Height;
	std::vector<bool> WholeBinaryMap = loadBinaryMap(Cambridge_20_BinaryMapPath, Width, Height);
	//std::vector<bool> ArcgisBinaryMap = loadBinaryMap(ArcgisBinaryMapPath, Width, Height);//SkeletonMap和BinaryMap的宽高必须一致
	m_pDetailedCategoryMap->setMapDim(make_pair(Width, Height));
	std::vector<bool> SkeletonMap = m_pDetailedCategoryMap->generateSkeletonMap(WholeBinaryMap);
	saveBinaryMap("RoadSkeletonMap.png", SkeletonMap, Width, Height);

	//2.定位路口点，分割出子路像素，折线拟合
	m_pDetailedCategoryMap->findIntersections(SkeletonMap);
	std::vector<SRoad> Roads = m_pDetailedCategoryMap->divideRoadSkeletonByIntersection(SkeletonMap, DeleteWindowSize);
	for (int i = 0; i < Roads.size(); i++)
		m_pDetailedCategoryMap->simplifyPixelArray(Roads[i], SimplifyRatio);

	//3.路宽估算
	m_pDetailedCategoryMap->estimateRoadWidth(WholeBinaryMap, Roads, WidthInMeter_sensat);

	//4.合并路口，删除较短的断头路，合并子路
	m_pDetailedCategoryMap->findIntersection2Roads(SkeletonMap, Roads, MaxDeadendLength);
	m_pDetailedCategoryMap->mergeSubRoads(Roads);

	////5.延长子路
	//saveRoads2Image(Roads, Width, Height, 3);
	//std::vector<bool> MidlineMap = loadBinaryMap(MidlineMapPath, Width, Height);
	//m_pDetailedCategoryMap->extendBrokenRoads(MidlineMap, Roads, ExtendStep);
	//m_pDetailedCategoryMap->addIntersection2Roads(Roads);

	////6.折线拟合
	//for (int i = 0; i < Roads.size(); i++)
	//	m_pDetailedCategoryMap->simplifyKeyPointArray(Roads[i], SimplifyRatio);

	//7.保存
	auto IntersectionSet = m_pDetailedCategoryMap->getIntersectionSet();//路口点
	std::vector<std::uint32_t> Intersections;
	for (auto p : IntersectionSet) {
		Intersections.push_back(get<0>(p) * Width + get<1>(p));
	}

	saveRoads2Json(Roads, Width, Height, true);
	saveRoads2Image(Roads, Width, Height, 0);
	saveRoads2Image(Roads, Width, Height, 1);
	saveRoads2Image(Roads, Width, Height, 2, Intersections);

	//saveColorMap("RoadSkeletonWithIntersentions.png", SkeletonMap, Width, Height, Intersections);
}