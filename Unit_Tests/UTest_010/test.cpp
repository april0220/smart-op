#include "pch.h"
#include "RoadRegionHandler.h"

using namespace hiveObliquePhotography;

class TEST_RoadDenoising : public testing::Test
{
protected:
	void SetUp() override
	{
		m_RoadHandler = new CRoadRegionHandler();
	}

	void TearDown() override
	{
		delete m_RoadHandler;
	}

	CRoadRegionHandler* m_RoadHandler = nullptr;
};

TEST_F(TEST_RoadDenoising, NT_MarkRoadGround)
{
	EXPECT_EQ(1, 1);
}
