#include "pch.h"

using namespace hiveObliquePhotography;

//测试用例列表：
//  * DT_InvalidInput: 错误的输入参数，期待返回false
//  * NT_Init：正确的输入参数，保证 1.MultiLayeredPointCloud、Block成员合法性；2.MultiLayeredPointCloud、Block部分成员计算正确性

class TEST_InitMultiLayeredPointCloud : public testing::Test
{
protected:
	float CellScale = 0.25f;
	float WorldScaleX = 1.0f;
	float WorldScaleY = 1.0f;
	float ExpectedBlockSize = 100.0f;
	float OverallSceneSize = 2000.0f;
	std::pair<float, float> LowerLeftTileMinCoordinate{ 0.0f, 0.0f };
};

TEST_F(TEST_InitMultiLayeredPointCloud, DT_InvalidInput)
{
	CMultiLayeredPointCloud PointCloud;
	float LongitudeSpan, LatitudeSpan;
	
	// Invalid Input
	// Invalid LongitudeSpan LatitudeSpan
	{
		LongitudeSpan = 0.0f;   //FIXME：如果你把多个错误输入放在一起，即使init()函数返回false了，你能保证init()能处理单个的错误输入？
		LatitudeSpan = 100.0f;
		std::pair<std::uint32_t, std::uint32_t> TileDim{ 2, 2 };
		std::vector<std::pair<float, float>> TileSpanSet{ {100, 50}, {100, 50},{100, 50}, {100, 50} };
		EXPECT_FALSE(PointCloud.init(LongitudeSpan, LatitudeSpan, CellScale, WorldScaleX, WorldScaleY, ExpectedBlockSize, OverallSceneSize, TileDim, TileSpanSet, LowerLeftTileMinCoordinate));
		const CMultiLayeredPointCloud::SOverallInfo& OverallInfo = PointCloud.getOverallInfo();
		EXPECT_FALSE(OverallInfo.isValid());
		EXPECT_FALSE(PointCloud.isAllBlockInitCorrectly());
	}
	{
		LongitudeSpan = 200.0f;   
		LatitudeSpan = 0.0f;
		std::pair<std::uint32_t, std::uint32_t> TileDim{ 2, 2 };
		std::vector<std::pair<float, float>> TileSpanSet{ {100, 50}, {100, 50},{100, 50}, {100, 50} };
		EXPECT_FALSE(PointCloud.init(LongitudeSpan, LatitudeSpan, CellScale, WorldScaleX, WorldScaleY, ExpectedBlockSize, OverallSceneSize, TileDim, TileSpanSet, LowerLeftTileMinCoordinate));
		const CMultiLayeredPointCloud::SOverallInfo& OverallInfo = PointCloud.getOverallInfo();
		EXPECT_FALSE(OverallInfo.isValid());
		EXPECT_FALSE(PointCloud.isAllBlockInitCorrectly());
	}
	// Invalid TileDim
	{
		LongitudeSpan = 200.0f;
		LatitudeSpan = 100.0f;
		std::pair<std::uint32_t, std::uint32_t> TileDim{ 1, 1 };
		std::vector<std::pair<float, float>> TileSpanSet{ {100, 50}, {100, 50},{100, 50}, {100, 50} };
		EXPECT_FALSE(PointCloud.init(LongitudeSpan, LatitudeSpan, CellScale, WorldScaleX, WorldScaleY, ExpectedBlockSize, OverallSceneSize, TileDim, TileSpanSet, LowerLeftTileMinCoordinate));
		const CMultiLayeredPointCloud::SOverallInfo& OverallInfo = PointCloud.getOverallInfo();
		EXPECT_FALSE(OverallInfo.isValid());
		EXPECT_FALSE(PointCloud.isAllBlockInitCorrectly());
	}
	// Invalid TileSpan
	{
		LongitudeSpan = 200.0f;
		LatitudeSpan = 100.0f;
		std::pair<std::uint32_t, std::uint32_t> TileDim{ 1, 1 };
		std::vector<std::pair<float, float>> TileSpanSet{ {0,0} };
		EXPECT_FALSE(PointCloud.init(LongitudeSpan, LatitudeSpan, CellScale, WorldScaleX, WorldScaleY, ExpectedBlockSize, OverallSceneSize, TileDim, TileSpanSet, LowerLeftTileMinCoordinate));
		const CMultiLayeredPointCloud::SOverallInfo& OverallInfo = PointCloud.getOverallInfo();
		EXPECT_FALSE(OverallInfo.isValid());
		EXPECT_FALSE(PointCloud.isAllBlockInitCorrectly());
	}
	// Invalid ExpectedBlockSize
	{
		LongitudeSpan = 200.0f;
		LatitudeSpan = 100.0f;
		ExpectedBlockSize = 0;
		std::pair<std::uint32_t, std::uint32_t> TileDim{ 2, 2 };
		std::vector<std::pair<float, float>> TileSpanSet{ {100, 50}, {100, 50},{100, 50}, {100, 50} };
		EXPECT_FALSE(PointCloud.init(LongitudeSpan, LatitudeSpan, CellScale, WorldScaleX, WorldScaleY, ExpectedBlockSize, OverallSceneSize, TileDim, TileSpanSet, LowerLeftTileMinCoordinate));
		const CMultiLayeredPointCloud::SOverallInfo& OverallInfo = PointCloud.getOverallInfo();
		EXPECT_FALSE(OverallInfo.isValid());
		EXPECT_FALSE(PointCloud.isAllBlockInitCorrectly());
	}
	// Invalid OverallSceneSize
	{
		{
			LongitudeSpan = 200.0f;
			LatitudeSpan = 100.0f;
			OverallSceneSize = 0;
			std::pair<std::uint32_t, std::uint32_t> TileDim{ 2, 2 };
			std::vector<std::pair<float, float>> TileSpanSet{ {100, 50}, {100, 50},{100, 50}, {100, 50} };
			EXPECT_FALSE(PointCloud.init(LongitudeSpan, LatitudeSpan, CellScale, WorldScaleX, WorldScaleY, ExpectedBlockSize, OverallSceneSize, TileDim, TileSpanSet, LowerLeftTileMinCoordinate));
			const CMultiLayeredPointCloud::SOverallInfo& OverallInfo = PointCloud.getOverallInfo();
			EXPECT_FALSE(OverallInfo.isValid());
			EXPECT_FALSE(PointCloud.isAllBlockInitCorrectly());
		}
		// OverallSceneSize < ExpectedBlockSize can also init correctly
		{
			LongitudeSpan = 200.0f;
			LatitudeSpan = 100.0f;
			ExpectedBlockSize = 100;
			OverallSceneSize = 50;
			std::pair<std::uint32_t, std::uint32_t> TileDim{ 2, 2 };
			std::vector<std::pair<float, float>> TileSpanSet{ {100, 50}, {100, 50},{100, 50}, {100, 50} };
			EXPECT_TRUE(PointCloud.init(LongitudeSpan, LatitudeSpan, CellScale, WorldScaleX, WorldScaleY, ExpectedBlockSize, OverallSceneSize, TileDim, TileSpanSet, LowerLeftTileMinCoordinate));
			const CMultiLayeredPointCloud::SOverallInfo& OverallInfo = PointCloud.getOverallInfo();
			EXPECT_TRUE(OverallInfo.isValid());
			EXPECT_TRUE(PointCloud.isAllBlockInitCorrectly());
		}
	}
}

TEST_F(TEST_InitMultiLayeredPointCloud, NT_Init)
{
	CMultiLayeredPointCloud PointCloud;
	float LongitudeSpan, LatitudeSpan;
	
	{
		LongitudeSpan = 200.0f;
		LatitudeSpan = 100.0f;
		std::pair<std::uint32_t, std::uint32_t> TileDim{ 2, 2 };
		std::vector<std::pair<float, float>> TileSpanSet{ {100, 50}, {100, 50},{100, 50}, {100, 50} };
		EXPECT_TRUE(PointCloud.init(LongitudeSpan, LatitudeSpan, CellScale, WorldScaleX, WorldScaleY, ExpectedBlockSize, OverallSceneSize, TileDim, TileSpanSet, LowerLeftTileMinCoordinate));
		const CMultiLayeredPointCloud::SOverallInfo& OverallInfo = PointCloud.getOverallInfo();
		EXPECT_TRUE(OverallInfo.isValid());
		EXPECT_TRUE(PointCloud.isAllBlockInitCorrectly());
		// All important member are correct
		EXPECT_EQ(OverallInfo._BlockDimX, 8);
		EXPECT_EQ(OverallInfo._BlockDimY, 4);
		EXPECT_EQ(OverallInfo._CellDimY, 404);
		EXPECT_EQ(OverallInfo._BlockScale, 25.25f);
		EXPECT_EQ(OverallInfo._OriginX, -2);
		EXPECT_EQ(OverallInfo._OriginY,-1);
		EXPECT_EQ(PointCloud.getNumBlocks(),32);
		auto Block = PointCloud.getBlock({4,2});
		EXPECT_EQ(Block.getNumTileContributer(), 4);
		Block = PointCloud.getBlock({ 0,0 });
		EXPECT_EQ(Block.getNumTileContributer(), 1);
	}
}