#include "pch.h"
#include "PCLPointCloudWrapper.h"

using namespace hiveObliquePhotography;

class TEST_Wrapper : public testing::Test
{
protected:
	void SetUp() override
	{
	}

	void TearDown() override
	{
	}

	pcl::PointCloud<pcl::PointXYZRGBNormal>* loadPCLPointCloud(const std::string& vFileName)
	{
		pcl::PointCloud<pcl::PointXYZRGBNormal>* pPointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
		pcl::io::loadPLYFile<pcl::PointCloud<pcl::PointXYZRGBNormal>::PointType>(vFileName, *pPointCloud);
		return pPointCloud;
	}

	const std::string m_PLYFileLoaderSig = MetafileKeywords::PLY_LOADER_SIG;
	const std::string m_PCDFileLoaderSig = MetafileKeywords::PCD_LOADER_SIG;

	const std::string m_PLYPointXYZRGBNormalFileName = TESTDATA_DIR + std::string("UTest_002_Data/Tile_+377_-10903.ply");
	const std::string m_PLYPointXYZRGBFileName = TESTDATA_DIR + std::string("UTest_002_Data/Tile_+377_-10903_WithoutNormal.ply");
	const std::string m_PCDFileName = TESTDATA_DIR + std::string("UTest_002_Data/slice15-Cloud.pcd");
};

//测试点：PCLPointCloud的size为空
TEST_F(TEST_Wrapper, DT_NullPCLPointCloud)
{
	pcl::PointCloud<pcl::PointXYZRGBNormal>* pPCLPointCloud = new pcl::PointCloud<pcl::PointXYZRGBNormal>;
	SUnorganizedPointCloud* pUnorganizedPointCloud = new SUnorganizedPointCloud;
	CPCLPointCloudWrapper Wrapper;
	EXPECT_ANY_THROW(Wrapper.dumpData2UnorgnizedPointCloud(pPCLPointCloud, pUnorganizedPointCloud));
	delete pPCLPointCloud;
	delete pUnorganizedPointCloud;
}

//测试点：SUnorganizedPointCloud为空指针
TEST_F(TEST_Wrapper, DT_Null)
{
	pcl::PointCloud<pcl::PointXYZRGBNormal>* pPCLPointCloud = loadPCLPointCloud(m_PLYPointXYZRGBNormalFileName);
	CPCLPointCloudWrapper Wrapper;
	EXPECT_EQ(Wrapper.dumpData2UnorgnizedPointCloud(pPCLPointCloud, nullptr), false);
	delete pPCLPointCloud;
}

//测试点：传入空UnorganizedPointCloud，以及正常的PointXYZRGBNormal类型的pcl点云
TEST_F(TEST_Wrapper, NT_EmptyUnorganizedPointCloud_PCLPointXYZRGBNormal)
{
	pcl::PointCloud<pcl::PointXYZRGBNormal>* pPCLPointCloud = loadPCLPointCloud(m_PLYPointXYZRGBNormalFileName);
	SUnorganizedPointCloud* pUnorganizedPointCloud = new SUnorganizedPointCloud;
	CPCLPointCloudWrapper Wrapper;
	EXPECT_TRUE(Wrapper.dumpData2UnorgnizedPointCloud(pPCLPointCloud, pUnorganizedPointCloud));

	bool IsNormal = false;
	bool IsRGB = false;
	for (int i = 0; i < pPCLPointCloud->points.size(); i++)
	{
		auto& PointCloud = pPCLPointCloud->points[i];
		if (Eigen::Vector3f(PointCloud.normal_x, PointCloud.normal_y, PointCloud.normal_z) != Eigen::Vector3f(0.0, 0.0, 0.0) && IsNormal == false)
			IsNormal = true;
		auto PointCloudRGB = PointCloud.getRGBVector3i();
		if (PointCloud.getRGBVector3i() != Eigen::Vector3i(0, 0, 0) && IsRGB == false)
			IsRGB = true;
		if (IsNormal | IsRGB)
			break;
	}
	ASSERT_EQ(IsNormal, true);
	ASSERT_EQ(IsRGB, true);

	for (int i = 0; i < pUnorganizedPointCloud->_PositionSet.size(); i++)
	{
		auto& PointCloud = pPCLPointCloud->points[i];
		EXPECT_EQ(PointCloud.getArray3fMap().matrix(), pUnorganizedPointCloud->_PositionSet[i]);
		EXPECT_EQ(PointCloud.getNormalVector3fMap(), pUnorganizedPointCloud->_NormalSet[i]);
		EXPECT_EQ(PointCloud.getRGBVector3i().x(), pUnorganizedPointCloud->_ColorSet[i]._r);
		EXPECT_EQ(PointCloud.getRGBVector3i().y(), pUnorganizedPointCloud->_ColorSet[i]._g);
		EXPECT_EQ(PointCloud.getRGBVector3i().z(), pUnorganizedPointCloud->_ColorSet[i]._b);
	}

	delete pPCLPointCloud;
	delete pUnorganizedPointCloud;
}

//测试点：传入空UnorganizedPointCloud，以及正常的PointXYZRGB类型的pcl点云
TEST_F(TEST_Wrapper, NT_EmptyUnorganizedPointCloud_PCLPointXYZRGB)
{
	pcl::PointCloud<pcl::PointXYZRGBNormal>* pPCLPointCloud = loadPCLPointCloud(m_PLYPointXYZRGBFileName);
	SUnorganizedPointCloud* pUnorganizedPointCloud = new SUnorganizedPointCloud;
	CPCLPointCloudWrapper Wrapper;
	EXPECT_TRUE(Wrapper.dumpData2UnorgnizedPointCloud(pPCLPointCloud, pUnorganizedPointCloud));

	bool IsNormal = false;
	bool IsRGB = false;
	for (int i = 0; i < pPCLPointCloud->points.size(); i++)
	{
		auto& PointCloud = pPCLPointCloud->points[i];
		if (Eigen::Vector3f(PointCloud.normal_x, PointCloud.normal_y, PointCloud.normal_z) != Eigen::Vector3f(0.0, 0.0, 0.0) && IsNormal == false)
			IsNormal = true;
		auto PointCloudRGB = PointCloud.getRGBVector3i();
		if (PointCloud.getRGBVector3i() != Eigen::Vector3i(0, 0, 0) && IsRGB == false)
			IsRGB = true;
		if (IsNormal | IsRGB)
			break;
	}
	ASSERT_EQ(IsNormal, false);
	ASSERT_EQ(IsRGB, true);

	for (int i = 0; i < pUnorganizedPointCloud->_PositionSet.size(); i++)
	{
		auto& PointCloud = pPCLPointCloud->points[i];
		EXPECT_EQ(PointCloud.getArray3fMap().matrix(), pUnorganizedPointCloud->_PositionSet[i]);
		EXPECT_EQ(PointCloud.getRGBVector3i().x(), pUnorganizedPointCloud->_ColorSet[i]._r);
		EXPECT_EQ(PointCloud.getRGBVector3i().y(), pUnorganizedPointCloud->_ColorSet[i]._g);
		EXPECT_EQ(PointCloud.getRGBVector3i().z(), pUnorganizedPointCloud->_ColorSet[i]._b);
	}

	delete pPCLPointCloud;
	delete pUnorganizedPointCloud;
}

// TODO：更多的测试用例

// 测试extractDataFromUnorgnizedPointCloud()和getPointCloud()
//测试点：SUnorganizedPointCloud为空
TEST_F(TEST_Wrapper, DT_EmptyUnorganizedPointCloud)
{
	SUnorganizedPointCloud* pUnorganizedPointCloud = new SUnorganizedPointCloud;
	CPCLPointCloudWrapper Wrapper;

	EXPECT_FALSE(Wrapper.extractDataFromUnorgnizedPointCloud(3, pUnorganizedPointCloud));
	delete pUnorganizedPointCloud;
}
	
//测试点：PointTypeFlag值是否正确
TEST_F(TEST_Wrapper, DT_PointTypeFlag)
{
	pcl::PointCloud<pcl::PointXYZRGBNormal>* pPCLPointCloud = loadPCLPointCloud(m_PLYPointXYZRGBFileName);
	SUnorganizedPointCloud* pUnorganizedPointCloud = new SUnorganizedPointCloud;
	CPCLPointCloudWrapper Wrapper;
	EXPECT_TRUE(Wrapper.dumpData2UnorgnizedPointCloud(pPCLPointCloud, pUnorganizedPointCloud));
	EXPECT_TRUE(Wrapper.extractDataFromUnorgnizedPointCloud(1, pUnorganizedPointCloud));
	EXPECT_TRUE(Wrapper.extractDataFromUnorgnizedPointCloud(3, pUnorganizedPointCloud));
	EXPECT_TRUE(Wrapper.extractDataFromUnorgnizedPointCloud(7, pUnorganizedPointCloud));

	EXPECT_FALSE(Wrapper.extractDataFromUnorgnizedPointCloud(0, pUnorganizedPointCloud));
	EXPECT_FALSE(Wrapper.extractDataFromUnorgnizedPointCloud(2, pUnorganizedPointCloud));
	EXPECT_FALSE(Wrapper.extractDataFromUnorgnizedPointCloud(6, pUnorganizedPointCloud));
	delete pPCLPointCloud;
	delete pUnorganizedPointCloud;
}

//测试点：传入包含XYZRGB数据的UnorganizedPointCloud，转为PointXYZRGB类型的pcl点云
TEST_F(TEST_Wrapper, NT_UnorganizedPointCloudXYZRGB_PCLPointXYZRGB)
{
	pcl::PointCloud<pcl::PointXYZRGBNormal>* pPCLPointCloud = loadPCLPointCloud(m_PLYPointXYZRGBFileName);
	SUnorganizedPointCloud* pUnorganizedPointCloud = new SUnorganizedPointCloud;
	CPCLPointCloudWrapper Wrapper;
	EXPECT_TRUE(Wrapper.dumpData2UnorgnizedPointCloud(pPCLPointCloud, pUnorganizedPointCloud));

	EXPECT_TRUE(Wrapper.extractDataFromUnorgnizedPointCloud(3 , pUnorganizedPointCloud));
	auto pPointCloud = Wrapper.getPointCloud<pcl::PointXYZRGB>();

	for (int i = 0; i < pUnorganizedPointCloud->_PositionSet.size(); i++)
	{
		auto& PointCloud = pPointCloud->points[i];
		EXPECT_EQ(PointCloud.getArray3fMap().matrix(), pUnorganizedPointCloud->_PositionSet[i]);
		EXPECT_EQ(PointCloud.getRGBVector3i().x(), pUnorganizedPointCloud->_ColorSet[i]._r);
		EXPECT_EQ(PointCloud.getRGBVector3i().y(), pUnorganizedPointCloud->_ColorSet[i]._g);
		EXPECT_EQ(PointCloud.getRGBVector3i().z(), pUnorganizedPointCloud->_ColorSet[i]._b);
	}

	delete pPointCloud;
	delete pPCLPointCloud;
	delete pUnorganizedPointCloud;
}

//测试点：传入包含XYZRGBNormal数据的UnorganizedPointCloud，转为PointXYZRGBNormal类型的pcl点云
TEST_F(TEST_Wrapper, NT_UnorganizedPointCloudXYZRGBNormal_PCLPointXYZRGBNormal)
{
	pcl::PointCloud<pcl::PointXYZRGBNormal>* pPCLPointCloud = loadPCLPointCloud(m_PLYPointXYZRGBNormalFileName);
	SUnorganizedPointCloud* pUnorganizedPointCloud = new SUnorganizedPointCloud;
	CPCLPointCloudWrapper Wrapper;
	EXPECT_TRUE(Wrapper.dumpData2UnorgnizedPointCloud(pPCLPointCloud, pUnorganizedPointCloud));

	EXPECT_TRUE(Wrapper.extractDataFromUnorgnizedPointCloud(7, pUnorganizedPointCloud));
	auto pPointCloud = Wrapper.getPointCloud<pcl::PointXYZRGBNormal>();

	for (int i = 0; i < pUnorganizedPointCloud->_PositionSet.size(); i++)
	{
		auto& PointCloud = pPointCloud->points[i];
		EXPECT_EQ(PointCloud.getArray3fMap().matrix(), pUnorganizedPointCloud->_PositionSet[i]);
		EXPECT_EQ(PointCloud.getNormalVector3fMap(), pUnorganizedPointCloud->_NormalSet[i]);
		EXPECT_EQ(PointCloud.getRGBVector3i().x(), pUnorganizedPointCloud->_ColorSet[i]._r);
		EXPECT_EQ(PointCloud.getRGBVector3i().y(), pUnorganizedPointCloud->_ColorSet[i]._g);
		EXPECT_EQ(PointCloud.getRGBVector3i().z(), pUnorganizedPointCloud->_ColorSet[i]._b);
	}

	delete pPointCloud;
	delete pPCLPointCloud;
	delete pUnorganizedPointCloud;
}
