#include "pch.h"

#include "MultiLayeredPointCloud.h"
#include "PointCloudFileLoader.h"
#include "UserDefinedCategoryMap.h"
#include "ProgramRunningParametersConfig.h"

using namespace hiveObliquePhotography;

//测试用例列表：
//  * ComputeBlockIndex: 测试计算点在哪个Block中的正确性
//  * HandleEndlessLoop: 测试处理尝试加锁死循环的合理性
//  * AddPointCompletenessTest: 测试填充点数据的完整性
//  * UnloadCorrectly: 换出操作的正确性（内容清空）
//  * NotifyPointCloudConvertIsDone: 测试通知Block转换完成函数的正确性
//  * SortPointsCorrectly: 排序划分Cell的正确性

const std::string PLYFileName = TESTDATA_DIR + std::string("UTest_004_Data/TestPointCloud.ply");
const std::string RawPointCloudConfigFileName = TESTDATA_DIR + std::string("UTest_004_Data/RawPointCloudMetafile.xml");
const std::string ProgramRunningParametersConfigFileName = TESTDATA_DIR + std::string("UTest_004_Data/ProgramRunningParameters.xml");
const std::string BPDFilePath= SOLUTION_DIR + std::string("Output/");
const std::string PNGFileName = TESTDATA_DIR + std::string("UTest_004_Data/Test.png");

class TEST_AddPoints2PointBlock : public testing::Test
{
protected:
	void SetUp() override
	{
		m_MultiLayeredPointCloud = new CMultiLayeredPointCloud;
		m_MultiLayeredPointCloud->init(200, 100, 0.25, 1.0, 1.0,100, 2000, { 2,2 }, { {100, 50}, { 100, 50 }, { 100, 50 }, { 100, 50 } }, { 0.0f, 0.0f });
		IPointCloudFileLoader* pLoader = hiveDesignPattern::hiveGetOrCreateProduct<IPointCloudFileLoader>("ply");
		ASSERT_NE(pLoader, nullptr);
		m_pUnorganizedPointCloud = pLoader->load(PLYFileName);
		ASSERT_NE(m_pUnorganizedPointCloud, nullptr);
		EXPECT_TRUE(m_pUnorganizedPointCloud->isValid());
		
		ASSERT_EQ(hiveConfig::hiveParseConfig(RawPointCloudConfigFileName, hiveConfig::EConfigType::XML, CRawPointCloudConfig::getInstance()), hiveConfig::EParseResult::SUCCEED);
		ASSERT_EQ(hiveConfig::hiveParseConfig(ProgramRunningParametersConfigFileName, hiveConfig::EConfigType::XML, CProgramRunningParametersConfig::getInstance()), hiveConfig::EParseResult::SUCCEED);
	}

	void TearDown() override
	{
		delete m_MultiLayeredPointCloud;
		delete m_pUnorganizedPointCloud;
	}

	template<typename T>
	bool readBlock(const std::string& vFilePath, std::vector<T>& voElementSet)
	{
		std::ifstream ReadFile(vFilePath, std::ios::in | std::ios::binary);
		_HIVE_EARLY_RETURN(ReadFile.fail(), _FORMAT_STR1("Fail to open the specified file named %1%", vFilePath), false);

		auto extractNumFromString = [](const char* vString) {
			int Num = 0;
			_HIVE_EARLY_RETURN(vString == nullptr, "The specified string is Null!", Num);
			while (*vString != '\0')
			{
				_HIVE_SIMPLE_IF((*vString >= '0' && *vString <= '9'), (Num = (Num * 10 + *vString - '0')));
				vString++;
			}
			return Num;
		};

		char FileHeader[64];
		for (int HeaderLine = 0, HeaderEntryNum = 8; HeaderLine < HeaderEntryNum; HeaderLine++)
		{
			ReadFile.getline(FileHeader, sizeof(FileHeader), '\n');
			if (HeaderLine == 5) { int ElementNum = extractNumFromString(FileHeader); voElementSet.reserve(ElementNum); }
			if (HeaderLine == 6) { int PropertyNum = extractNumFromString(FileHeader); HeaderEntryNum += PropertyNum; }
		}

		for (T vElement; ReadFile.read(reinterpret_cast<char*>(&vElement), sizeof(T));)
			voElementSet.push_back(vElement);

		ReadFile.close();
		return true;
	}

	CMultiLayeredPointCloud* m_MultiLayeredPointCloud = nullptr;
	SUnorganizedPointCloud* m_pUnorganizedPointCloud = nullptr;
};

TEST_F(TEST_AddPoints2PointBlock, NT_ComputeBlockIndex)
{
	auto BlockIdx = m_MultiLayeredPointCloud->computeBlockIndex(m_pUnorganizedPointCloud->_PositionSet[0]);
	EXPECT_EQ(BlockIdx.first, 0);
	EXPECT_EQ(BlockIdx.second, 0);

	BlockIdx = m_MultiLayeredPointCloud->computeBlockIndex(m_pUnorganizedPointCloud->_PositionSet[2]);
	EXPECT_EQ(BlockIdx.first, 4);
	EXPECT_EQ(BlockIdx.second, 0);

	//Block公共边点
	BlockIdx = m_MultiLayeredPointCloud->computeBlockIndex(m_pUnorganizedPointCloud->_PositionSet[5]);
	EXPECT_EQ(BlockIdx.first, 5);
	EXPECT_EQ(BlockIdx.second, 2);
}

TEST_F(TEST_AddPoints2PointBlock, DT_HandleEndlessLoop)
{
	auto Block = m_MultiLayeredPointCloud->getBlock({4,0});
	Block.tryLockBlock4Writing();
	EXPECT_FALSE(m_MultiLayeredPointCloud->addPointSet(m_pUnorganizedPointCloud));
	Block.unlock4Writing();
}

TEST_F(TEST_AddPoints2PointBlock, NT_AddPointCompletenessTest)
{
	//expect all points add in block correctly;
	auto Flag = m_MultiLayeredPointCloud->addPointSet(m_pUnorganizedPointCloud);
	auto BlockA = m_MultiLayeredPointCloud->getBlock({ 0,0 });
	EXPECT_EQ(BlockA.getNumPoints(), 0);

	auto BlockB = m_MultiLayeredPointCloud->getBlock({ 4,0 });
	EXPECT_EQ(BlockB.getNumPoints(), 4);

	auto BlockC = m_MultiLayeredPointCloud->getBlock({ 4,2 });
	EXPECT_EQ(BlockC.getNumPoints(), 3);
}

TEST_F(TEST_AddPoints2PointBlock, NT_UnloadCorrectly)
{
	auto Flag = m_MultiLayeredPointCloud->addPointSet(m_pUnorganizedPointCloud);
	auto Block = m_MultiLayeredPointCloud->getBlock({ 0,0 });
	Block.unload();
	EXPECT_EQ(Block.getNumPoints(), 0);
	EXPECT_EQ(Block.getPointColorSet().size(), 0);
	EXPECT_NE(Block.getFirstPointIndexInEachCell().size(), 0);
}

TEST_F(TEST_AddPoints2PointBlock, NT_NotifyPointCloudConvertIsDone)
{

}

TEST_F(TEST_AddPoints2PointBlock, NT_SortPointsCorrectly)
{
	CPointCloudBlock Block;
	// Without normal
	Block.init(0, 0, 0.5, 2, 2, {0.0f,0.0f}, { 0 });
	Block.addPoint({ 0,0,0 }, {0,0,0}, { 255, 0, 0 },3); //1
	Block.addPoint({ 0.8f,0.8f,0 }, { 0,0,0 }, { 255,0,0 }, 3); //5
	Block.addPoint({ 0.7f,0.7f,1 }, { 0,0,0 }, { 255,255,0 }, 3); //3
	Block.addPoint({ 0.2f,0.7f,1 }, { 0,0,0 }, { 0,255,0 }, 3); //2
	Block.addPoint({ 0.1f,0.1f,0.6f }, { 0,0,0 }, { 255,255,0 }, 3); //0
	Block.addPoint({ 0.9f,0.6f,0.7f }, { 0,0,0 }, { 0,255,255 }, 3); //4
	Block.sort4DividingCell();
	// FirstPointIndexInEachCell is correct
	auto CellOffset = Block.getFirstPointIndexInEachCell();
	ASSERT_EQ(CellOffset.size(), 4);
	EXPECT_EQ(CellOffset[0], 0);
	EXPECT_EQ(CellOffset[1], 2);
	EXPECT_EQ(CellOffset[2], 2);
	EXPECT_EQ(CellOffset[3], 3);

	// point attribute set is correct
	auto ColorSet = Block.getPointColorSet();
	EXPECT_EQ(ColorSet[0]._r, 255); EXPECT_EQ(ColorSet[0]._g, 255); EXPECT_EQ(ColorSet[0]._b, 0);
	EXPECT_EQ(ColorSet[4]._r, 0); EXPECT_EQ(ColorSet[4]._g, 255); EXPECT_EQ(ColorSet[4]._b, 255);
}

TEST_F(TEST_AddPoints2PointBlock, NT_LoadCorrectly)
{
	//TODO: 1.  2. 
}

TEST_F(TEST_AddPoints2PointBlock, NT_SaveBlockFiles)
{
	CPointCloudBlock Block;
	Block.init(0, 0, 0.5, 2, 2, { 0.0f, 0.0f }, { 0 });
	Block.addPoint({ 0, 0, 0 }, { 0, 0, 0 }, { 255, 0, 0 }, 7);//1
	Block.addPoint({ 0.8f, 0.8f, 0 }, { 0, 0, 0 }, { 255, 0, 0 }, 7);//5
	Block.addPoint({ 0.7f, 0.7f, 1 }, { 0, 0, 0 }, { 255, 255, 0 }, 7);//3
	Block.addPoint({ 0.2f, 0.7f, 1 }, { 0, 0, 0 }, { 0, 255, 0 }, 7);//2
	Block.addPoint({ 0.1f, 0.1f, 0.6f }, { 0, 0, 0 }, { 255, 255, 0 }, 7);//0
	Block.addPoint({ 0.9f, 0.6f, 0.7f }, { 0, 0, 0 }, { 0, 255, 255 }, 7);//4
	Block.sort4DividingCell();
	Block.save();

	std::string BPDNoNeedFileName;
	std::string BPDNeedFileName;
	std::vector<std::string> FileType{ "position" ,"color" ,"normal" ,"category" };
	for (auto e : FileType)
	{
		BPDNoNeedFileName = BPDFilePath + _FORMAT_STR3("block_%1%_%2%_%3%_NoNeedProcessing.bpd", 0, 0, e);
		BPDNeedFileName = BPDFilePath + _FORMAT_STR3("block_%1%_%2%_%3%_NeedProcessing.bpd", 0, 0, e);
		EXPECT_NE(_access(BPDNoNeedFileName.c_str(), 0), -1); EXPECT_NE(_access(BPDNeedFileName.c_str(), 0), -1);
		if (e == "position")
		{
			std::vector<Eigen::Vector3f> NeedPositionElement;
			std::vector<Eigen::Vector3f> NoNeedPositionElement;
			readBlock(BPDNeedFileName, NeedPositionElement);
			readBlock(BPDNoNeedFileName, NoNeedPositionElement);
			EXPECT_EQ(NeedPositionElement[0].x(), 0.1f); EXPECT_EQ(NeedPositionElement[1].y(), 0); EXPECT_EQ(NeedPositionElement[4].z(), 0.7f);
			EXPECT_EQ(NoNeedPositionElement.empty(), true);
		}
		else if (e == "normal")
		{
			std::vector<Eigen::Vector3f> NeedNormalElement;
			std::vector<Eigen::Vector3f> NoNeedNormalElement;
			readBlock(BPDNeedFileName, NeedNormalElement);
			readBlock(BPDNoNeedFileName, NoNeedNormalElement);
			EXPECT_EQ(NeedNormalElement[0].x(), 0); EXPECT_EQ(NeedNormalElement[1].y(), 0); EXPECT_EQ(NeedNormalElement[4].z(), 0);
			EXPECT_EQ(NoNeedNormalElement.empty(), true);
		}
		else if (e == "color")
		{
			std::vector<SRGBColor> NeedColorElement;
			std::vector<SRGBColor> NoNeedColorElement;
			readBlock(BPDNeedFileName, NeedColorElement);
			readBlock(BPDNoNeedFileName, NoNeedColorElement);
			EXPECT_EQ(NeedColorElement[0]._r, 255); EXPECT_EQ(NeedColorElement[1]._g, 0); EXPECT_EQ(NeedColorElement[4]._b, 255);
			EXPECT_EQ(NoNeedColorElement.empty(), true);
		}
		else if (e == "category")
		{
			std::vector<std::uint16_t> NeedCategoryElement;
			std::vector<std::uint16_t> NoNeedCategoryElement;
			readBlock(BPDNeedFileName, NeedCategoryElement);
			readBlock(BPDNoNeedFileName, NoNeedCategoryElement);
			EXPECT_EQ(NeedCategoryElement.empty(), true);
			EXPECT_EQ(NoNeedCategoryElement.empty(),true);
		}
	}
}