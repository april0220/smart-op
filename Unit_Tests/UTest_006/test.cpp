#include "pch.h"

using namespace hiveObliquePhotography;

//测试用例列表：

//FIXME: 我不知道这里generate这个单词你想表达什么？类型图不是用户创建的吗，你只是载入而已，你哪里在generate了？
class TEST_LoadCategoryMap : public testing::Test
{
protected:
	void SetUp() override
	{
		m_Category2ColorMap.insert(std::pair<SRGBColor, EUserDefinedPointCatetory>({ 255,0,0 }, EUserDefinedPointCatetory::USER_DEFINED_ROAD));
		m_Category2ColorMap.insert(std::pair<SRGBColor, EUserDefinedPointCatetory>({ 0,255,0 }, EUserDefinedPointCatetory::USER_DEFINED_LANDSCAPE));
		m_Category2ColorMap.insert(std::pair<SRGBColor, EUserDefinedPointCatetory>({ 0,0,255 }, EUserDefinedPointCatetory::USER_DEFINED_RIVER));
	}

	void TearDown() override
	{
	}
	
	std::pair<float, float> m_OriginCoordinate{ 0,0 };
	float m_WorldScaleX = 1.0f;
	float m_WorldScaleY = 1.0f;
	const std::vector<std::string> m_CategoryMapFileNameSet = { TESTDATA_DIR + std::string("UTest_006_Data/Image1.jpg"),
																TESTDATA_DIR + std::string("UTest_006_Data/Image2.jpg"),
																TESTDATA_DIR + std::string("UTest_006_Data/Image3.jpg"),
																TESTDATA_DIR + std::string("UTest_006_Data/Image4.jpg") };

	std::vector<std::tuple<std::string, std::uint32_t, std::uint32_t>> m_CategoryMapSet{{m_CategoryMapFileNameSet[0],0,0},{m_CategoryMapFileNameSet[1],1,0}, {m_CategoryMapFileNameSet[2],0,1}, {m_CategoryMapFileNameSet[3],1,1} };
	std::map<SRGBColor, EUserDefinedPointCatetory> m_Category2ColorMap;

	const std::string m_PointCloudFileName = TESTDATA_DIR + std::string("UTest_006_Data/TestPointCloud.ply");
};

TEST_F(TEST_LoadCategoryMap, DT_InvalidXSpan)
{
	EXPECT_FALSE(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->load(0,100, m_OriginCoordinate, m_WorldScaleX, m_WorldScaleY, m_CategoryMapSet, m_Category2ColorMap));
}

TEST_F(TEST_LoadCategoryMap, DT_InvalidYSpan)
{
	EXPECT_FALSE(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->load(200, 0, m_OriginCoordinate, m_WorldScaleX, m_WorldScaleY, m_CategoryMapSet, m_Category2ColorMap));
}

TEST_F(TEST_LoadCategoryMap, DT_EmptyPathSet)
{
	EXPECT_FALSE(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->load(200, 100, m_OriginCoordinate, m_WorldScaleX, m_WorldScaleY, {}, m_Category2ColorMap));
}

TEST_F(TEST_LoadCategoryMap, DT_InvalidPath)
{
	auto TempSet = m_CategoryMapSet;
	TempSet.push_back({ m_PointCloudFileName ,0,2 });
	TempSet.push_back({ " " ,1,2 });
	EXPECT_FALSE(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->load(200, 100, m_OriginCoordinate, m_WorldScaleX, m_WorldScaleY, TempSet, m_Category2ColorMap));
}

TEST_F(TEST_LoadCategoryMap, DT_EmptyMapCategory)
{
	EXPECT_FALSE(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->load(200, 100, m_OriginCoordinate, m_WorldScaleX, m_WorldScaleY, m_CategoryMapSet, {}));
}

TEST_F(TEST_LoadCategoryMap, NT_LoadCategoryMapCorrectly)
{
	EXPECT_TRUE(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->load(200, 100, m_OriginCoordinate, m_WorldScaleX, m_WorldScaleY, m_CategoryMapSet, m_Category2ColorMap));
	EXPECT_EQ(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getNumCategoryMap(), 400);
	auto Info = hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getCategoryMapInfo();
	ASSERT_NE(Info.size(), 0);
	EXPECT_EQ(Info[0],200); EXPECT_EQ(Info[1], 100); EXPECT_EQ(Info[2], 10); EXPECT_EQ(Info[3], 5);
	EXPECT_EQ(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getPointCategoryAt(5, 5), EUserDefinedPointCatetory::USER_DEFINED_ROAD);
	EXPECT_EQ(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getPointCategoryAt(15, 5), EUserDefinedPointCatetory::USER_DEFINED_LANDSCAPE);
	EXPECT_EQ(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getPointCategoryAt(5, 15), EUserDefinedPointCatetory::USER_DEFINED_RIVER);
}

//TEST_F(TEST_LoadCategoryMap, NT_GetInnerCategory)
//{
//	EXPECT_TRUE(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->load(200, 100, m_OriginCoordinate, m_WorldScaleX, m_WorldScaleY, m_CategoryMapSet, m_Category2ColorMap));
//	EXPECT_EQ(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getCategory(50.0f, 25.0f), (std::uint16_t)EPointCategory::GROUND);
//	EXPECT_EQ(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getCategory(150.0f, 25.0f), (std::uint16_t)EPointCategory::ROAD);
//	EXPECT_EQ(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getCategory(50.0f, 75.0f), (std::uint16_t)EPointCategory::BUILDING);
//	EXPECT_EQ(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getCategory(250.0f, 70.0f), (std::uint16_t)EPointCategory::UNDEFINED);
//	EXPECT_EQ(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getCategory(150.0f, -50.0f), (std::uint16_t)EPointCategory::UNDEFINED);
//}

//TEST_F(TEST_LoadCategoryMap, NT_GetBoundaryCategory)
//{
//	EXPECT_TRUE(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->load(200, 100, m_OriginCoordinate, m_WorldScaleX, m_WorldScaleY, m_CategoryMapSet, m_Category2ColorMap));
//	EXPECT_EQ(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getCategory(100.0f, 25.0f), (std::uint16_t)EPointCategory::GROUND | (std::uint16_t)EPointCategory::ROAD | (std::uint16_t)EPointCategory::BOUDNARY_3);
//	EXPECT_EQ(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getCategory(100.0f, 50.0f), (std::uint16_t)EPointCategory::GROUND | (std::uint16_t)EPointCategory::ROAD | (std::uint16_t)EPointCategory::BUILDING | (std::uint16_t)EPointCategory::BOUDNARY_3);
//	EXPECT_EQ(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getCategory(85.0f, 25.0f), (std::uint16_t)EPointCategory::GROUND | (std::uint16_t)EPointCategory::ROAD | (std::uint16_t)EPointCategory::BOUDNARY_5);
//	EXPECT_EQ(hiveDesignPattern::hiveGetOrCreateProduct<CUserDefinedCategoryMap>("UserDefinedCategoryMap")->getCategory(75.0f, 25.0f), (std::uint16_t)EPointCategory::GROUND | (std::uint16_t)EPointCategory::ROAD | (std::uint16_t)EPointCategory::BOUDNARY_7);
//}