//
// pch.h
//

#pragma once

#include "gtest/gtest.h"

#include "common/DesignPatternInterface.h"

#include "PointCloudBlock.h"
#include "UserDefinedCategoryMap.h"
