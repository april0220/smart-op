﻿#include <iostream>
#include <optional>
#include <random>
#include <chrono>

std::optional<int> returnOptional(int vValue, int vCompareValue)
{
    if (vValue > vCompareValue) return vValue;
    return std::nullopt;
}

std::pair<int, bool> returnPair(int vValue, int vCompareValue)
{
	return (vValue > vCompareValue) ? std::make_pair(vValue, true) : std::make_pair(0, false);
}

int returnDirectly(int vValue, int vCompareValue)
{
    return (vValue > vCompareValue) ? vValue : 0;
}

int main()
{
    const std::uint32_t LoopNumber = 100000000;
    int CompareValue = 9;
    std::chrono::steady_clock::time_point StartMoment;
    std::chrono::steady_clock::time_point EndMoment;
	std::uint32_t Counter = 0;
	std::random_device rd;  // 产生一个种子随机数
	std::mt19937 gen(rd()); // 用种子随机数初始化一个URBG
	std::uniform_int_distribution<> dis(1, 20); //定义一个随机分布

    std::optional<int> r1;
	StartMoment = std::chrono::steady_clock::now();
    for (int i = 0; i < LoopNumber; i++)
    {
        r1 = returnOptional(dis(gen), CompareValue);
        if (r1.has_value()) Counter++;
    }
    EndMoment = std::chrono::steady_clock::now();
	std::cout << "Runtime of using [std::optioanl]:" << std::chrono::duration<double, std::ratio<1, 1000>>(EndMoment - StartMoment).count() << "ms." << std::endl;
	std::cout << "Runtime of using [std::optioanl]:" << std::chrono::duration<double, std::ratio<1, 1000>>(EndMoment - StartMoment).count() << "ms with " << Counter << " hits." << std::endl;

    std::pair<int, bool> r2;
    Counter = 0;
	StartMoment = std::chrono::steady_clock::now();
    for (int i = 0; i < LoopNumber; i++)
    {
         r2 = returnPair(dis(gen), CompareValue);
         if (r2.second) Counter++;
    }
	EndMoment = std::chrono::steady_clock::now();
	std::cout << "Runtime of using [std::pair]:" << std::chrono::duration<double, std::ratio<1, 1000>>(EndMoment - StartMoment).count() << "ms." << std::endl;
	std::cout << "Runtime of using [std::pair]:" << std::chrono::duration<double, std::ratio<1, 1000>>(EndMoment - StartMoment).count() << "ms with " << Counter << " hits." << std::endl;

	int r3;
	Counter = 0;
	StartMoment = std::chrono::steady_clock::now();
    for (int i = 0; i < LoopNumber; i++)
    {
        r3 = returnDirectly(dis(gen), CompareValue);
        if (r3 > 0) Counter++;
    }
	EndMoment = std::chrono::steady_clock::now();
	std::cout << "Runtime of using [int]:" << std::chrono::duration<double, std::ratio<1, 1000>>(EndMoment - StartMoment).count() << "ms." << std::endl;
	std::cout << "Runtime of using [int]:" << std::chrono::duration<double, std::ratio<1, 1000>>(EndMoment - StartMoment).count() << "ms with " << Counter << " hits." << std::endl;

    return 1;
}
